# Modac

Modac is a modular audio converter and amplifier.
It includes many different inputs and embedded digital active filtering. It uses a separate modules (DAC, power stage, volume).
It follows an earlier project. In 2012 I have made a double dac using a TDA1541A and AKM dacs with analog active filtering.
In this project I could switch between the TDA1541A and AK4396. The AKM won.

The schematics are drawn with Kicad.

Modularity:

Each piece can be used in a different project, or replaced by something else. Hence the name.
The purpose is to decompose the functionality into modules. Therefore it will be simpler to design and modify. 
And I am tired of the stack of amplifiers needed on my multichannel active filter system.
A simple all in one box using power opamps would be better and efficient.
Digital filtering is now available using as instance the STM32F4, or Psoc5 chips.
Digital multiplexing is cleaner, no more plops because of a shitty PC interface or cliquing relays.

Doing all the active filtering using digital filters and be able to configure it from a computer is easier than changing resistors on a board.
Plus the components can be repurposed, and many designs can be quickly achieved. It can be used to experiment.

![The prototype with 4 power outputs on speakon connectors](prototype.JPG)

Hardware used:

- Digital input and filter: Psoc5 chip + CS8416 Toslink/SPDIF Receiver (from previous desing) + one ADC + bluetooth i2s input.
- DAC: AK4396VF as on the previous design + OP249 opamp + ltc1563-3 reconstruction filter. One board for the tweeters and one for the medium range.
- Power amplifier: - Opa2544 dual power opamp for power + audio opamp preamplifier + UPC1237 protection circuit.
  The idea of using a power opamp comes from an engineer I worked with. He was using power opamps to control motors on a flight simulator and he told me to use this directly as an audio amplifier (the datasheet says "audio amplifier").

The design being modular, it can be reused in many configurations. The digital board can even be cascaded to add output canals.
The speaker protection boards is not in the project, it can be sourced from ebay (I use Clover Audio boards).
You need a protection circuit if you are a developer (otherwise the debugger or any mistake will kill your speakers).

I use a Psoc5 LP. I like the STM32F4 chips because everything is open source, however the PSOC5 has up to 20 I2S blocks, and a FIR filter block. 
The Psoc 5 LP is vastly more capabe, I was lucky to chose it, it has even USB sound interface!

The psoc5 is able to do 24bit multiply accumulate with a specialised module. 
It is difficult to do this by software on a 32bit architecture because of the 48bit accumulation register needed.
The Psoc has an FPGA, you can route signals to any pin. You can reroute any digital signal to your logic analyser or oscilloscope pins in the fpga schematic without touching the board.
It greatly improves electronic development speed.

Progress:

A first batch of the boards has been produced.
The power opamp sounds great.
Spdif input is working.
The filtering works with the filter module.
The filtering works with a custom stereo FIR filter.

Next steps:

- new boards
- usb audio
- Filtering interface












