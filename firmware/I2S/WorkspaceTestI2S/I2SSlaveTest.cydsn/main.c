/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

//#define USE_GENERATED_UDB

#define BUFSIZE 64

uint8 inputDataBuffer[BUFSIZE];
uint8 outputDataBuffer[BUFSIZE];

#define I2SSLAVEENABLE_CTRL 1

/* Defines for DMA_T */
#define DMA_T_BYTES_PER_BURST 1
#define DMA_T_REQUEST_PER_BURST 1
#define DMA_T_SRC_BASE (CYDEV_SRAM_BASE)
#define DMA_T_DST_BASE (CYDEV_PERIPH_BASE)

/* Defines for DMA_2 */
#define DMA_2_BYTES_PER_BURST 1
#define DMA_2_REQUEST_PER_BURST 1
#define DMA_2_SRC_BASE (CYDEV_PERIPH_BASE)
#define DMA_2_DST_BASE (CYDEV_SRAM_BASE)


/* Variable declarations for DMA_T */
static uint8 gDMA_T_Chan;
static uint8 gDMA_T_TD[2];

/* Variable declarations for DMA_2 */
static uint8 gDMA_2_Chan;
static uint8 gDMA_2_TD[2];

int DMATFini = 0;
int DMA2Fini = 0;

void DMAT_audioOutput_ISR()
{
    DMATFini = 1;
}

/*
* DmaT transfers the Toslink data to the treatment buffers
*
*/
void DMAT_Configuration(uint8* inputBuffer)
{
    gDMA_T_Chan = DMA_T_DmaInitialize(DMA_T_BYTES_PER_BURST, DMA_T_REQUEST_PER_BURST, HI16(DMA_T_SRC_BASE), HI16(DMA_T_DST_BASE));
    gDMA_T_TD[0] = CyDmaTdAllocate();
    CyDmaTdSetConfiguration(gDMA_T_TD[0], BUFSIZE, gDMA_T_TD[0], DMA_T__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR);
    CyDmaTdSetAddress(gDMA_T_TD[0], LO16((uint32)inputBuffer), LO16((uint32)I2S_T_TX_CH0_F0_PTR));
	// Associate the TD with the channel
    CyDmaChSetInitialTd(gDMA_T_Chan, gDMA_T_TD[0]);
    // Channel separation in the interrupt
    isr_DMAT_StartEx(DMAT_audioOutput_ISR); // When the last input sample is received, call audioInput to start a transfer to the DFB
	// Enable it
    CyDmaChEnable(gDMA_T_Chan, 1);
}

void DMA2_audioInput_ISR()
{
    DMA2Fini = 1;
}
/*
* Dma1 transfers the Toslink data to the treatment buffers
*
*/
void DMA2_Configuration(uint8* outputBuffer)
{
    gDMA_2_Chan = DMA_2_DmaInitialize(DMA_2_BYTES_PER_BURST, DMA_2_REQUEST_PER_BURST, HI16(DMA_2_SRC_BASE), HI16(DMA_2_DST_BASE));
    gDMA_2_TD[0] = CyDmaTdAllocate();
    //CyDmaTdSetConfiguration(gDMA_2_TD[0], BUFSIZE, CY_DMA_DISABLE_TD, DMA_2__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
    CyDmaTdSetConfiguration(gDMA_2_TD[0], BUFSIZE, gDMA_2_TD[0], DMA_2__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
#ifdef USE_GENERATED_UDB
    CyDmaTdSetAddress(gDMA_2_TD[0], LO16((uint32)ModacI2SSlave_1_Datapath_1_F0_PTR), LO16((uint32)outputBuffer));
#else
    CyDmaTdSetAddress(gDMA_2_TD[0], LO16((uint32)ModacI2SSlaveV_1_Datapath_1_u0__F0_F1_REG), LO16((uint32)outputBuffer));
#endif
	// Associate the TD with the channel
    CyDmaChSetInitialTd(gDMA_2_Chan, gDMA_2_TD[0]);
    // Channel separation in the interrupt
    isr_DMA2_StartEx(DMA2_audioInput_ISR); // When the last input sample is received, call audioInput to start a transfer to the DFB
	// Enable it
    CyDmaChEnable(gDMA_2_Chan, 1);
}

int main(void)
{
    for (int i = 0; i < BUFSIZE; i++)
    {
        inputDataBuffer[i] = i & 0x07;
        //inputDataBuffer[i] = 0x01;
        outputDataBuffer[i] = 0;
    }
    DMAT_Configuration(inputDataBuffer);
    DMA2_Configuration(outputDataBuffer);
    
    I2S_T_ClearTxFIFO();
    I2S_T_Start();
    // Send the toslink data
    I2S_T_EnableTx();

    // Enabling the custom I2S
#ifdef USE_GENERATED_UDB
    ModacI2SSlave_1_Count7_1_Enable();
    ModacI2SSlave_1_Count7_1_Start();
#endif
  
    Control_Reg_1_Write(I2SSLAVEENABLE_CTRL);
    
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    //uint8 togle = 0;
    for(;;)
    {
        CyDelay(30); 
        //Control_Reg_1_Write(togle);
        //togle = togle ^ 1;
        /* Place your application code here. */
    }
}

/* [] END OF FILE */
