
//`#start header` -- edit after this line, do not edit this line
// ========================================
//
// Copyright YOUR COMPANY, THE YEAR
// All Rights Reserved
// UNPUBLISHED, LICENSED SOFTWARE.
//
// CONFIDENTIAL AND PROPRIETARY INFORMATION
// WHICH IS THE PROPERTY OF your company.
//
// ========================================
`include "cypress.v"
//`#end` -- edit above this line, do not edit this line
// Generated on 04/11/2020 at 19:07
// Component: ModacI2SSlaveV
module ModacI2SSlaveV (
	output  DMA_Request,
	output  Tp1,
	output  Tp2,
	output  Tp3,
	output  Tp4,
	output  Tp5,
	input   clock,
	input   Enable,
	input   LRCk,
	input   Sdi
);

//`#start body` -- edit after this line, do not edit this line
//        Your code goes here

// Copyright 2020, Patrick Areny the Modac Project (https://gitlab.com/arenyp/modac)
// Usefull documents:
/*
https://www.cypress.com/file/123561/download PSoC 5LP Architecture TRM
https://www.cypress.com/file/137436/download Component Author Guide
https://www.cypress.com/file/41531/download  www.cypress.comDocument No. 001-82156Rev. *I1AN82156Designing PSoC Creator Components with UDB Datapaths
*/

/* ==================== Wire and Register Declarations ==================== */
localparam [1:0] IdleState = 2'b00;
localparam [1:0] Shift = 2'b01;
localparam [1:0] ShiftNFifo = 2'b10;
wire DMArequest;
wire Datapath_1_d0_load;
wire Datapath_1_d1_load;
wire Datapath_1_f0_load;
wire Datapath_1_f1_load;
wire Datapath_1_route_ci;
wire  [2:0] Datapath_1_select;
wire ctrl_1;
wire ctrl_2;
wire ctrl_3;
wire ctrl_4;
wire ctrl_5;
wire ctrl_6;
wire ctrl_7;
wire Count7_1_en;
wire Count7_1_load;
wire Count7_1_reset;
wire  [6:0] Count7_1_count;
wire Count7_1_tc;
reg cntrReset;
reg  [1:0] ShiftStateMachine_1;
reg UDBEnabled;
reg [5:0] count;
reg rx_f0_load;
reg LRCkTransition0;
reg LRCkTransition1;
wire LRCKnegedge;

/* ==================== Assignment of Combinatorial Variables ==================== */
assign DMA_Request = DMArequest;
assign Datapath_1_d0_load = (1'b0);
assign Datapath_1_d1_load = (1'b0);
assign Datapath_1_f0_load = rx_f0_load;
assign Datapath_1_f1_load = (1'b0);
assign Datapath_1_route_ci = (1'b0);
assign Datapath_1_select[0] = (ShiftStateMachine_1[0]);
assign Datapath_1_select[1] = (ShiftStateMachine_1[1]);
assign Datapath_1_select[2] = (1'b0);
assign Count7_1_en = (1'b1);
assign Count7_1_load = (1'b0);
assign Count7_1_reset = (1'b0);//(cntrReset);

`define SHOWCLOCKS
`ifdef SHOWCLOCKS
assign Tp1 = clock;
assign Tp2 = LRCk;
assign Tp3 = Sdi;
assign Tp4 = count[0];
assign Tp5 = Datapath_1_f0_load;
`else
assign Tp1 = clock;
assign Tp2 = LRCk;
assign Tp3 = LRCkTransition0;
assign Tp4 = LRCkTransition1;
assign Tp5 = LRCKposedge;
`endif

//assign Tp5 = (count == 3'b111);

/* ==================== Assignment of Registered Variables ==================== */

localparam counterStart = 6'b000001;

assign LRCKnegedge = LRCkTransition1 & ~LRCkTransition0;

always @ (posedge clock)
begin : LeftSampleEdgeDetect
    if (LRCk)
    begin
        LRCkTransition0 <= 1'b1;
    end
    else
    begin
        LRCkTransition0 <= 1'b0;
    end
    LRCkTransition1 <= LRCkTransition0;
end

always @ (posedge clock)
begin : counter
    if (LRCKnegedge) // Left channel first
    begin
        UDBEnabled <= Enable;
        count <= counterStart;
    end
    else
    begin
        count <= count + 6'b000001;
    end
end

/* ==================== Datapath_1 (Width: 8) Instantiation ==================== */

parameter Datapath_1_dpconfig0 = 
{
    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0, `CS_SHFT_OP___SL, `CS_A0_SRC__ALU, `CS_A1_SRC_NONE, `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA, `CS_CMP_SEL_CFGA,  /*CFGRAM0:  A0 = SDIN; Shift A0 */
    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0, `CS_SHFT_OP___SL, `CS_A0_SRC__ALU, `CS_A1_SRC_NONE, `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA, `CS_CMP_SEL_CFGA,  /*CFGRAM1:  Shift and push */
    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0, `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE, `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA, `CS_CMP_SEL_CFGA,  /*CFGRAM2:   */
    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0, `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE, `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA, `CS_CMP_SEL_CFGA,  /*CFGRAM3:   */
    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0, `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE, `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA, `CS_CMP_SEL_CFGA,  /*CFGRAM4:   */
    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0, `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE, `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA, `CS_CMP_SEL_CFGA,  /*CFGRAM5:   */
    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0, `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE, `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA, `CS_CMP_SEL_CFGA,  /*CFGRAM6:   */
    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0, `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE, `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA, `CS_CMP_SEL_CFGA,  /*CFGRAM7:   */
    8'hFF, 8'h00, /*CFG9:  CFG9 */
    8'hFF, 8'hFF, /*CFG11-10:  CFG11-10 */
    `SC_CMPB_A1_D1, `SC_CMPA_A0_D1, `SC_CI_B_ARITH, `SC_CI_A_ARITH, `SC_C1_MASK_DSBL, `SC_C0_MASK_DSBL, `SC_A_MASK_DSBL, `SC_DEF_SI_0, `SC_SI_B_DEFSI, `SC_SI_A_ROUTE, /*CFG13-12:   SI A source is routed from SI input */
    `SC_A0_SRC_ACC, `SC_SHIFT_SL, 1'h0, 1'h0, `SC_FIFO1__A0, `SC_FIFO0__A0, `SC_MSB_DSBL, `SC_MSB_BIT0, `SC_MSB_NOCHN, `SC_FB_NOCHN, `SC_CMP1_NOCHN, `SC_CMP0_NOCHN, /*CFG15-14:   FIFO source is A0 */
    /*CFG17-16:   */
     3'h00, `SC_FIFO_SYNC__ADD, 2'h00, `SC_FIFO1_DYN_OF, 3'h00, `SC_FIFO_CLK__DP,`SC_FIFO_CAP_AX,`SC_FIFO_LEVEL,`SC_FIFO_ASYNC, `SC_EXTCRC_DSBL, `SC_WRK16CAT_DSBL
};
cy_psoc3_dp8 #(
    .cy_dpconfig_a( Datapath_1_dpconfig0 ),
    .d0_init_a( 8'b00000000 ),
    .d1_init_a( 8'b00000000 ),
    .a0_init_a( 8'b00000000 ),
    .a1_init_a( 8'b00000000 ))
    Datapath_1(
        .clk( clock ),
        .cs_addr( Datapath_1_select ),
        .route_si( Sdi ),
        .route_ci( Datapath_1_route_ci ),
        .f0_load( Datapath_1_f0_load ),   // mov F0 <- AO when a byte is completed
        .f1_load( Datapath_1_f1_load ),
        .d0_load( Datapath_1_d0_load ),
        .d1_load( Datapath_1_d1_load ),
        .ce0(  ), 
        .cl0(  ), 
        .z0(  ), 
        .ff0(  ), 
        .ce1(  ), 
        .cl1(  ), 
        .z1(  ), 
        .ff1(  ), 
        .ov_msb(  ), 
        .co_msb(  ), 
        .cmsb(  ), 
        .so(  ), 
        .f0_bus_stat( DMArequest ), 
        .f0_blk_stat(  ), 
        .f1_bus_stat(  ), 
        .f1_blk_stat(  )
    );

/* ==================== State Machine: ShiftStateMachine_1 ==================== */

localparam STATE_SHIFTIN      = 2'b00;
localparam STATE_SHIFTINF0OUT = 2'b01;

// Load FO with 10 signal
always @(posedge clock)
begin
    rx_f0_load <= (ShiftStateMachine_1 == STATE_SHIFTINF0OUT);
end

always @(posedge clock)
begin
    if(UDBEnabled)
    begin
        case(ShiftStateMachine_1)
        STATE_SHIFTIN:
            // WORD_SEL64
            // 8 16 24 32 40 48 56 64/0
            case(count[5:0])
            6'd62: ShiftStateMachine_1 <= STATE_SHIFTINF0OUT;
            6'd54: ShiftStateMachine_1 <= STATE_SHIFTINF0OUT;
            6'd46: ShiftStateMachine_1 <= STATE_SHIFTINF0OUT;
            6'd38: ShiftStateMachine_1 <= STATE_SHIFTINF0OUT;
            6'd30: ShiftStateMachine_1 <= STATE_SHIFTINF0OUT;
            6'd22: ShiftStateMachine_1 <= STATE_SHIFTINF0OUT;
            6'd14: ShiftStateMachine_1 <= STATE_SHIFTINF0OUT;
            6'd6 : ShiftStateMachine_1 <= STATE_SHIFTINF0OUT;
            default: ShiftStateMachine_1 <= STATE_SHIFTIN;
            endcase            
        STATE_SHIFTINF0OUT: ShiftStateMachine_1 <= STATE_SHIFTIN;
        //
        default: ShiftStateMachine_1 <= STATE_SHIFTIN;
        endcase
    end
    else
    /*******************************************************************
    * Because of the delay for the Rx enabling the first bit should be 
    * captured in preparation for the Rx being enabled. So rx has to 
    * shift input data continuously, even if it isn't enabled 
    *******************************************************************/
    begin
        ShiftStateMachine_1 <= STATE_SHIFTIN;
    end
end
        
        
//end

    
//`#end` -- edit above this line, do not edit this line
endmodule
//`#start footer` -- edit after this line, do not edit this line
//`#end` -- edit above this line, do not edit this line
