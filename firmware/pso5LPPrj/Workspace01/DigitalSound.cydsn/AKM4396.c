/**
    AKM4396 Dac configuration
*/
#include "project.h"
#include "AKM4396.h"

t_AKMregs g_AKMregs;

void AKM_BITWR(unsigned char bit)
{
  CCLK_Write(0);
  if (bit != 0)
  {
    CDTI_Write(1);
  }
  else
  {
    CDTI_Write(0);
  }
  CyDelay(1);
  CCLK_Write(1);
  CyDelay(1);
}

// If 0 all the dacs are selected
void selectAllDacs(char select)
{
  CSDAC1_Write(select); // Chip select/ to 0/1 
  CSDAC2_Write(select); // Chip select/ to 0/1
}

unsigned char write_AKM(unsigned char address, unsigned char value)
{
  unsigned char i;

  CyDelay(1);
  //CSDAC1_Write(0); // Chip select/ to 0
  selectAllDacs(0);
  // C1 = 0, C0 = 0
  AKM_BITWR(0);
  AKM_BITWR(0);
  // R/W to write: 1
  AKM_BITWR(1);
  // Write the register address
  for (i = 4; i > 0; i--)
    {
      AKM_BITWR((address >> i) & 1);
    }
  AKM_BITWR(address & 1);
  // Write the data
  for (i = 7; i > 0; i--)
    {
      AKM_BITWR((value >> i) & 1);
    }
  AKM_BITWR(value & 1);
  //CSDAC1_Write(1); // Chip select/ to 1
  selectAllDacs(1);
  CyDelay(1);
  return 0;
}

void mute_AK4396(char mute)
{
  if (mute)
    g_AKMregs.ctrl2 = g_AKMregs.ctrl2 | 1;
  else
    g_AKMregs.ctrl2 = g_AKMregs.ctrl2 & (~1);
  write_AKM(AKMCTRL2, g_AKMregs.ctrl2);
}

void reset_AK4396(void)
{
  AUTORST_Write(0);
  CyDelay(5);
  AUTORST_Write(1);
  CyDelay(10);
}

void Config_AK4396(/*int frequency*/)
{
  // Non selected
  selectAllDacs(1);
  CCLK_Write(1); // Clock to 1
  CyDelay(4);

  // Set the structure to the default values because the
  // component can only be written
  g_AKMregs.ctrl1 = 0x05;
  g_AKMregs.ctrl2 = 0x02;
  g_AKMregs.ctrl3 = 0x00; // PCM mode selection (not changed)
  g_AKMregs.Lattenuation = 0xFF; // volume maximum
  g_AKMregs.Rattenuation = 0xFF;

  // Reset the ic once at power up
  //reset_AK4396();
  // AKS 0 0 0 0 MODE2 MODE1 MODE0 RSTN
  // 24bits I2s:           011 <-
  // 24bits MSB justified: 010
  // 24bits LSB justified: 100
  g_AKMregs.ctrl1 = 0b00000111;
  write_AKM(AKMCTRL1, g_AKMregs.ctrl1);
  // Normal speed, Default emphasis response, No mute,
  g_AKMregs.ctrl2 = 0b00000000;
  write_AKM(AKMCTRL2, g_AKMregs.ctrl2);
  return;
/*
  // PCM mode
  // Ne pas ecrire, ceci implique un reset si on change D/P
  g_AKMregs.ctrl3 = 0b00000000;
  write_AKM(AKMCTRL3, g_AKMregs.ctrl3);

  write_AKM(AKML_ATTEN, g_AKMregs.Lattenuation);
  write_AKM(AKMR_ATTEN, g_AKMregs.Rattenuation);
*/
  // Mute disabled
  //mute_AK4396(0);
}

void setRollOffFilter(char filterslow)
{
  static char prev = 0;

  if (filterslow != prev)
    {
      if (filterslow)
	g_AKMregs.ctrl2 |=  0b00100000;
      else
	g_AKMregs.ctrl2 &= ~0b00100000;
      write_AKM(AKMCTRL2, g_AKMregs.ctrl2);
      prev = filterslow;
    }
}

// Sampling speed control (table 6)
void update_AKM_DFS0(char f96khz)
{
  if (f96khz)
    {
      if ((g_AKMregs.ctrl2 & DOUBLE_SPEED) == 0) // Not yet in double speed
    	{
    	  mute_AK4396(1);
    	  CyDelay(20);
    	  g_AKMregs.ctrl2 |= DOUBLE_SPEED;
    	  write_AKM(AKMCTRL2, g_AKMregs.ctrl2);
    	  mute_AK4396(0);
    	}
    }
  else if ((g_AKMregs.ctrl2 & DOUBLE_SPEED) != 0) // in double speed
	{
	  mute_AK4396(1);
	  CyDelay(20);
	  g_AKMregs.ctrl2 &= ~DOUBLE_SPEED;
	  write_AKM(AKMCTRL2, g_AKMregs.ctrl2);
	  mute_AK4396(0);
	}
}

