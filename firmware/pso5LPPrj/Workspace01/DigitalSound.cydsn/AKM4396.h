
#define AK4396_ADDRESS  0x00

// AKM4396 register
#define AKMCTRL1             0x00
#define AKMCTRL2             0x01
#define AKMCTRL3             0x02
#define AKML_ATTEN           0x03
#define AKMR_ATTEN           0x04

#define NORMAL_SPEED  0x00 // 30 - 54   Khz
#define DOUBLE_SPEED  0x08 // 54 - 108  Khz
#define QUAD_SPEED    0x10 // 120 - 216 Khz

typedef struct s_AKMregs
{
  unsigned char ctrl1;
  unsigned char ctrl2;
  unsigned char ctrl3;
  unsigned char Lattenuation;
  unsigned char Rattenuation;
}              t_AKMregs;

/*
typedef struct s_dac
{
  char       benabled;   // Sound enabled
  int        freq;       // Frequency
  int        err;        // Input error
  char       blostClock; // 1 in case of a spdif clock problem
  char       external44khzclk; // 1 if the 44khz quartz (redbook) is used on the spdif receiver
  char       source;
  char       AK4396_SlowRollOffFilter;
}              t_dac;
*/

void Config_AK4396(/*int frequency*/);
void setRollOffFilter(char filterslow);
void mute_AK4396(char mute);
void selectAllDacs(char select);
