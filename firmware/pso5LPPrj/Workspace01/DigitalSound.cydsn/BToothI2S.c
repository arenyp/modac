/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "constants.h"
#include "inputISR.h"

//extern int currentIn;

#define I2SSLAVEENABLE_CTRL 1

/* Defines for DMA_2 */
#define DMA_BT_BYTES_PER_BURST 1
#define DMA_BT_REQUEST_PER_BURST 1
#define DMA_BT_SRC_BASE (CYDEV_PERIPH_BASE)
#define DMA_BT_DST_BASE (CYDEV_SRAM_BASE)


/* Variable declarations for DMA_BTooth */
static uint8 gDMA_BTooth_Chan;
static uint8 gDMA_BTooth_TD[2];

void audioInput_ISR();

// Interrupt routine
void DMA_BTooth_audioInput_ISR()
{
    audioInput_ISR(); // Start the output
    PWM_1_Start();
}

/*
* Dma1 transfers the Toslink data to the treatment buffers
*
*/
void DMA_BTooth_Configuration(uint8* inputBuffer)
{
    gDMA_BTooth_Chan = DMA_BTooth_DmaInitialize(DMA_BT_BYTES_PER_BURST, DMA_BT_REQUEST_PER_BURST, HI16(DMA_BT_SRC_BASE), HI16(DMA_BT_DST_BASE));
    gDMA_BTooth_TD[0] = CyDmaTdAllocate();
    gDMA_BTooth_TD[1] = CyDmaTdAllocate();
    CyDmaTdSetConfiguration(gDMA_BTooth_TD[0], SNDBUFFERSZ, gDMA_BTooth_TD[1], DMA_BTooth__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
    CyDmaTdSetConfiguration(gDMA_BTooth_TD[1], SNDBUFFERSZ, gDMA_BTooth_TD[0], DMA_BTooth__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
    CyDmaTdSetAddress(gDMA_BTooth_TD[0], LO16((uint32)ModacI2SSlaveV_1_Datapath_1_u0__F0_F1_REG), LO16((uint32)inputBuffer));
    CyDmaTdSetAddress(gDMA_BTooth_TD[1], LO16((uint32)ModacI2SSlaveV_1_Datapath_1_u0__F0_F1_REG), LO16((uint32)&inputBuffer[SNDBUFFERSZ]));
	// Associate the TD with the channel
    CyDmaChSetInitialTd(gDMA_BTooth_Chan, gDMA_BTooth_TD[0]);
    // Channel separation in the interrupt
    isr_DMA_BTooth_StartEx(DMA_BTooth_audioInput_ISR); // When the last input sample is received, call audioInput to start a transfer to the DFB
	// Enable it
    //CyDmaChEnable(gDMA_BTooth_Chan, 1);
}

// Starts or stops the bluetooth function
void startBlueTooth()
{
    uint8 ctrlReg;

    ctrlReg = Control_Reg_1_Read();
    CyDmaChSetInitialTd(gDMA_BTooth_Chan, gDMA_BTooth_TD[0]);
    CyDmaChEnable(gDMA_BTooth_Chan, 1);
    Control_Reg_1_Write(ctrlReg | I2SSLAVEENABLE_CTRL);
}

void stopBlueTooth()
{
    uint8 ctrlReg;

    ctrlReg = Control_Reg_1_Read();
    CyDmaChDisable(gDMA_BTooth_Chan);
    ctrlReg = ctrlReg & ~I2SSLAVEENABLE_CTRL; // Clear the bit, preserve the other control bits
    Control_Reg_1_Write(ctrlReg);
}

#ifdef DEBUGI2SSLAVE
#define BUFSIZE 64

uint8 inputDataBuffer[BUFSIZE];

#define I2SSLAVEENABLE_CTRL 1

/* Defines for DMA_T */
#define DMA_T_BYTES_PER_BURST 1
#define DMA_T_REQUEST_PER_BURST 1
#define DMA_T_SRC_BASE (CYDEV_SRAM_BASE)
#define DMA_T_DST_BASE (CYDEV_PERIPH_BASE)

/* Variable declarations for DMA_T */
static uint8 gDMA_T_Chan;
static uint8 gDMA_T_TD[2];

int DMATFini = 0;

void DMAT_audioOutput_ISR()
{
    DMATFini = 1;
}

/*
* DmaT transfers the Toslink data to the treatment buffers
*
*/
void DMAT_Configuration(uint8* inputBuffer)
{
    gDMA_T_Chan = DMA_T_DmaInitialize(DMA_T_BYTES_PER_BURST, DMA_T_REQUEST_PER_BURST, HI16(DMA_T_SRC_BASE), HI16(DMA_T_DST_BASE));
    gDMA_T_TD[0] = CyDmaTdAllocate();
    CyDmaTdSetConfiguration(gDMA_T_TD[0], BUFSIZE, gDMA_T_TD[0], DMA_T__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR);
    CyDmaTdSetAddress(gDMA_T_TD[0], LO16((uint32)inputBuffer), LO16((uint32)I2S_T_TX_CH0_F0_PTR));
	// Associate the TD with the channel
    CyDmaChSetInitialTd(gDMA_T_Chan, gDMA_T_TD[0]);
    // Channel separation in the interrupt
    isr_DMAT_StartEx(DMAT_audioOutput_ISR); // When the last input sample is received, call audioInput to start a transfer to the DFB
	// Enable it
    CyDmaChEnable(gDMA_T_Chan, 1);
}

uint8 inputDataTestBuffer[BUFSIZE];

void initDMATest()
{
    for (int i = 0; i < BUFSIZE; i++)
    {
        inputDataTestBuffer[i] = i & 0x07;
    }
    DMAT_Configuration(inputDataTestBuffer);

    I2S_T_ClearTxFIFO();
    I2S_T_Start();
    // Send the toslink data
    I2S_T_EnableTx();
}
#endif

/* [] END OF FILE */
