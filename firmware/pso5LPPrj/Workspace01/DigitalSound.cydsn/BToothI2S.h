/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#define AUDIO_CLOCK_SELECT_BIT (1 << 7)

void DMA_BTooth_audioInput_ISR();
void DMA_BTooth_Configuration(uint8* inputBuffer);

void startBlueTooth();
void stopBlueTooth();



/* [] END OF FILE */
