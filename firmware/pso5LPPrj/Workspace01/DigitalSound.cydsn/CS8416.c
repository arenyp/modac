
#include "project.h"
#include "CS8416.h"

#define I2C_WRITE 0
#define I2C_READ  1

#define NACK 0

void reset_AK4396andCS8416(void)
{
  AUTORST_Write(0);
  CyDelay(5);
  AUTORST_Write(1);
  CyDelay(10);
}

unsigned char read_CS8416(unsigned char address)
{
  unsigned char r;
    
  I2C_1_MasterSendStart(CS8416_ADDRESS, I2C_WRITE);
  I2C_1_MasterWriteByte(address);
  I2C_1_MasterSendRestart(CS8416_ADDRESS, I2C_READ);
  r = I2C_1_MasterReadByte(NACK);
  I2C_1_MasterSendStop();
/*
  i2c_start_wait(CS8416_ADDRESS + I2C_WRITE);       // set device address and write mode
  i2c_write(address);                               // write the address
  i2c_rep_start(CS8416_ADDRESS + I2C_READ);
  r = i2c_read(0);                                  // read one byte
  i2c_stop();
*/
  return r;  
}

void write_CS8416(unsigned char address, unsigned char value)
{
  unsigned char status;
    
  I2C_1_MasterClearStatus();
  status = I2C_1_MasterSendStart(CS8416_ADDRESS, I2C_WRITE);
  if (status == I2C_1_MSTR_NO_ERROR)
  {
    status = I2C_1_MasterWriteByte(address);
    status = I2C_1_MasterWriteByte(value);
  }
  I2C_1_MasterSendStop();
    /*
  i2c_start_wait(CS8416_ADDRESS + I2C_WRITE);       // set device address and write mode
  i2c_write(address);                               // write the address
  i2c_write(value);                                 // write the data
  i2c_stop();
    */
}

void CS8416_Configure_spdif_receiver(void)
{
  I2C_1_Start();

  // Serial audio data format
  //      SOMS SSF SORES1-0 SOJUST SODEL SOSPOL SOLRPOL
  // I2S:    x   x      x x      0     1      0       1
  //write_CS8416(CSSER_DATA_FORMAT, 0b10100101); // Master, Omclk = 64fs, 24bits, SODEL, SOLRPOL
  write_CS8416(CSSER_DATA_FORMAT, 0b00000101); // Slave, Omclk don't care, 24bits, Left justified, SODEL MSB on second period, SOLRPOL high = righ channel

  //      0 FSWCLK 0 0 PDUR TRUNC Reserved Reserved
  //      0      0 0 0    1     1        0        0
  write_CS8416(CSCTRL0,           0b00000000); // FORCE LOCAL CLOCK, PDUR, TRUNC
  write_CS8416(CSCTRL1,           0b00000000); // Interrupt active hight, hold audio sample on error, MCLK = 256*FS
  write_CS8416(CSCTRL2,           0b01000010); // Auto de-emphasis, GPO-O to interrupt output
  write_CS8416(CSCTRL3,           0b00000000); // GPO-1 and GPO-2 to GRND
  write_CS8416(CSCTRL4,           0b10000010); // Run!, input multiplexer on RXP0, GPO  linked to the third grounded input

  I2C_1_Stop();
  CyDelay(4);
  CS8416_read_status();
}

void CS8416_set_mux_input(char mux_input)
{
  I2C_1_Start();
  switch (mux_input)
    {
    case MUX_TOSLINK0:
      write_CS8416(CSCTRL4, 0b10000010); // Run!, input multiplexer on RXP0, GPO  linked to the third grounded input
      break;
    case MUX_TOSLINK1:
      write_CS8416(CSCTRL4, 0b10001010); // Run!, input multiplexer on RXP1, GPO  linked to the third grounded input
      break;
    default:
      break;
    };
  I2C_1_Stop();
}

unsigned int CS8416_read_status()
{
    unsigned char formatDetectStatus;
    unsigned char errorStatus;
    unsigned char ctrl4;
    unsigned char format;
    unsigned char id;
    
    I2C_1_Start();
    formatDetectStatus = read_CS8416(CSAUDIO_FORMAT);
    errorStatus = read_CS8416(CSRECEIVER_ERROR);
    ctrl4 = read_CS8416(CSCTRL4);
    format = read_CS8416(CSSER_DATA_FORMAT);
    id = read_CS8416(CSID_VERSION);
    I2C_1_Stop();
    return errorStatus + formatDetectStatus * 256 + ctrl4 + format + id;
}
