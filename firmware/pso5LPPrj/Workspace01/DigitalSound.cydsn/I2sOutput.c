/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include "constants.h"
#include "inputISR.h"

/* Variable declarations for DMA_2 */
uint8 gDMA_2_Chan;
uint8 gDMA_2_TD[2];

/* Variable declarations for DMA_3 */
uint8 gDMA_3_Chan;
uint8 gDMA_3_TD[2];

uint8 outputStarted = 0;
uint8 outputReady = WAIT;

uint8 TDBuffer1Update[2];
uint8 TDBuffer2Update[2];

// Says that the output buffer is ready to stream
void setOutputReady()
{
    outputReady = READY;
}

// Says that the output buffer is not ready to stream
void disableOutputReady()
{
    outputReady = WAIT;
}

// returns the output buffer ready flag
uint8 getOutputReady()
{
    return outputReady;
}

// true if the I2S outputs were started
uint8 getOutputStarted()
{
    return outputStarted;
}

// Checks if a new TD must be started, and starts the transfer
void audioOnput1_ISR()
{
    if (getOutputStarted() && getNextTD() < NOTD && TDBuffer1Update[getNextTD()])
    {
        TDBuffer1Update[getNextTD()] = 0;
        CyDmaChSetInitialTd(gDMA_2_Chan, gDMA_2_TD[getNextTD()]);
        CyDmaChEnable(gDMA_2_Chan, 1);
    }
}

void audioOnput2_ISR()
{
    if (getOutputStarted() && getNextTD() < NOTD && TDBuffer2Update[getNextTD()])
    {
        TDBuffer2Update[getNextTD()] = 0;
        CyDmaChSetInitialTd(gDMA_3_Chan, gDMA_3_TD[getNextTD()]);
        CyDmaChEnable(gDMA_3_Chan, 1);
    }
}

/*
* If the DMA have finished and nothing was ready, it must be restarted.
*/
void restartDMAOutIfNeeded()
{
    uint8 status;
    uint8 currentTD;
    
    CyDmaChStatus(gDMA_2_Chan, &currentTD, &status);
    if ((status & CY_DMA_STATUS_TD_ACTIVE) == 0)
    {
        TDBuffer1Update[getNextTD()] = 0;
        CyDmaChSetInitialTd(gDMA_2_Chan, gDMA_2_TD[getNextTD()]);
        CyDmaChEnable(gDMA_2_Chan, 1);
    }
    CyDmaChStatus(gDMA_3_Chan, &currentTD, &status);
    if ((status & CY_DMA_STATUS_TD_ACTIVE) == 0)
    {
        TDBuffer2Update[getNextTD()] = 0;
        CyDmaChSetInitialTd(gDMA_3_Chan, gDMA_3_TD[getNextTD()]);
        CyDmaChEnable(gDMA_3_Chan, 1);
    }
}

/*
* Dma2 is the first dac output
*
*/
void DMA2_Configuration(uint8* inputBuffer)
{
    gDMA_2_Chan = DMA_2_DmaInitialize(DMA_2_BYTES_PER_BURST, DMA_2_REQUEST_PER_BURST, HI16(DMA_2_SRC_BASE), HI16(DMA_2_DST_BASE));
    gDMA_2_TD[0] = CyDmaTdAllocate();
    gDMA_2_TD[1] = CyDmaTdAllocate();
#ifdef DMAOUTTDLOOPS
    CyDmaTdSetConfiguration(gDMA_2_TD[0], SNDBUFFERSZ, gDMA_2_TD[1], DMA_2__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR);
    CyDmaTdSetConfiguration(gDMA_2_TD[1], SNDBUFFERSZ, gDMA_2_TD[0], DMA_2__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR);
#else
    CyDmaTdSetConfiguration(gDMA_2_TD[0], SNDBUFFERSZ, CY_DMA_DISABLE_TD, DMA_2__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR);
    CyDmaTdSetConfiguration(gDMA_2_TD[1], SNDBUFFERSZ, CY_DMA_DISABLE_TD, DMA_2__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR);
#endif
    // From ram to the I2S output, @ + 1 to pass the useless byte consequence of the endian swap. And we transfert bytes anyways, no alignment issue.
    CyDmaTdSetAddress(gDMA_2_TD[0], LO16((uint32)inputBuffer), LO16((uint32)I2S_2out_TX_CH0_F0_PTR));
    CyDmaTdSetAddress(gDMA_2_TD[1], LO16((uint32)&inputBuffer[SNDBUFFERSZ]), LO16((uint32)I2S_2out_TX_CH0_F0_PTR));
	// Associate the TD with the channel
    CyDmaChSetInitialTd(gDMA_2_Chan, gDMA_2_TD[0]);
	// Enable it
    //CyDmaChEnable(gDMA_2_Chan, 1);
    TDBuffer1Update[0] = TDBuffer1Update[1] = 0;
}

/*
* Dma3 is the second dac output
*
*/
void DMA3_Configuration(uint8* inputBuffer)
{
    gDMA_3_Chan = DMA_3_DmaInitialize(DMA_3_BYTES_PER_BURST, DMA_3_REQUEST_PER_BURST, HI16(DMA_3_SRC_BASE), HI16(DMA_3_DST_BASE));
    gDMA_3_TD[0] = CyDmaTdAllocate();
    gDMA_3_TD[1] = CyDmaTdAllocate();
#ifdef DMAOUTTDLOOPS
    CyDmaTdSetConfiguration(gDMA_3_TD[0], SNDBUFFERSZ, gDMA_3_TD[1], DMA_3__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR);
    CyDmaTdSetConfiguration(gDMA_3_TD[1], SNDBUFFERSZ, gDMA_3_TD[0], DMA_3__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR);
#else
    // We must wait for the DMA to stop in order to restart it in the input interrupt. And this does not work.
    CyDmaTdSetConfiguration(gDMA_3_TD[0], SNDBUFFERSZ, CY_DMA_DISABLE_TD, DMA_3__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR);
    CyDmaTdSetConfiguration(gDMA_3_TD[1], SNDBUFFERSZ, CY_DMA_DISABLE_TD, DMA_3__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR);
#endif
    // From ram to the I2S output, @ + 1 to pass the useless byte consequence of the endian swap
    CyDmaTdSetAddress(gDMA_3_TD[0], LO16((uint32)inputBuffer), LO16((uint32)I2S_3out_TX_CH0_F0_PTR));
    CyDmaTdSetAddress(gDMA_3_TD[1], LO16((uint32)&inputBuffer[SNDBUFFERSZ]), LO16((uint32)I2S_3out_TX_CH0_F0_PTR));
	// Associate the TD with the channel
    CyDmaChSetInitialTd(gDMA_3_Chan, gDMA_3_TD[0]);
	// Enable it
    //CyDmaChEnable(gDMA_3_Chan, 1);
    TDBuffer2Update[0] = TDBuffer2Update[1] = 0;
}

// Starts only the I2S clocks, with zero on the data. This is used to configure the DACS
void startI2SOutputCLocks()
{
    I2S_2out_Start(); // I2S low frequency out
    I2S_3out_Start(); // I2S High frequency out
}

// Starts all the output I2S channels and their DMAS
void startOutputChannels()
{
    // Do not output until the first output buffer is ready (filtered)
    /*
    while (outputReady == WAIT)
    {
        // do nothing
    }
    */
    // I2S outputs initialisation (clear them here otherwise a synchronisation problem could occur)
    I2S_2out_ClearTxFIFO();
    I2S_3out_ClearTxFIFO();
#ifndef DMAOUTTDLOOPS
    outputStarted = 1;
    // setup the next TD interrupts
    isr_DMAOut1_StartEx(audioOnput1_ISR);
    isr_DMAOut2_StartEx(audioOnput2_ISR);
#endif
    // DMA Start
    CyDmaChEnable(gDMA_2_Chan, 1);
    CyDmaChEnable(gDMA_3_Chan, 1);
    // Start the I2S clocks and DMA requests to feed the fifos
    startI2SOutputCLocks();
    // Wait for DMA to fill the  FIFO
    uint8 I2SStatus = I2S_2out_ReadTxStatus();
    while ((I2SStatus & I2S_2out_TX_FIFO_0_NOT_FULL) != 0)
    {
        I2SStatus = I2S_2out_ReadTxStatus();
    }
#ifndef LOWFREQONLY
    I2SStatus = I2S_3out_ReadTxStatus();
    while ((I2SStatus & I2S_3out_TX_FIFO_0_NOT_FULL) != 0)
    {
        I2SStatus = I2S_3out_ReadTxStatus();
    }
#endif
    // Start outputing at the next word select front
    I2S_2out_EnableTx();
#ifndef LOWFREQONLY
    I2S_3out_EnableTx();
#endif
}
/*
* Stop the outputs
*
*/
void stopOutputChannels()
{
    outputStarted = 0;
    // Stop the output
    I2S_2out_DisableTx();
#ifndef LOWFREQONLY
    I2S_3out_DisableTx();
#endif
    CyDelayUs(20); /* Provide enough time for DMA to transfer the last audio samples completely to I2S TX FIFO */
    // Stop the I2S clocks
    I2S_2out_Stop(); // I2S low frequency out
#ifndef LOWFREQONLY
    I2S_3out_Stop(); // I2S High frequency out
#endif
    CyDmaChDisable(gDMA_2_Chan);
    CyDmaChDisable(gDMA_3_Chan);
}

/* [] END OF FILE */
