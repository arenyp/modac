/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

void setOutputReady();
void disableOutputReady();
uint8 getOutputReady();
uint8 getOutputStarted();
// Dma2 is the second dac output
void DMA2_Configuration(uint8* inputBuffer);
// Dma3 is the second dac output
void DMA3_Configuration(uint8* inputBuffer);
// Starts only the I2S clocks, with zero on the data. This is used to configure the DACS
void startI2SOutputCLocks();
// Starts all the output I2S channels and their DMAS
void startOutputChannels();
void stopOutputChannels();
void restartDMAOutIfNeeded();

/* [] END OF FILE */
