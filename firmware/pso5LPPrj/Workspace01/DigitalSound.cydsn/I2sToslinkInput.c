/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include "constants.h"
#include "inputISR.h"

extern int currentIn;

/* Variable declarations for DMA_1 */
static uint8 gDMA_TLink_Chan;
static uint8 gDMA_TLink_TD[2];

/*
* Dma1 transfers the Toslink data to the treatment buffers
*
*/
void DMA_TLink_Configuration(uint8* inputBuffer)
{
    gDMA_TLink_Chan = DMA_TLink_DmaInitialize(DMA_1_BYTES_PER_BURST, DMA_1_REQUEST_PER_BURST, HI16(DMA_1_SRC_BASE), HI16(DMA_1_DST_BASE));
    gDMA_TLink_TD[0] = CyDmaTdAllocate();
    gDMA_TLink_TD[1] = CyDmaTdAllocate();
#ifdef DIRECTTOBUFFEROUT
    CyDmaTdSetConfiguration(gDMA_TLink_TD[0], SNDBUFFERSZ, gDMA_TLink_TD[1], DMA_TLink__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
    CyDmaTdSetConfiguration(gDMA_TLink_TD[1], SNDBUFFERSZ, gDMA_TLink_TD[0], DMA_TLink__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
    CyDmaTdSetAddress(gDMA_TLink_TD[0], LO16((uint32)I2S_TLink_RX_CH0_F0_PTR), LO16((uint32)&buffer3OutputLoFreq[1]));
    CyDmaTdSetAddress(gDMA_TLink_TD[1], LO16((uint32)I2S_TLink_RX_CH0_F0_PTR), LO16((uint32)&buffer3OutputLoFreq[SNDBUFFERSZ + 1]));
#else
    CyDmaTdSetConfiguration(gDMA_TLink_TD[0], SNDBUFFERSZ, gDMA_TLink_TD[1], DMA_TLink__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
    CyDmaTdSetConfiguration(gDMA_TLink_TD[1], SNDBUFFERSZ, gDMA_TLink_TD[0], DMA_TLink__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
    CyDmaTdSetAddress(gDMA_TLink_TD[0], LO16((uint32)I2S_TLink_RX_CH0_F0_PTR), LO16((uint32)inputBuffer));
    CyDmaTdSetAddress(gDMA_TLink_TD[1], LO16((uint32)I2S_TLink_RX_CH0_F0_PTR), LO16((uint32)&inputBuffer[SNDBUFFERSZ]));
#endif
	// Associate the TD with the channel
    CyDmaChSetInitialTd(gDMA_TLink_Chan, gDMA_TLink_TD[0]);
    // Channel separation in the interrupt
    isr_DMA_TLink_StartEx(audioInput_ISR); // When the last input sample is received, call audioInput to start a transfer to the DFB
	// Enable it
    //CyDmaChEnable(gDMA_TLink_Chan, 1);
}

// Disables the receive DMA and I2S Master RX
void stopToslinkI2SInput()
{
    // Stop the corresponding I2S input
    I2S_TLink_DisableRx();
    I2S_TLink_ClearRxFIFO();
    CyDmaChDisable(gDMA_TLink_Chan);
    I2S_TLink_Stop();
    // Says that it will wait for the next buffer before starting
    disableOutputReady();
}

// Starts the receive DMA and I2S Master RX
void startToslinkI2SInput()
{
    // Start the corresponding I2S input
    currentIn = 0;
    CyDmaChSetInitialTd(gDMA_TLink_Chan, gDMA_TLink_TD[0]);
    CyDmaChEnable(gDMA_TLink_Chan, 1);
    I2S_TLink_ClearRxFIFO();
    I2S_TLink_Start();  // Toslink in
    // Receive the toslink data
    I2S_TLink_EnableRx();
}

/* [] END OF FILE */
