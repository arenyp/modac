/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

// Toslink I2S DMA input configuration
void DMA_TLink_Configuration(uint8* inputBuffer);
// Disables the receive DMA and I2S Master RX
void stopToslinkI2SInput();
// Starts the receive DMA and I2S Master RX
void startToslinkI2SInput();

/* [] END OF FILE */
