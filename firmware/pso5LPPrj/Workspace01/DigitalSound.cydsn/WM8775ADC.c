/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include "WM8775ADC.h"
#include "constants.h"
#include "inputISR.h"

#define R7_TIMEOUTDISABLE  0x07
#define R11_ADCCTRL0       0x0B
#define R12_ADCCTRL1       0x0C
#define R13_ADCPWRCTRL     0x0D
#define R14_WM8775LCHANNEL 0x0E
#define R15_ATTENUATION    0x0F
#define R16_ALCCTRL1       0x10
#define R17_ALCCTRL2       0x11
#define R18_ALCCTRL3       0x12
#define R19_NOISEGATE      0x13
#define R20_LIMITERCTRL    0x14
#define R21_MIXERCTRL      0x15
#define R23_RESETREGISTER  0x17


#define RIGHTJUSTIFIEDMODE (0b00000000)
#define LEFTJUSTIFIEDMODE  (0b00000001)
#define I2SMODE            (0b00000010)
#define DSPMODE            (0b00000011)

#define NONINVERTEDPOL     (0)

#define DATA24BIT           0b00100000
#define HIGHPASSDISABLE     0b10000000


#define SLAVEINTERFACE   0b00000000
#define MASTERINTERFACE  0b10000000

#define MCLK256FS        0b00000010

#define OVERSAMPLING128  0b00000000
#define OVERSAMPLING64   0b00001000

#define POWERON   0b00000000
#define POWERDOWNSTEP1 0b00000110
#define POWERDOWNSTEP2 0b00000001

#define ZEROGAIN 0xCF

#define CHANNEL_AIN1 0b00000001
#define CHANNEL_AIN2 0b00000010
#define CHANNEL_AIN3 0b00000100
#define CHANNEL_AIN4 0b00001000
#define MUTELR       0b00110000


/* Defines for DMA */
#define DMA_ADC_BYTES_PER_BURST 1
#define DMA_ADC_REQUEST_PER_BURST 1
#define DMA_ADC_SRC_BASE (CYDEV_PERIPH_BASE)
#define DMA_ADC_DST_BASE (CYDEV_SRAM_BASE)


/* Variable declarations for DMA_1 */
static uint8 gDMA_ADC_Chan;
static uint8 gDMA_ADC_TD[2];

extern int currentIn;
void audioInput_ISR();

// Init the ADC DMA
void DMA_ADC_Configuration(uint8 *inputBuffer)
{
    gDMA_ADC_Chan = DMA_ADC_DmaInitialize(DMA_ADC_BYTES_PER_BURST, DMA_ADC_REQUEST_PER_BURST, HI16(DMA_ADC_SRC_BASE), HI16(DMA_ADC_DST_BASE));
    gDMA_ADC_TD[0] = CyDmaTdAllocate();
    gDMA_ADC_TD[1] = CyDmaTdAllocate();
    //
    CyDmaTdSetConfiguration(gDMA_ADC_TD[0], SNDBUFFERSZ, gDMA_ADC_TD[1], DMA_ADC__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
    CyDmaTdSetConfiguration(gDMA_ADC_TD[1], SNDBUFFERSZ, gDMA_ADC_TD[0], DMA_ADC__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
    CyDmaTdSetAddress(gDMA_ADC_TD[0], LO16((uint32)I2S_ADCIn_RX_CH0_F0_PTR), LO16((uint32)inputBuffer));
    CyDmaTdSetAddress(gDMA_ADC_TD[1], LO16((uint32)I2S_ADCIn_RX_CH0_F0_PTR), LO16((uint32)&inputBuffer[SNDBUFFERSZ]));
	// Associate the TD with the channel
    CyDmaChSetInitialTd(gDMA_ADC_Chan, gDMA_ADC_TD[0]);
    // Channel separation in the interrupt
    isr_DMA_ADC_StartEx(audioInput_ISR); // When the last input sample is received, call audioInput to start a transfer to the DFB
	// Enable it
    //CyDmaChEnable(gDMA_ADC_Chan, 1);
}

// Start to receive the data from the ADC
void startI2SADCin()
{
    // Start the corresponding I2S input
    currentIn = 0;
    CyDmaChSetInitialTd(gDMA_ADC_Chan, gDMA_ADC_TD[0]);
    CyDmaChEnable(gDMA_ADC_Chan, 1);
    I2S_ADCIn_ClearRxFIFO();
    I2S_ADCIn_Start();  // Toslink in
    // Receive the toslink data
    I2S_ADCIn_EnableRx();
}

// Stop receiving from the ADC
void stopI2SADCin()
{
    CyDmaChDisable(gDMA_ADC_Chan);
    I2S_ADCIn_DisableRx();
    I2S_ADCIn_ClearRxFIFO();
    I2S_ADCIn_Stop();
}

// If 0 the chip is selected
void selectWM8775(char select)
{
  //TP_2_Write(select);
  CSADC_Write(select); // Chip select/ to 0/1 
}

// Write one bit
void WM8775_BITWR(unsigned char bit)
{
  //TP_1_Write(0);
  CCLK_Write(0);
  if (bit != 0)
  {
    //TP_3_Write(1);
    CDTI_Write(1);
  }
  else
  {
    //TP_3_Write(0);
    CDTI_Write(0);
  }
  CyDelay(1);
  CCLK_Write(1);
  //TP_1_Write(1);
  CyDelay(1);
}

unsigned char write_WM8775ADC(unsigned char address, unsigned short value)
{
  unsigned char i;

  CyDelay(1);
  selectWM8775(0);
  // Write the register address
  for (i = 6; i > 0; i--)
    {
      WM8775_BITWR((address >> i) & 1);
    }
  WM8775_BITWR(address & 1);
  // Write the data
  for (i = 8; i > 0; i--)
    {
      WM8775_BITWR((value >> i) & 1);
    }
  WM8775_BITWR(value & 1);
  selectWM8775(1);
  CyDelay(1);
  return 0;
}

// If one, sets the power on
void WM8775Power(int8 powerOn)
{
    if (powerOn)
    {
        write_WM8775ADC(R13_ADCPWRCTRL, 0);
    }
    else
    {
        write_WM8775ADC(R13_ADCPWRCTRL, POWERDOWNSTEP1);
        write_WM8775ADC(R13_ADCPWRCTRL, POWERDOWNSTEP2);        
    }
}

// Select the channel from 0 to 3
void WM8775ChannelSelect(uint8 channel)
{
    /* LRBOTH = 1, use input channel. */
	write_WM8775ADC(R21_MIXERCTRL, 0x100 | (1 << channel));
}

void muteWM8775ADC()
{
    //write_WM8775ADC(INPUTMUX, MUTELR);
}

/* Configures the ADC
*  It works like the DAC with CCLK CDTI and a chip select
*  It samples at 44100hz 24bits, transmits at 256fs
*/
void initWM8775ADC()
{
    // Reset
    write_WM8775ADC(R23_RESETREGISTER, 0x000);
    //
    /* Disable zero cross detect timeout */
	write_WM8775ADC(R7_TIMEOUTDISABLE, 0x000);
	/* I2S, 24-bit mode */
	write_WM8775ADC(R11_ADCCTRL0, 0x022);
	/* Slave mode, clock ratio 256fs */
	write_WM8775ADC(R12_ADCCTRL1, 0x002);
	/* Powered up */
	write_WM8775ADC(R13_ADCPWRCTRL, 0x00);
	/* ADC gain +2.5dB, enable zero cross */
	write_WM8775ADC(R14_WM8775LCHANNEL, 0x1d4);
	/* ADC gain +2.5dB, enable zero cross */
	write_WM8775ADC(R15_ATTENUATION, 0x1d4);
	/* ALC Stereo, ALC target level -1dB FS max gain +8dB */
	write_WM8775ADC(R16_ALCCTRL1, 0x1bf);
	/* Enable gain control, use zero cross detection,
	   ALC hold time 42.6 ms */
	write_WM8775ADC(R17_ALCCTRL2, 0x185);
	/* ALC gain ramp up delay 34 s, ALC gain ramp down delay 33 ms */
	write_WM8775ADC(R18_ALCCTRL3, 0x0a2);
	/* Enable noise gate, threshold -72dBfs */
	write_WM8775ADC(R19_NOISEGATE, 0x005);
	/* Transient window 4ms, lower PGA gain limit -1dB */
	write_WM8775ADC(R20_LIMITERCTRL, 0x07a);
    //
    uint8 channel = 0;
    WM8775ChannelSelect(channel);
}


/* [] END OF FILE */
