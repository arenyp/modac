/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

void DMA_ADC_Configuration(uint8 *inputBuffer);
void startI2SADCin();
void stopI2SADCin();

void initWM8775ADC();
void WM8775Power(int8 powerOn);

/* [] END OF FILE */
