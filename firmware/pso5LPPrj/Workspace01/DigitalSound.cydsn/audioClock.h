/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

// Available sample rates
#define SAMPLERATE44100 0
#define SAMPLERATE48000 1
#define SAMPLERATE96000 2

// Clock type
#define TOSLINKMCLK  0
#define PLLADC       1
#define PLLUSBCFG    2
#define PLLBLUETOOTH 3

void clockSelect(uint8 clockType);
void configurePLLForAudioMasterClock(uint8 sampleRate, uint8 clockType);
void adjustAudioMasterClock(int value);

/* [] END OF FILE */
