#include "Filter_1.h"
#include "Filter_1_PVT.h"


/*******************************************************************************
* ChannelA filter coefficients.
* Filter Type is FIR
*******************************************************************************/

/* Renamed array for backward compatibility.
*  Should not be used in new designs.
*/
#define ChannelAFirCoefficients Filter_1_ChannelAFirCoefficients

/* Number of FIR filter taps are: 16 */

const uint8 CYCODE Filter_1_ChannelAFirCoefficients[Filter_1_FIR_A_SIZE] = 
{
 0x27u, 0xF8u, 0xFFu, 0x00u, /* Tap(0), -0.00023949146270752 */

 0xB1u, 0x5Au, 0x00u, 0x00u, /* Tap(1), 0.00276768207550049 */

 0x1Eu, 0x87u, 0x01u, 0x00u, /* Tap(2), 0.0119359493255615 */

 0xA2u, 0xEBu, 0x03u, 0x00u, /* Tap(3), 0.0306284427642822 */

 0x0Fu, 0x82u, 0x07u, 0x00u, /* Tap(4), 0.0586565732955933 */

 0x89u, 0xAFu, 0x0Bu, 0x00u, /* Tap(5), 0.0912944078445435 */

 0x70u, 0x6Bu, 0x0Fu, 0x00u, /* Tap(6), 0.120466232299805 */

 0x2Du, 0xA1u, 0x11u, 0x00u, /* Tap(7), 0.137731194496155 */

 0x2Du, 0xA1u, 0x11u, 0x00u, /* Tap(8), 0.137731194496155 */

 0x70u, 0x6Bu, 0x0Fu, 0x00u, /* Tap(9), 0.120466232299805 */

 0x89u, 0xAFu, 0x0Bu, 0x00u, /* Tap(10), 0.0912944078445435 */

 0x0Fu, 0x82u, 0x07u, 0x00u, /* Tap(11), 0.0586565732955933 */

 0xA2u, 0xEBu, 0x03u, 0x00u, /* Tap(12), 0.0306284427642822 */

 0x1Eu, 0x87u, 0x01u, 0x00u, /* Tap(13), 0.0119359493255615 */

 0xB1u, 0x5Au, 0x00u, 0x00u, /* Tap(14), 0.00276768207550049 */

 0x27u, 0xF8u, 0xFFu, 0x00u, /* Tap(15), -0.00023949146270752 */
};


/*******************************************************************************
* ChannelB filter coefficients.
* Filter Type is FIR
*******************************************************************************/

/* Renamed array for backward compatibility.
*  Should not be used in new designs.
*/
#define ChannelBFirCoefficients Filter_1_ChannelBFirCoefficients

/* Number of FIR filter taps are: 16 */

const uint8 CYCODE Filter_1_ChannelBFirCoefficients[Filter_1_FIR_B_SIZE] = 
{
 0x57u, 0x8Du, 0xFFu, 0x00u, /* Tap(0), -0.00349915027618408 */

 0x81u, 0xA4u, 0x00u, 0x00u, /* Tap(1), 0.00502026081085205 */

 0xB2u, 0x52u, 0xFCu, 0x00u, /* Tap(2), -0.02872633934021 */

 0xCEu, 0x41u, 0x00u, 0x00u, /* Tap(3), 0.00200819969177246 */

 0x7Du, 0xF6u, 0xF0u, 0x00u, /* Tap(4), -0.117477774620056 */

 0x76u, 0xA5u, 0x01u, 0x00u, /* Tap(5), 0.012861967086792 */

 0xB8u, 0xCCu, 0xD6u, 0x00u, /* Tap(6), -0.321877479553223 */

 0x49u, 0x7Eu, 0x41u, 0x00u, /* Tap(7), 0.511666417121887 */

 0x49u, 0x7Eu, 0x41u, 0x00u, /* Tap(8), 0.511666417121887 */

 0xB8u, 0xCCu, 0xD6u, 0x00u, /* Tap(9), -0.321877479553223 */

 0x76u, 0xA5u, 0x01u, 0x00u, /* Tap(10), 0.012861967086792 */

 0x7Du, 0xF6u, 0xF0u, 0x00u, /* Tap(11), -0.117477774620056 */

 0xCEu, 0x41u, 0x00u, 0x00u, /* Tap(12), 0.00200819969177246 */

 0xB2u, 0x52u, 0xFCu, 0x00u, /* Tap(13), -0.02872633934021 */

 0x81u, 0xA4u, 0x00u, 0x00u, /* Tap(14), 0.00502026081085205 */

 0x57u, 0x8Du, 0xFFu, 0x00u, /* Tap(15), -0.00349915027618408 */
};

