/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

// Number of FIR filter taps is 16
#define FILTERTAPS 16
#define FILTERTAPS_SIZE (4 * 16)

typedef unsigned char uint8;


/* [] END OF FILE */
