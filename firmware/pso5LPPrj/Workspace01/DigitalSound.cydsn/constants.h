/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* Defines for DMA_1 */
#define DMA_1_BYTES_PER_BURST 1
#define DMA_1_REQUEST_PER_BURST 1
#define DMA_1_SRC_BASE (CYDEV_PERIPH_BASE)
#ifdef DIRECTTOI2SOUT
#define DMA_1_DST_BASE (CYDEV_PERIPH_BASE)
#else
#define DMA_1_DST_BASE (CYDEV_SRAM_BASE)
#endif

/* Defines for DMA_2 */
#define DMA_2_BYTES_PER_BURST 1
#define DMA_2_REQUEST_PER_BURST 1
#define DMA_2_SRC_BASE (CYDEV_SRAM_BASE)
//#define DMA_2_SRC_BASE (CYDEV_FLASH_BASE)
//#define DMA_2_SRC_BASE g_samples
#define DMA_2_DST_BASE (CYDEV_PERIPH_BASE)
//#define DMA_2_DST_BASE (CYDEV_SRAM_BASE)

/* Defines for DMA_3 */
#define DMA_3_BYTES_PER_BURST 1
#define DMA_3_REQUEST_PER_BURST 1
#define DMA_3_SRC_BASE (CYDEV_SRAM_BASE)
#define DMA_3_DST_BASE (CYDEV_PERIPH_BASE)

/* Defines for DMA_3 */
#define DMA_FILTER_BYTES_PER_BURST 4      // The DFB can use 32bit DMA spokes with SRAM
#define DMA_FILTER_REQUEST_PER_BURST 1
#define DMA_FILTER_SRC_BASE (CYDEV_SRAM_BASE)
#define DMA_FILTER_DST_BASE (CYDEV_PERIPH_BASE)


#define NBSAMPLES     256
#define NBCHANNELS      2
#define BYTESPERSAMPLE  4

// Multiple of 24bits
#define SNDBUFFERSZ (NBSAMPLES * NBCHANNELS * BYTESPERSAMPLE)

#define SWAPBYTES (2 * BYTESPERSAMPLE)

#define NBFTAPS 16

#define RIGHTC 0
#define LEFTC  18

#define WAIT  0
#define READY 1

#define LOPROGRESS 0
#define HIPROGRESS 1


//#define DIRECTTOBUFFEROUT
//#define LOWFREQONLY   // previous define is mandatory
#define BYPASS_THE_DFB
//#define USEBLUETOOTHINPUT
//#define USE_USBAUDIO
// Remove this define to stop the output TD and restart it only when the next output buffer is ready (hence resynchronise)
//#define DMAOUTTDLOOPS   // Enables DMA TD loops for the audio output.
//#define WAITFORBLUETOOTHINPUT
