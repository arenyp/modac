/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include "I2sOutput.h"
#include "constants.h"
#include "inputISR.h"

extern uint8 buffer1Input[2 * SNDBUFFERSZ + SWAPBYTES];
extern int currentIn;
extern uint8 buffer3OutputLoFreq[2 * SNDBUFFERSZ + SWAPBYTES];
extern uint8 buffer4OutputHiFreq[2 * SNDBUFFERSZ + SWAPBYTES];

extern uint8 gDMA_2_Chan;
extern uint8 gDMA_2_TD[2];
extern uint8 gDMA_3_Chan;
extern uint8 gDMA_3_TD[2];

extern uint8 TDBuffer1Update[2];
extern uint8 TDBuffer2Update[2];

uint8 nextTD = NOTD;

// Gives the next TD to the DMA output interrupts
uint8 getNextTD()
{
    return nextTD;
}

// Stops the DMA outputs on their next interrupt
void resetNextTD()
{
    nextTD = NOTD;
}

// Waits for completion of the last TD and starts with the next channel
void waitDMAAndRestart(uint8 channelHandle, int currentInOffset,  uint8* pDMA_TD)
{
    uint8 currentTD;
    uint8 status;
    uint8 nextTD;
    
    // Wait transfer end
    CyDmaChStatus(channelHandle, &currentTD, &status);  
    //if (currentTD == pDMA_TD[0])
    if (currentInOffset == 0) // The last received buffer is 0
    {
        nextTD =  pDMA_TD[0]; // Transmit the first TD
    }
    else
    {
        nextTD =  pDMA_TD[1]; // Transmit the second TD
    }
    //while ((status & (CY_DMA_STATUS_TD_ACTIVE | CY_DMA_STATUS_CHAIN_ACTIVE)) != 0)
    while ((status & CY_DMA_STATUS_TD_ACTIVE) != 0)
    {
        CyDmaChStatus(channelHandle, &currentTD, &status);
    }
    CyDmaChSetInitialTd(channelHandle, nextTD);
    CyDmaChEnable(channelHandle, 1);
}

// Filter DMA out routine, on completion of a TD.
// Called in isr_FOutL_Interrupt
void filteringCompleted()
{
#ifdef READ_DFB_TESTOUTPUT
    if (finished == 0)
    {
        memcpy(samplesOutA, buffer4OutputHiFreq, SNDBUFFERSZ);
        memcpy(samplesOutB, buffer4OutputHiFreq, SNDBUFFERSZ);
        finished = 1;
    }
#endif
    setOutputReady();
}

// Input DMA routine, on completion of a TD.
// Called in isr_DMA1
void audioInput_ISR()
{
    // Now filter evetything out
#ifdef BYPASS_THE_DFB
    memcpy(&buffer3OutputLoFreq[currentIn + 1], &buffer1Input[currentIn + 1], SNDBUFFERSZ);
#ifndef LOWFREQONLY
    memcpy(&buffer4OutputHiFreq[currentIn + 1], &buffer1Input[currentIn + 1], SNDBUFFERSZ);
#endif
#ifndef DMAOUTTDLOOPS
    // The output DMA stoped at the end of his data buffer (or not if the master clock is slower)
    // Restart it here so it does not desynchronise.
    /*
    if (getOutputStarted())
    {
        waitDMAAndRestart(gDMA_2_Chan, currentIn,  gDMA_2_TD);
        waitDMAAndRestart(gDMA_3_Chan, currentIn,  gDMA_3_TD);
    }*/
    if (currentIn == 0) // The last received buffer is 0
    {
        nextTD = 0; // Transmit the first TD
    }
    else
    {
        nextTD = 1; // Transmit the second TD
    }
    TDBuffer1Update[nextTD] = 1;
    TDBuffer2Update[nextTD] = 1;
    //restartDMAOutIfNeeded();
#endif
    //
    filteringCompleted();
#else
#ifdef USEBLUETOOTHINPUT
    filteringCompleted();
#else
#ifdef DIRECTTOBUFFEROUT
    filteringCompleted();
#else
    // Start the endian swap of the buffer
    startFilterInputDMA(currentIn);
#endif    
#endif
#endif
    // Change the input buffer
    if (currentIn == 0)
    {
        currentIn = SNDBUFFERSZ; // Change buffer
    }
    else
    {
        currentIn = 0;
    }
}

/* [] END OF FILE */
