/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#define NOTD 200

// Waits for completion of the last TD and starts with the next channel
void waitDMAAndRestart(uint8 channelHandle, int currentInOffset,  uint8* pDMA_TD);
void setOutputReady();
void disableOutputReady();
uint8 isOutputReady();
void filteringCompleted();
void audioInput_ISR();
uint8 getNextTD();
void resetNextTD();

/* [] END OF FILE */
