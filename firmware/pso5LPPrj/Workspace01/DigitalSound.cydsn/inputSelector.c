/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "audioClock.h"
#include "inputSelector.h"
#include "usbAudio.h"
#include "BToothI2S.h"
#include "WM8775ADC.h"
#include "watchDog.h"
#include "I2sOutput.h"
#include "constants.h"
#include "inputISR.h"

// Prototypes
void DMA1_Configuration(uint8* inputBuffer);
// Ugly extern
extern uint8 buffer1Input[2 * SNDBUFFERSZ + SWAPBYTES];
extern int currentIn;

void stopToslinkI2SInput();
void startToslinkI2SInput();

static uint8 inputSelect = AUDIOIN_TOSLINK0;

// Sets the input depending on the select pins
int8 inputSelectIn()
{
    uint8 selIn = SEL_0_Read() | SEL_1_Read() << 1 | SEL_2_Read() << 2; // VCC pull ups
    uint8 ret = AUDIOIN_TOSLINK0;
    
    switch (selIn)
    {
        case 6:
        {
            ret = AUDIOIN_USB;
        }
        break;
        case 5:
        {
            ret = AUDIOIN_ADC0;
        }
        break;
        case 3:
        {
            ret = AUDIOIN_BLUETOOTH;
        }
        break;
        default:
            ret = AUDIOIN_TOSLINK0;
        break;
    };
    inputSelect = ret;
    return ret;
}

uint8 getCurrentInput()
{
    return inputSelect;
}

void stopInput(uint8 input)
{
    // In any case, stop the DMA out
    resetNextTD();
    // \todo wait for a zero crossing, fill the next buffer with a decreasing slope and wait for it's end
   // stopOutputChannels();
    switch(input)
    {
        case AUDIOIN_TOSLINK0:
        case AUDIOIN_TOSLINK1:
        {
            stopToslinkI2SInput();
        }
        break;
        case AUDIOIN_ADC0:
        case AUDIOIN_ADC1:
        case AUDIOIN_ADC2:
        case AUDIOIN_ADC3:
        {
            stopI2SADCin();
            WM8775Power(0);
        }
        break;
        case AUDIOIN_USB:
        {
            //USBStop();
        }
        break;
        case AUDIOIN_BLUETOOTH:
        {
            stopBlueTooth();
        }
        break;
        default :
        {
            clockSelect(TOSLINKMCLK);
        }
        break;
    };
}

// The DACs needs a clock, we use the ADC clock
void startADCCLock()
{
    clockSelect(PLLADC);
    configurePLLForAudioMasterClock(SAMPLERATE48000, PLLADC);
}

void selectInput(uint8 input)
{
    // Stops the previous input
    stopInput(inputSelect);
    // Waits a little
    CyDelay(10);
    // Resets the ring buffer offset
    currentIn = 0;
    switch(input)
    {
        case AUDIOIN_ADC0:
        case AUDIOIN_ADC1:
        case AUDIOIN_ADC2:
        case AUDIOIN_ADC3:
        {
            // Start the master clock before programming the ADC
            startADCCLock();
            // Init the ADC
            initWM8775ADC();
            // Start the I2S input and DMA
            startI2SADCin();
        }
        break;
        case AUDIOIN_USB:
        {
            configurePLLForAudioMasterClock(SAMPLERATE48000, PLLADC);
            clockSelect(PLLUSBCFG); // Configure the multiplexer
            // The USB device interface must be initialised while interrupts are enabled
            //usbInit();
        }
        break;
        case AUDIOIN_BLUETOOTH:
        {
#ifndef WAITFORBLUETOOTHINPUT
            // Start and sotp are made by the watchdog timer because it needs to know if a clock is ready
            configurePLLForAudioMasterClock(SAMPLERATE48000, PLLBLUETOOTH);
            clockSelect(PLLBLUETOOTH);
            startBlueTooth();
#endif
        }
        break;
        case AUDIOIN_TOSLINK0:
        case AUDIOIN_TOSLINK1:
        default :
        {
            clockSelect(TOSLINKMCLK);
            startToslinkI2SInput();
        }
        break;
    };
    inputSelect = input;
}



/* [] END OF FILE */
