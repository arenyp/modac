/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

// Audio inputs
//
// CS8416 toslink receiver I2S slave (2 of 4 inputs multiplexed)
// WM8775 ADC I2S slave (4 inputs multiplexed)
// Bluetooth I2S master
// USB audio
//
// There is one input buffer, each of those inputs has a DMA (or copy for the USB)
// When we switch between inputs we must:
// - Stop the previous transfer and the input interface
// - Configure the input interface and start the new transfer


#define AUDIOIN_TOSLINK0  0
#define AUDIOIN_TOSLINK1  1
#define AUDIOIN_ADC0      2
#define AUDIOIN_ADC1      3
#define AUDIOIN_ADC2      4
#define AUDIOIN_ADC3      5
#define AUDIOIN_USB       6
#define AUDIOIN_BLUETOOTH 7
//#define AUDIOIN_I2SMIC    4

void startADCCLock();

int8 inputSelectIn();
void selectInput(uint8 input);
uint8 getCurrentInput();

/* [] END OF FILE */
