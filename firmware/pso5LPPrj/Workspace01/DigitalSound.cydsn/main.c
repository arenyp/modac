/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "math.h"
#include "AKM4396.h"
#include "CS8416.h"
#include "WM8775ADC.h"
#include "coefficients.h"
#include "constants.h"
#include "stereoFilter.h"
#include "usbAudio.h"
#include "BToothI2S.h"
#include "I2sToslinkInput.h"
#include "I2sOutput.h"
#include "watchDog.h"
#include "audioClock.h"
#include "inputSelector.h"

// 
static uint32 gSamples[NBSAMPLES];

// Stereo output buffers
int currentOut = 0; // buffer used for output, 0 or 1
uint8 buffer3OutputLoFreq[2 * SNDBUFFERSZ + SWAPBYTES];
uint8 buffer4OutputHiFreq[2 * SNDBUFFERSZ + SWAPBYTES];

// Stereo input buffer
int currentIn = 0; // Two buffers in one. This is the DMA-input, ring buffer start
uint8 buffer1Input[2 * SNDBUFFERSZ + SWAPBYTES];

void fillSampleBuffer()
{
    float  midV = 200000.;
    int    v;
    int    i;

    // Sinewave generation, 24bits
    for (i = 0; i < NBSAMPLES; i++)
    {
        // 24 bit samples, little endian (3 bytes)
        v = (int)(midV * sinf(2.f * M_PI * ((float)i / (float)NBSAMPLES)));
        //
        gSamples[i] = v;
    }
    // The first samples must be set to zero because they go into the filter
    for (i = 0; i < SNDBUFFERSZ; i++)
    {
        buffer1Input[i] = 0;
        buffer3OutputLoFreq[i] = 0;
        buffer4OutputHiFreq[i] = 0;
    }
}

#ifdef READ_DFB_TESTOUTPUT
int32 samplesOutA[NBSAMPLES * NBCHANNELS];
int32 samplesOutB[NBSAMPLES * NBCHANNELS];
int finished = 0;

int32 testData[] = {
0x00E168,
0x00432A,
0x00000B,
0x00000C,
0x00000D,
0x00000E,
0x00000F,
0x000010,
0x000011,
0x000012,
0x000013,
0x000014,
0x000015,
0x000016,
0x000017,
0x000018,
0x000019,
0x00001A,
0x00000D,
0x00000E,
0x00000F,
0x000010,
0x000011,
0x000012,
0x000013,
0x000014,
0x000015,
0x000016,
0x000017,
0x000018,
0x000019,
0x00001A
};
#endif

int main(void)
{
    CyDelay(20); // Power line stabilisation

    fillSampleBuffer();

    // Reset the ics once at power up
    reset_AK4396andCS8416();
    CS8416_Configure_spdif_receiver();

    // Input DMAs initialisation
    DMA_TLink_Configuration(&buffer1Input[1]); // Audio Input, keep a sample at start for endian swap, like X, FF 3D 17 X; FF 1A 90 X; ...
    DMA_BTooth_Configuration(&buffer1Input[1]);
    DMA_ADC_Configuration(&buffer1Input[1]);

    // Output DMAs initialisation
    DMA2_Configuration(&buffer3OutputLoFreq[1]); // Output to Dac 1; offset for endian swap
#ifndef LOWFREQONLY
    DMA3_Configuration(&buffer4OutputHiFreq[1]); // Output to Dac 2
#endif

    // Init of the external DACs.
    // ! The dac can only be powered on when the I2S clocks are running
    // Set the input to the ADC in order to always generate a clock on the DACs
    startADCCLock();
    startI2SOutputCLocks();
    // If no master clock is generated and the DACs are not quickly programmed, they will emit loud noises.
    Config_AK4396(0);
    // Enable the dacs
    selectAllDacs(0); // CS\ to 0
    
#ifdef BYPASS_THE_DFB000
    isr_FOutL_StartEx(filteringCompleted);    
#endif
    // Starts the input selection and other constant tests
    initWatchDogTimer();

    CyGlobalIntEnable;
    
#ifdef USE_USBAUDIO
    usbInit();
#endif

    // Configure the custom stereo filters on the DFB; (bytes 123x, 123x, ... aligned)
#ifdef BYPASS_THE_DFB000
    initDFBStereoFilter(buffer1Input, buffer4OutputHiFreq, buffer3OutputLoFreq);
#endif
#ifdef READ_DFB_TESTOUTPUT
    memcpy(buffer1Input, testData, sizeof(testData));
    audioInput_ISR();
#else
    
#endif

    // Input selection and start.
    uint8 input = inputSelectIn();
    selectInput(input);

    // Do not output until the first output buffer is ready (filtered)
    while (getOutputReady() == WAIT)
    {
#ifdef USE_USBAUDIO
        if (input == AUDIOIN_USB)
        {
            USBFSStateHandling(&buffer1Input[1]);
        }
#endif 
    }
    // Start the outputs
    startOutputChannels();
    // Refresh input select here
        
    //
    for(;;)
    {
#ifdef USE_USBAUDIO
        if (input == AUDIOIN_USB)
        {
            USBFSStateHandling(&buffer1Input[1]);
        }
#else
        CyDelay(10);
#endif
/*
        // Change audio input
        if (inputSelectIn() != getCurrentInput())
        {
            selectInput(input);
        }
*/
    }
}

/* [] END OF FILE */
