// Pass through code, use this to pass through the DFB without filtering
// Filter coefficients are stored in RAM-Barea acu   
// 16 taps  
dw 0x0F0F 
// mreg  
dw 0x0000 
// lreg  
dw 0x0000 
// initial value for ACU-A.reg   
area data_b
// 16 taps 
dw 0x00E168
dw 0x00432A
dw 0x00000B

// add another 14 taps here... 
area data_a
// 16 taps 
dw 0x00E168
dw 0x00432A
dw 0x00000B


initial:  
// Enable modulus arithmetic 
acu(setmod,setmod) dmux(sa,sa) alu(set0) mac(clra)     
acu(clear,clear) dmux(sa,sa) alu(hold) mac(hold) jmp(eob, wait_for_data) 

wait_for_data: 
// Wait input from stage-A(in1) for the next element; Clear the semaphore 0
acu(hold,hold) dmux(sa,sa) alu(hold) mac(hold) jmpl(in1,load_data)
// alu(setsem, 001) alu(clearsem, 001)

load_data:
// Clear the semaphore
acu(hold,hold) dmux(sa,sa) alu(clearsem, 010) mac(hold)

// stage-A(in1) present. Open the channel from stage-A(addr(1))
// addr(1) means ACU Ram Row 1 on A and B
acu(hold,hold) addr(1) dmux(ba,sa) alu(seta) mac(hold)
// Nop
acu(hold,hold) dmux(sa,sa) alu(hold) mac(hold)
// Writes the alu to the output
// Write shifter output to the bus' holding register A with addr(1)   
acu(hold,hold) addr(1) dmux(sa,sa) alu(hold) mac(hold) write(abus)
acu(hold,hold) addr(0) dmux(sa,sa) alu(hold) mac(hold) write(bbus) //jmp(eob, wait_for_data)
// Nop
//acu(hold,hold) dmux(sa,sa) alu(hold) mac(hold)
// Set the semaphore 0
//acu(clear,clear) dmux(sa,sa) alu(clearsem, 010) mac(clra)
acu(hold,hold) dmux(sa,sa) alu(setsem, 010) mac(hold)
// jmp(eob, wait_for_data) 
acu(clear,clear) dmux(sa,sa) alu(hold) mac(clra) jmp(eob, wait_for_data)    


