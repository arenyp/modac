/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include "DFB_1.h"
#include "constants.h"
#include "coefficients.h"
#include "stereoFilter.h"

extern const uint8 Filter_1_ChannelAFirCoefficients[FILTERTAPS_SIZE];
extern const uint8 Filter_1_ChannelBFirCoefficients[FILTERTAPS_SIZE];
extern uint8 buffer1Input[2 * SNDBUFFERSZ + SWAPBYTES];

/* Variable declarations for the DFB's DMAs */
static uint8 gDMA_FilterIn_Chan;
static uint8 gDMA_FilterIn_TD[2];
#ifndef BYPASS_THE_DFB
static uint8 gDMA_FilterOutH_Chan;
static uint8 gDMA_FilterOutH_TD[2];
static uint8 gDMA_FilterOutL_Chan;
static uint8 gDMA_FilterOutL_TD[2];
#endif

/*
* Starts the DMA transfer of the input buffer to the DFB
* The DFB input DMA stops when completing a TD, reconfigure his TD here
*/
void startFilterInputDMA(int currentBuffer)
{
    CyDmaChDisable(gDMA_FilterIn_Chan);
    if (currentBuffer == 0)
    {
        CyDmaChSetInitialTd(gDMA_FilterIn_Chan, gDMA_FilterIn_TD[0]);
    }
    else
    {
        CyDmaChSetInitialTd(gDMA_FilterIn_Chan, gDMA_FilterIn_TD[1]);
    }
    CyDmaChEnable(gDMA_FilterIn_Chan, 1);
    // Rising hedge
    DFB_1_ClearSemaphores(DFB_1_SEM1); // DMA request starts on rising hedge so whe must be low first
    DFB_1_SetSemaphores(DFB_1_SEM1); // Starts a DMA request
    PWM_1_Start();
}

/*
 * Configure the DFB DMAs
 *
 * The input samples come in like B3 B2 B1 B0  were B0 always 0 (unless you use 32bits samples...)
 * Samples are big endian and the DFB is litte endian. So we swap using the DMA channels.
 *
 */
void DFB_DMAs_Configuration(uint8 *stereoInputBuffer, uint8 *outputHiBuffer, uint8 *outputLoBuffer)
{
#ifndef BYPASS_THE_DFB
    //----------------------------
    // In
    //----------------------------
    gDMA_FilterIn_Chan = DMA_FilterIn_DmaInitialize(DMA_FILTER_BYTES_PER_BURST, DMA_FILTER_REQUEST_PER_BURST, HI16(CYDEV_SRAM_BASE), HI16(DFB_1_STAGEA_PTR));
    gDMA_FilterIn_TD[0] = CyDmaTdAllocate();
    gDMA_FilterIn_TD[1] = CyDmaTdAllocate();
    // The DMA wil stop after SNDBUFFERSZ bytes, it is restarted by the swap DMA interrupt
    CyDmaTdSetConfiguration(gDMA_FilterIn_TD[0], SNDBUFFERSZ, CY_DMA_DISABLE_TD,
        CY_DMA_TD_SWAP_EN | CY_DMA_TD_SWAP_SIZE4 | DMA_FilterIn__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR);
    CyDmaTdSetConfiguration(gDMA_FilterIn_TD[1], SNDBUFFERSZ, CY_DMA_DISABLE_TD,
        CY_DMA_TD_SWAP_EN | CY_DMA_TD_SWAP_SIZE4 | DMA_FilterIn__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR);
    // From DFB hold A register to ram
    CyDmaTdSetAddress(gDMA_FilterIn_TD[0], LO16((uint32)stereoInputBuffer), LO16((uint32)DFB_1_STAGEA_PTR));
    CyDmaTdSetAddress(gDMA_FilterIn_TD[1], LO16((uint32)&stereoInputBuffer[SNDBUFFERSZ]), LO16((uint32)DFB_1_STAGEA_PTR));
	// Associate the TD with the channel
    CyDmaChSetInitialTd(gDMA_FilterIn_Chan, gDMA_FilterIn_TD[0]);
	// Enable it
    CyDmaChEnable(gDMA_FilterIn_Chan, 1);
    //----------------------------
    // Out High frequency
    //----------------------------
    gDMA_FilterOutH_Chan = DMA_FilterOutH_DmaInitialize(DMA_FILTER_BYTES_PER_BURST, DMA_FILTER_REQUEST_PER_BURST, HI16(DFB_1_HOLDA_PTR), HI16(CYDEV_SRAM_BASE));
    gDMA_FilterOutH_TD[0] = CyDmaTdAllocate();
    gDMA_FilterOutH_TD[1] = CyDmaTdAllocate();
    // The output is swaped directly from the Hold register, it causes an unexpecte zero to be first but the output DMA starts one byte ahead
    CyDmaTdSetConfiguration(gDMA_FilterOutH_TD[0], SNDBUFFERSZ, gDMA_FilterOutH_TD[1], 
        CY_DMA_TD_SWAP_EN | CY_DMA_TD_SWAP_SIZE4 | DMA_FilterOutH__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
    CyDmaTdSetConfiguration(gDMA_FilterOutH_TD[1], SNDBUFFERSZ, gDMA_FilterOutH_TD[0], 
        CY_DMA_TD_SWAP_EN | CY_DMA_TD_SWAP_SIZE4 | DMA_FilterOutH__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
    // From DFB hold A register to ram
    CyDmaTdSetAddress(gDMA_FilterOutH_TD[0], LO16((uint32)DFB_1_HOLDA_PTR), LO16((uint32)outputHiBuffer));
    CyDmaTdSetAddress(gDMA_FilterOutH_TD[1], LO16((uint32)DFB_1_HOLDA_PTR), LO16((uint32)&outputHiBuffer[SNDBUFFERSZ]));
	// Associate the TD with the channel
    CyDmaChSetInitialTd(gDMA_FilterOutH_Chan, gDMA_FilterOutH_TD[0]);
	// Enable it
    CyDmaChEnable(gDMA_FilterOutH_Chan, 1);
    //----------------------------
    // Out Low frequency
    //----------------------------
    gDMA_FilterOutL_Chan = DMA_FilterOutL_DmaInitialize(DMA_FILTER_BYTES_PER_BURST, DMA_FILTER_REQUEST_PER_BURST, HI16(DFB_1_HOLDB_PTR), HI16(CYDEV_SRAM_BASE));
    gDMA_FilterOutL_TD[0] = CyDmaTdAllocate();
    gDMA_FilterOutL_TD[1] = CyDmaTdAllocate();
    CyDmaTdSetConfiguration(gDMA_FilterOutL_TD[0], SNDBUFFERSZ, gDMA_FilterOutL_TD[1], 
        CY_DMA_TD_SWAP_EN | CY_DMA_TD_SWAP_SIZE4 | DMA_FilterOutL__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
    CyDmaTdSetConfiguration(gDMA_FilterOutL_TD[1], SNDBUFFERSZ, gDMA_FilterOutL_TD[0], 
        CY_DMA_TD_SWAP_EN | CY_DMA_TD_SWAP_SIZE4 | DMA_FilterOutL__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
    // From DFB hold A register to ram
    CyDmaTdSetAddress(gDMA_FilterOutL_TD[0], LO16((uint32)DFB_1_HOLDB_PTR), LO16((uint32)outputLoBuffer));
    CyDmaTdSetAddress(gDMA_FilterOutL_TD[1], LO16((uint32)DFB_1_HOLDB_PTR), LO16((uint32)&outputLoBuffer[SNDBUFFERSZ]));
	// Associate the TD with the channel
    CyDmaChSetInitialTd(gDMA_FilterOutL_Chan, gDMA_FilterOutL_TD[0]);
	// Enable it
    CyDmaChEnable(gDMA_FilterOutL_Chan, 1);
#endif //BYPASS_THE_DFB
}


/*
 * Filter data ready ISR
 *
 * Inputs the samples
*/
void filterISR()
{
}   

// In main.c
void filteringCompleted();

void initDFBStereoFilter(uint8 *stereoInputBuffer, uint8 *outputHiBuffer, uint8 *outputLoBuffer)
{
    //uint32 DFBRamStartAddress = 0;
    DFB_DMAs_Configuration(stereoInputBuffer, outputHiBuffer, outputLoBuffer);
    /*
    int ret = DFB_1_LoadDataRAMA((uint32*)Filter_1_ChannelAFirCoefficients, &DFBRamStartAddress, FILTERTAPS_SIZE);
    if (ret != DFB_1_SUCCESS)
    {
        while (1);
    }
    int ret = DFB_1_LoadDataRAMA((uint32*)Filter_1_ChannelBFirCoefficients, &DFBRamStartAddress[FILTERTAPS], FILTERTAPS_SIZE);
    if (ret != DFB_1_SUCCESS)
    {
        while (1);
    }
    */
    //isr_DFBFilter_StartEx(filterISR);
    //isr_DMADFBIn_StartEx(filterISR);
    isr_FOutL_StartEx(filteringCompleted); // Starts the output DMAs
    DFB_1_Start();
    //DFB_1_SetCoherency(DFB_1_STGA_KEY_HIGH | DFB_1_STGB_KEY_HIGH | DFB_1_HOLDA_KEY_HIGH | DFB_1_HOLDB_KEY_HIGH);
}

