/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/*
// Structure to keep track of the filtering progess per stereo voice
typedef struct strChannelSync
{
    int    FBSampleCount;  // current processed sample
    int    bufferOffset;   // ring buffer position
    uint8  finished;       // finished filtering
    uint8 *pbuffer;        // current buffer
} tChannelSync;
*/

void sampleSwapDMA(uint8 *InputBuffer, uint8 *outputBuffer);
//void initSampleSwapDMA(uint8 *InputBuffer, uint8 *outputBuffer);
void initDFBStereoFilter(uint8 *stereoInputBuffer, uint8 *outputHiBuffer, uint8 *outputLoBuffer);
//void writeDFBFirstSample(int currentIn);
void startFilterInputDMA(int currentBuffer);
void startEndianSwap(int currentSwapBuffer);

/* [] END OF FILE */
