/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include "usbAudio.h"
#include "constants.h"
#include "inputISR.h"
#include "inputSelector.h"
#include "stereoFilter.h"


#ifdef USE_USBAUDIO

#define USB_INTERFACE_INACTIVE               0
#define USB_INIT_AFTER_ENUMERATION_REQUIRED  1
#define USB_INIT_AFTER_ENUMERATION_COMPLETED 2
    
uint8 USBDeviceState = USB_INTERFACE_INACTIVE;

void setOutputReady();
    
extern uint8 buffer1Input[2 * SNDBUFFERSZ + SWAPBYTES];
extern int currentIn;
extern uint8 buffer3OutputLoFreq[2 * SNDBUFFERSZ + SWAPBYTES];
extern uint8 buffer4OutputHiFreq[2 * SNDBUFFERSZ + SWAPBYTES];

/* UBSFS device constants. */
#define USBFS_AUDIO_DEVICE  (0u)
#define AUDIO_INTERFACE     (1u)
#define AUDIO_OUT_ENDPOINT  (1u)

/* Audio buffer constants. */
#define TRANSFER_SIZE   (96u)
volatile uint16 outIndex = 0u;
volatile uint16 USBBufferIndex = 0u;
volatile int sampleIndex = 0u;

/* Variables used to manage DMA. */
volatile uint8 syncDma = 0u;
volatile uint8 syncDmaCounter = 0u;

extern uint8 TDBuffer1Update[2];
extern uint8 TDBuffer2Update[2];
extern uint8 nextTD;

/*********************************************************************************
*
* Summary:
*
*  The main function performs the following actions:
*   . Starts the USBFS component and waits until the device is got enumerated
*      by the host.
*   . When the PC starts streaming, the DMA starts copying data into the DAC.
*   . None of the synchronization methods is implemented:
*      If the PC (source) transfers data faster than the device (sink), the extra samples are dropped.
*      If the PC (source) transfers data slower than the device (sink), the transfer is stopped and starts after the buffer is half-full.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void usbInit()
{
    //CyGlobalIntEnable;
    // Start USBFS Operation
    // Global interrupts must be enabled at this point
    USBFS_Start(USBFS_AUDIO_DEVICE, USBFS_DWR_VDDD_OPERATION);
}

void USBStart()
{
    //USBFS_S
}

void USBStop()
{
    //USBFS_S
}

uint8 translationBuffer[16 * TRANSFER_SIZE];

/*
* To be called in the for loop of the main function
*/
void USBFSStateHandling(uint8 *pinputBuffer)
{
    unsigned int i;
    uint8 skipNextOut = 0u;
    //static uint8 totalBuffers = 0u;
    
    // Wait for device enumeration.
    if (USBDeviceState == USB_INTERFACE_INACTIVE && USBFS_GetConfiguration() != 0)
    {
        USBDeviceState = USB_INIT_AFTER_ENUMERATION_REQUIRED;
    }
    /* Check if configuration or interface settings are changed. */
    if (0u != USBFS_IsConfigurationChanged())
    {    
        USBDeviceState = USB_INIT_AFTER_ENUMERATION_COMPLETED;
        
        /* Check active alternate setting. */
        uint8 USBFSConfiguration = USBFS_GetConfiguration();
        uint8 interfaceSettings = USBFS_GetInterfaceSetting(AUDIO_INTERFACE);
        if ((USBFSConfiguration != 0u) &&
            (interfaceSettings != 0u))
        {
            /* Alternate settings 1: Audio is streaming. */

            /* Reset variables. */
            USBBufferIndex  = 0u;
            /*
            outIndex = 0u;
            syncDma  = 0u;
            skipNextOut = 0u;
            syncDmaCounter = 0u;*/

            /* Enable OUT endpoint to receive audio stream. */
            USBFS_EnableOutEP(AUDIO_OUT_ENDPOINT); // Audio ON
        }
        else
        {
            /* Alternate settings 0: Audio is not streaming (mute). */
            // "Audio OFF"
        }
    }
    if (USBDeviceState != USB_INIT_AFTER_ENUMERATION_COMPLETED)
    {
        return;
    }
    // Get the state of the audio out endpoint
    if (USBFS_OUT_BUFFER_FULL == USBFS_GetEPState(AUDIO_OUT_ENDPOINT))
    {
        if (skipNextOut == 0)
        {
            uint8 outputBuffer = 0;
            // Copy data from OUT endpoint buffer
            int length = USBFS_GetEPCount(AUDIO_OUT_ENDPOINT);
            uint16 receivedBytes = USBFS_ReadOutEP(AUDIO_OUT_ENDPOINT, translationBuffer, length);
            //
            if (receivedBytes % 6 != 0) // Visual check if the input is not synchronised on a sample boundary
            {   
                PWM_1_Start();
            }
            
            // The USB sends packed 24bits (could be different maybe), but with I2S, samples are received as 32bits values
            // So adapt here
            /*
            int addedSz = receivedBytes + receivedBytes / 3;
            if (sampleIndex < SNDBUFFERSZ && sampleIndex + addedSz >= SNDBUFFERSZ)
            {
                outputBuffer = 1; // Enable the first buffer output
            }
            else if (sampleIndex > SNDBUFFERSZ && sampleIndex + addedSz >= 2 * SNDBUFFERSZ)
            {
                outputBuffer = 2; // The second buffer
            }
            */
            int oldSampleIndex = sampleIndex;
            i = 0;
            while (i < receivedBytes) // TRANSFER_SIZE must be divisible by 3 and by 2 and shorter than the input buffer size
            {
                // Endian swap
                //buffer1Input
                pinputBuffer[sampleIndex + 0] = translationBuffer[i + 2];
                pinputBuffer[sampleIndex + 1] = translationBuffer[i + 1];
                pinputBuffer[sampleIndex + 2] = translationBuffer[i + 0];
                //
                pinputBuffer[sampleIndex + 3] = 0;
                //
                pinputBuffer[sampleIndex + 4] = translationBuffer[i + 5];
                pinputBuffer[sampleIndex + 5] = translationBuffer[i + 4];
                pinputBuffer[sampleIndex + 6] = translationBuffer[i + 3];   
                //
                pinputBuffer[sampleIndex + 7] = 0;
                //
                i += 6;
                sampleIndex += 8;
                // If we are at the end of a 4 Bytes sample, test the buffer limits
                if (sampleIndex >= 2 * SNDBUFFERSZ) // loop to zero
                {
                    sampleIndex = 0;
                }
            }
            
            if (oldSampleIndex < SNDBUFFERSZ && sampleIndex >= SNDBUFFERSZ)
            {
                outputBuffer = 1; // Enable the first buffer output
            }
            else if (oldSampleIndex > SNDBUFFERSZ && sampleIndex < SNDBUFFERSZ)
            {
                outputBuffer = 2; // The second buffer
            }
            // Enable OUT endpoint to receive data from host.
            USBFS_EnableOutEP(AUDIO_OUT_ENDPOINT);
            //-------------------------------------------------------------------
            // Enable DMA transfers when sound buffer is half-full.
            //-------------------------------------------------------------------
            if (outputBuffer != 0 && getCurrentInput() == AUDIOIN_USB)
            {
                /*
                if (outputBuffer == 1)
                {
                    currentIn = 0;
                }*/
                //audioInput_ISR();
                
                
                memcpy(&buffer4OutputHiFreq[currentIn + 1], &buffer1Input[currentIn + 1], SNDBUFFERSZ);                
                memcpy(&buffer3OutputLoFreq[currentIn + 1], &buffer1Input[currentIn + 1], SNDBUFFERSZ);

                filteringCompleted();
                if (currentIn == 0) // The last received buffer is 0
                {
                    nextTD = 0; // Transmit the first TD
                }
                else
                {
                    nextTD = 1; // Transmit the second TD
                }
                TDBuffer1Update[nextTD] = 1;
                TDBuffer2Update[nextTD] = 1;
                // Change the input buffer
                if (currentIn == 0)
                {
                    currentIn = SNDBUFFERSZ; // Change buffer
                }
                else
                {
                    currentIn = 0;
                }
                // If the previous buffers were transfered, then restart a transfer
                //restartDMAOutIfNeeded();
                //resetNextTD();
                
                /*
                if (outputBuffer == 1)
                {
                    outIndex = 0;
                }
                else
                {
                    outIndex = 1;
                }
                */
                /*
                totalBuffers++;
                if (totalBuffers == 40)
                {
                    memcpy(&buffer3OutputLoFreq[currentIn], &pinputBuffer[currentIn], SNDBUFFERSZ);    
                }
                */
                /*
                // Now filter evetything out
#ifdef BYPASS_THE_DFB
                memcpy(&buffer3OutputLoFreq[currentIn], &pinputBuffer[currentIn], SNDBUFFERSZ);
                memcpy(&buffer4OutputHiFreq[currentIn], &pinputBuffer[currentIn], SNDBUFFERSZ);
                filteringCompleted();
#else
                // Start the endian swap of the buffer
                startFilterInputDMA(currentIn);
#endif
                // Change the input buffer
                if (currentIn == 0)
                {
                    currentIn = SNDBUFFERSZ; // Change buffer
                }
                else
                {
                    currentIn = 0;
                }
                setOutputReady();
                */
                // Disable underflow delayed start.
                //syncDma = 1;
            }
        }
        else
        {
            /* Ignore received data from host and arm OUT endpoint
            * without reading if overflow is detected.
            */
            USBFS_EnableOutEP(AUDIO_OUT_ENDPOINT);
            skipNextOut = 0;
            PWM_1_Start();
        }
        /* When internal 32-kHz clock is slower, compare to PC traffic
        * then skip next transfer from PC.
        */
        /*
        if (outIndex == USBBufferIndex)
        {
            skipNextOut = 1;
        }
        */
    }
}

#endif //USE_USBAUDIO

/* [] END OF FILE */
