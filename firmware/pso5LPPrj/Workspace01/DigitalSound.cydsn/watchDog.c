/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include "constants.h"
#include "BToothI2S.h"
#include "audioClock.h"
#include "inputSelector.h"
#include "watchDog.h"

static uint8 blueToothInputReady = 0;


uint8 getBluetoothReady()
{
    return blueToothInputReady;
}

void resetSampleCounter()
{
    SampleCounter_Stop();
    SampleCounter_WriteCounter(0);
    SampleCounter_Enable();
}

inline void readTimerStatus()
{
    /* Read Status register in order to clear the sticky Terminal Count (TC) bit 
	 * in the status register. Note that the function is not called, but rather 
	 * the status is read directly.
	 */
   	Timer_WatchDog_STATUS;
}

// Called 100 times per second
void watchDogTimerRoutine()
{
    readTimerStatus();
    
    unsigned int count = SampleCounter_ReadCounter();

    // Check the input selector
    uint8 currentInput = getCurrentInput();
    
    
    if (currentInput == AUDIOIN_BLUETOOTH)
    {
        /*
        if (count == 0 || count < 1000)
        {
            if (blueToothInputReady != 0)
            {
               // startBlueTooth(0); // Stop the bluetooth input
                blueToothInputReady = 0;
            }        
        }
        else
        */
        {
            if (blueToothInputReady == 0 && count > 100) // Ack
            {
#ifdef WAITFORBLUETOOTHINPUT
                configurePLLForAudioMasterClock(SAMPLERATE48000, PLLBLUETOOTH);
                clockSelect(PLLBLUETOOTH);
                startBlueTooth(1); // Start the bluetooth input
#endif
                blueToothInputReady = 1;
            }
        }
    }
    resetSampleCounter();
    // Restart the down count
    //Timer_WatchDog_WriteCounter(Timer_WatchDog_INIT_PERIOD);
}

// This a timer triggering an interrupt 100 times per second to check for input selection change or bluetooth clock input
void initWatchDogTimer()
{
    // Start the sample counter for the bluetooth chip
    SampleCounter_Start();
    // Start the timer
    isr_WatchDog_StartEx(watchDogTimerRoutine);    
    Timer_WatchDog_Start();
}


/* [] END OF FILE */
