// Written in https://www.onlinegdb.com/online_c++_compiler

#include <stdio.h>
#include <string.h>

#define NBTAPS 16

typedef unsigned char uint8;
typedef long long int64;
typedef int int32;

const uint8 Filter_1_ChannelAFirCoefficients[4 * NBTAPS] = {
  0xFDu, 0xBAu, 0xFFu, 0x00u,	/* Tap(0), -0.00210607051849365 */

  0x97u, 0xE6u, 0xFFu, 0x00u,	/* Tap(1), -0.000775456428527832 */

  0x00u, 0xD5u, 0xB6u, 0x00u,	/* Tap(2), 0.00652194023132324 */

  0x1Eu, 0x44u, 0x03u, 0x00u,	/* Tap(3), 0.0255162715911865 */

  0x2Bu, 0x6Au, 0x07u, 0x00u,	/* Tap(4), 0.0579274892807007 */

  0x46u, 0xA3u, 0x0Cu, 0x00u,	/* Tap(5), 0.0987327098846436 */

  0x09u, 0x89u, 0x11u, 0x00u,	/* Tap(6), 0.136994481086731 */

  0xDDu, 0x82u, 0x14u, 0x00u,	/* Tap(7), 0.160243630409241 */

  0xDDu, 0x82u, 0x14u, 0x00u,	/* Tap(8), 0.160243630409241 */

  0x09u, 0x89u, 0x11u, 0x00u,	/* Tap(9), 0.136994481086731 */

  0x46u, 0xA3u, 0x0Cu, 0x00u,	/* Tap(10), 0.0987327098846436 */

  0x2Bu, 0x6Au, 0x07u, 0x00u,	/* Tap(11), 0.0579274892807007 */

  0x1Eu, 0x44u, 0x03u, 0x00u,	/* Tap(12), 0.0255162715911865 */

  0x00u, 0xD5u, 0xB6u, 0x00u,	/* Tap(13), 0.00652194023132324 */

  0x97u, 0xE6u, 0xFFu, 0x00u,	/* Tap(14), -0.000775456428527832 */

  0xFDu, 0xBAu, 0xFFu, 0x00u,	/* Tap(15), -0.00210607051849365 */
};

const uint8 Filter_1_ChannelBFirCoefficients[4 * NBTAPS] = {
  0x81u, 0xCAu, 0xFFu, 0x00u,	/* Tap(0), -0.00163257122039795 */

  0x9Bu, 0x18u, 0x01u, 0x00u,	/* Tap(1), 0.00856339931488037 */

  0x19u, 0x04u, 0xFDu, 0x00u,	/* Tap(2), -0.0233124494552612 */

  0x51u, 0xE9u, 0x00u, 0x00u,	/* Tap(3), 0.00712025165557861 */

  0x62u, 0x0Eu, 0xF1u, 0x00u,	/* Tap(4), -0.116748571395874 */

  0xB9u, 0xB1u, 0x00u, 0x00u,	/* Tap(5), 0.00542366504669189 */

  0x1Fu, 0xAFu, 0xD4u, 0x00u,	/* Tap(6), -0.338405728340149 */

  0x98u, 0x9Cu, 0x3Eu, 0x00u,	/* Tap(7), 0.489153861999512 */

  0x98u, 0x9Cu, 0x3Eu, 0x00u,	/* Tap(8), 0.489153861999512 */

  0x1Fu, 0xAFu, 0xD4u, 0x00u,	/* Tap(9), -0.338405728340149 */

  0xB9u, 0xB1u, 0x00u, 0x00u,	/* Tap(10), 0.00542366504669189 */

  0x62u, 0x0Eu, 0xF1u, 0x00u,	/* Tap(11), -0.116748571395874 */

  0x51u, 0xE9u, 0x00u, 0x00u,	/* Tap(12), 0.00712025165557861 */

  0x19u, 0x04u, 0xFDu, 0x00u,	/* Tap(13), -0.0233124494552612 */

  0x9Bu, 0x18u, 0x01u, 0x00u,	/* Tap(14), 0.00856339931488037 */

  0x81u, 0xCAu, 0xFFu, 0x00u,	/* Tap(15), -0.00163257122039795 */
};

int32 testData[] = {
0x00E168,
0x00432A,
0x00000B,
0x00000C,
0x00000D,
0x00000E,
0x00000F,
0x000010,
0x000011,
0x000012,
0x000013,
0x000014,
0x000015,
0x000016,
0x000017,
0x000018,
0x000019,
0x00001A,
0x00000D,
0x00000E,
0x00000F,
0x000010,
0x000011,
0x000012,
0x000013,
0x000014,
0x000015,
0x000016,
0x000017,
0x000018,
0x000019,
0x00001A
};

int64 getTap (bool bATap, int iTap)
{
  int64 res = 0;

  if (bATap)
    {
      memcpy(&res, &Filter_1_ChannelAFirCoefficients[4 * iTap], 4);
    }
  else
    {
      memcpy(&res, &Filter_1_ChannelBFirCoefficients[4 * iTap], 4);
    }
  // Sign extend to64bits
  if (res & 0x800000)
    {
      res = res | 0xFFFFFFFFFF000000;
    }
  return res;
}

char* printSigned24b(int32 value)
{
  static char result[1024];
  float decimalValue = 0;

  if (value != 0)
    {
      decimalValue = value;
      decimalValue = decimalValue / (float)0x7FFFFF;
    }
  sprintf(result, "%.7f", decimalValue);
  return result;
}

void printOp(int i, int64 accA, int64 mul, int64 tapA, int64 lastSample)
{
  int accAi = accA;
  int tapAi = tapA;
  int lastS = lastSample;
  char resTap[1024];
  char res[1024];
  char resSample[1024];
  char mulRes[1024];
  
  strncpy(res, printSigned24b(accAi), 1024);
  strncpy(resTap, printSigned24b(tapAi), 1024);
  strncpy(resSample, printSigned24b(lastS), 1024);
  strncpy(mulRes, printSigned24b(mul >> 23), 1024);
  printf(" %2d: accA 0x%06x %s; %s = 0x%06x * 0x%06x = %s * %s\n", i, accAi, res, mulRes,
	 lastS, tapAi, resSample, resTap);
}

int main ()
{
  int64 accA;
  int64 accB;
  int64 mulRes;
  int32 LastSamples[NBTAPS];
  int32 currentIndex = 0;
  int32 HoldA;	
  int32 HoldB;
 
  // Empty past samples
  memset(LastSamples, 0, sizeof(LastSamples));
  // Input samples
  for (unsigned int sample = 0; sample < sizeof (testData) / sizeof (int64); sample++)
    {
      LastSamples[currentIndex] = testData[sample];
      printf("Sample %d\n", sample);
      // Filter 1
      accA = accB = 0;
      for (int i = 0; i < NBTAPS; i++)
	{
	  int64 tapA = getTap(true, i);
	  int sampleIndex = (currentIndex + i) % NBTAPS;
	  mulRes = (tapA * (int64)LastSamples[sampleIndex ]); // 48bits result
	  accA += mulRes;
	  printOp(i, accA >> 23, mulRes, tapA,  LastSamples[sampleIndex ]);
	}
      accA = accA >> 23;
      HoldA = accA;
      printf("-> HoldA 0x%x %s\n", HoldA, printSigned24b(HoldA));
      for (int i = 0; i < NBTAPS; i++)
	{	
	  int64 tapB = getTap(false, i);
	  int sampleIndex = (currentIndex + i) % NBTAPS;
	  mulRes = (tapB * (int64)LastSamples[sampleIndex ]); // 48bits result
	  accB += mulRes;
	  printOp(i, accB >> 23, mulRes, tapB,  LastSamples[sampleIndex ]);
	}
      accB = accB >> 23;
      HoldB = accB;
      printf("-> HoldB 0x%x %s\n", HoldB, printSigned24b(HoldB));
      //
      currentIndex = (currentIndex + NBTAPS - 1) % NBTAPS;
    }
  return 0;
}

