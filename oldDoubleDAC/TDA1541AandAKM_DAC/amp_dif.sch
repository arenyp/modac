EESchema Schematic File Version 4
LIBS:dac-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title ""
Date "7 dec 2011"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4300 5800 4150 5800
Wire Wire Line
	4150 5800 4150 5750
Wire Wire Line
	2400 2700 1950 2700
Wire Wire Line
	4150 3400 3650 3400
Wire Wire Line
	3650 3400 3000 3400
Wire Wire Line
	3000 3400 3000 3300
Wire Wire Line
	3050 2700 3000 2700
Wire Wire Line
	3000 2700 2900 2700
Connection ~ 3000 1450
Wire Wire Line
	2900 1450 3000 1450
Wire Wire Line
	3000 1450 3900 1450
Wire Wire Line
	3000 1750 3050 1750
Connection ~ 4850 2600
Wire Wire Line
	4850 1750 4350 1750
Wire Wire Line
	3650 1750 3650 2500
Wire Wire Line
	3650 2500 3750 2500
Wire Wire Line
	4300 2150 4150 2150
Wire Wire Line
	4150 2150 4150 2200
Wire Wire Line
	4150 3000 4150 3050
Wire Wire Line
	4150 3050 4300 3050
Wire Wire Line
	4750 2600 4850 2600
Wire Wire Line
	4850 2600 5750 2600
Wire Wire Line
	3550 1750 3650 1750
Wire Wire Line
	3650 1750 3950 1750
Connection ~ 3650 1750
Wire Wire Line
	4400 1450 4850 1450
Wire Wire Line
	4850 1450 4850 1750
Wire Wire Line
	4850 1750 4850 2600
Connection ~ 4850 1750
Connection ~ 3000 1750
Connection ~ 3000 2700
Wire Wire Line
	3550 2700 3650 2700
Wire Wire Line
	3650 2700 3750 2700
Wire Wire Line
	3650 2800 3650 2700
Connection ~ 3650 2700
Wire Wire Line
	3650 3200 3650 3400
Connection ~ 3650 3400
Wire Wire Line
	3000 2050 3000 1750
Wire Wire Line
	3000 1750 3000 1450
Wire Wire Line
	3000 2800 3000 2700
Wire Wire Line
	3000 2700 3000 2450
Wire Wire Line
	2400 1450 1950 1450
Wire Wire Line
	2400 5450 1950 5450
Wire Wire Line
	4150 6150 3650 6150
Wire Wire Line
	3650 6150 3000 6150
Wire Wire Line
	3000 6150 3000 6050
Wire Wire Line
	3050 5450 3000 5450
Wire Wire Line
	3000 5450 2900 5450
Connection ~ 3000 4200
Wire Wire Line
	2900 4200 3000 4200
Wire Wire Line
	3000 4200 3900 4200
Wire Wire Line
	3000 4500 3050 4500
Connection ~ 4850 5350
Wire Wire Line
	4850 4500 4350 4500
Wire Wire Line
	3650 4500 3650 5250
Wire Wire Line
	3650 5250 3750 5250
Wire Wire Line
	4300 4900 4150 4900
Wire Wire Line
	4150 4900 4150 4950
Wire Wire Line
	4750 5350 4850 5350
Wire Wire Line
	4850 5350 5750 5350
Wire Wire Line
	3550 4500 3650 4500
Wire Wire Line
	3650 4500 3950 4500
Connection ~ 3650 4500
Wire Wire Line
	4400 4200 4850 4200
Wire Wire Line
	4850 4200 4850 4500
Wire Wire Line
	4850 4500 4850 5350
Connection ~ 4850 4500
Connection ~ 3000 4500
Connection ~ 3000 5450
Wire Wire Line
	3550 5450 3650 5450
Wire Wire Line
	3650 5450 3750 5450
Wire Wire Line
	3650 5550 3650 5450
Connection ~ 3650 5450
Wire Wire Line
	3650 5950 3650 6150
Connection ~ 3650 6150
Wire Wire Line
	3000 4800 3000 4500
Wire Wire Line
	3000 4500 3000 4200
Wire Wire Line
	3000 5550 3000 5450
Wire Wire Line
	3000 5450 3000 5200
Wire Wire Line
	2400 4200 1950 4200
Text HLabel 1950 1450 0    60   Input ~ 0
AOUTL-
Text HLabel 1950 2700 0    60   Input ~ 0
AOUTL+
Text HLabel 5750 2600 2    60   Input ~ 0
AOUTL
Text HLabel 5750 5350 2    60   Input ~ 0
AOUTR
Text HLabel 1950 5450 0    60   Input ~ 0
AOUTR+
Text HLabel 1950 4200 0    60   Input ~ 0
AOUTR-
Text GLabel 4150 6150 2    60   Input ~ 0
GND
Text GLabel 4300 5800 2    60   Input ~ 0
+15VA
Text GLabel 4300 4900 2    60   Input ~ 0
-15VA
$Comp
L dac-rescue:OP275-RESCUE-dac U6
U 2 1 4E29B635
P 4250 5350
AR Path="/4E29B474/4E29B635" Ref="U6"  Part="2" 
AR Path="/5BE593FD/5BE9B629/4E29B635" Ref="U?"  Part="2" 
F 0 "U?" H 4200 5550 60  0000 L CNN
F 1 "OP275" H 4200 5100 60  0000 L CNN
F 2 "" H 4250 5350 60  0001 C CNN
F 3 "" H 4250 5350 60  0001 C CNN
	2    4250 5350
	1    0    0    1   
$EndComp
$Comp
L dac-rescue:R-RESCUE-dac R51
U 1 1 4E29B634
P 3300 5450
AR Path="/4E29B474/4E29B634" Ref="R51"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B634" Ref="R?"  Part="1" 
F 0 "R?" V 3380 5450 50  0000 C CNN
F 1 "150R" V 3300 5450 50  0000 C CNN
F 2 "" H 3300 5450 60  0001 C CNN
F 3 "" H 3300 5450 60  0001 C CNN
	1    3300 5450
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:R-RESCUE-dac R50
U 1 1 4E29B633
P 3300 4500
AR Path="/4E29B474/4E29B633" Ref="R50"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B633" Ref="R?"  Part="1" 
F 0 "R?" V 3380 4500 50  0000 C CNN
F 1 "150R" V 3300 4500 50  0000 C CNN
F 2 "" H 3300 4500 60  0001 C CNN
F 3 "" H 3300 4500 60  0001 C CNN
	1    3300 4500
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:C-RESCUE-dac C59
U 1 1 4E29B632
P 3000 5000
AR Path="/4E29B474/4E29B632" Ref="C59"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B632" Ref="C?"  Part="1" 
F 0 "C?" H 3050 5100 50  0000 L CNN
F 1 "3,3n" H 3050 4900 50  0000 L CNN
F 2 "" H 3000 5000 60  0001 C CNN
F 3 "" H 3000 5000 60  0001 C CNN
	1    3000 5000
	1    0    0    -1  
$EndComp
$Comp
L dac-rescue:C-RESCUE-dac C63
U 1 1 4E29B631
P 4150 4500
AR Path="/4E29B474/4E29B631" Ref="C63"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B631" Ref="C?"  Part="1" 
F 0 "C?" H 4200 4600 50  0000 L CNN
F 1 "680p" H 4200 4400 50  0000 L CNN
F 2 "" H 4150 4500 60  0001 C CNN
F 3 "" H 4150 4500 60  0001 C CNN
	1    4150 4500
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:R-RESCUE-dac R47
U 1 1 4E29B630
P 3000 5800
AR Path="/4E29B474/4E29B630" Ref="R47"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B630" Ref="R?"  Part="1" 
F 0 "R?" V 3080 5800 50  0000 C CNN
F 1 "2,4K" V 3000 5800 50  0000 C CNN
F 2 "" H 3000 5800 60  0001 C CNN
F 3 "" H 3000 5800 60  0001 C CNN
	1    3000 5800
	1    0    0    -1  
$EndComp
$Comp
L dac-rescue:R-RESCUE-dac R45
U 1 1 4E29B62F
P 2650 5450
AR Path="/4E29B474/4E29B62F" Ref="R45"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B62F" Ref="R?"  Part="1" 
F 0 "R?" V 2730 5450 50  0000 C CNN
F 1 "2,4K" V 2650 5450 50  0000 C CNN
F 2 "" H 2650 5450 60  0001 C CNN
F 3 "" H 2650 5450 60  0001 C CNN
	1    2650 5450
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:R-RESCUE-dac R44
U 1 1 4E29B62E
P 2650 4200
AR Path="/4E29B474/4E29B62E" Ref="R44"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B62E" Ref="R?"  Part="1" 
F 0 "R?" V 2730 4200 50  0000 C CNN
F 1 "2,4K" V 2650 4200 50  0000 C CNN
F 2 "" H 2650 4200 60  0001 C CNN
F 3 "" H 2650 4200 60  0001 C CNN
	1    2650 4200
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:R-RESCUE-dac R53
U 1 1 4E29B62D
P 4150 4200
AR Path="/4E29B474/4E29B62D" Ref="R53"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B62D" Ref="R?"  Part="1" 
F 0 "R?" V 4230 4200 50  0000 C CNN
F 1 "2,4K" V 4150 4200 50  0000 C CNN
F 2 "" H 4150 4200 60  0001 C CNN
F 3 "" H 4150 4200 60  0001 C CNN
	1    4150 4200
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:C-RESCUE-dac C61
U 1 1 4E29B62C
P 3650 5750
AR Path="/4E29B474/4E29B62C" Ref="C61"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B62C" Ref="C?"  Part="1" 
F 0 "C?" H 3700 5850 50  0000 L CNN
F 1 "680p" H 3700 5650 50  0000 L CNN
F 2 "" H 3650 5750 60  0001 C CNN
F 3 "" H 3650 5750 60  0001 C CNN
	1    3650 5750
	-1   0    0    1   
$EndComp
Text GLabel 4150 3400 2    60   Input ~ 0
GND
Text GLabel 4300 3050 2    60   Input ~ 0
+15VA
Text GLabel 4300 2150 2    60   Input ~ 0
-15VA
$Comp
L dac-rescue:OP275-RESCUE-dac U6
U 1 1 4E29B4ED
P 4250 2600
AR Path="/4E29B474/4E29B4ED" Ref="U6"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B4ED" Ref="U?"  Part="1" 
F 0 "U?" H 4200 2800 60  0000 L CNN
F 1 "OP275" H 4200 2350 60  0000 L CNN
F 2 "" H 4250 2600 60  0001 C CNN
F 3 "" H 4250 2600 60  0001 C CNN
	1    4250 2600
	1    0    0    1   
$EndComp
$Comp
L dac-rescue:R-RESCUE-dac R49
U 1 1 4E29B4E0
P 3300 2700
AR Path="/4E29B474/4E29B4E0" Ref="R49"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B4E0" Ref="R?"  Part="1" 
F 0 "R?" V 3380 2700 50  0000 C CNN
F 1 "150R" V 3300 2700 50  0000 C CNN
F 2 "" H 3300 2700 60  0001 C CNN
F 3 "" H 3300 2700 60  0001 C CNN
	1    3300 2700
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:R-RESCUE-dac R48
U 1 1 4E29B4DB
P 3300 1750
AR Path="/4E29B474/4E29B4DB" Ref="R48"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B4DB" Ref="R?"  Part="1" 
F 0 "R?" V 3380 1750 50  0000 C CNN
F 1 "150R" V 3300 1750 50  0000 C CNN
F 2 "" H 3300 1750 60  0001 C CNN
F 3 "" H 3300 1750 60  0001 C CNN
	1    3300 1750
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:C-RESCUE-dac C58
U 1 1 4E29B4D0
P 3000 2250
AR Path="/4E29B474/4E29B4D0" Ref="C58"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B4D0" Ref="C?"  Part="1" 
F 0 "C?" H 3050 2350 50  0000 L CNN
F 1 "3,3n" H 3050 2150 50  0000 L CNN
F 2 "" H 3000 2250 60  0001 C CNN
F 3 "" H 3000 2250 60  0001 C CNN
	1    3000 2250
	1    0    0    -1  
$EndComp
$Comp
L dac-rescue:C-RESCUE-dac C62
U 1 1 4E29B4C0
P 4150 1750
AR Path="/4E29B474/4E29B4C0" Ref="C62"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B4C0" Ref="C?"  Part="1" 
F 0 "C?" H 4200 1850 50  0000 L CNN
F 1 "680p" H 4200 1650 50  0000 L CNN
F 2 "" H 4150 1750 60  0001 C CNN
F 3 "" H 4150 1750 60  0001 C CNN
	1    4150 1750
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:R-RESCUE-dac R46
U 1 1 4E29B4B8
P 3000 3050
AR Path="/4E29B474/4E29B4B8" Ref="R46"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B4B8" Ref="R?"  Part="1" 
F 0 "R?" V 3080 3050 50  0000 C CNN
F 1 "2,4K" V 3000 3050 50  0000 C CNN
F 2 "" H 3000 3050 60  0001 C CNN
F 3 "" H 3000 3050 60  0001 C CNN
	1    3000 3050
	1    0    0    -1  
$EndComp
$Comp
L dac-rescue:R-RESCUE-dac R43
U 1 1 4E29B4B0
P 2650 2700
AR Path="/4E29B474/4E29B4B0" Ref="R43"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B4B0" Ref="R?"  Part="1" 
F 0 "R?" V 2730 2700 50  0000 C CNN
F 1 "2,4K" V 2650 2700 50  0000 C CNN
F 2 "" H 2650 2700 60  0001 C CNN
F 3 "" H 2650 2700 60  0001 C CNN
	1    2650 2700
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:R-RESCUE-dac R42
U 1 1 4E29B4AD
P 2650 1450
AR Path="/4E29B474/4E29B4AD" Ref="R42"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B4AD" Ref="R?"  Part="1" 
F 0 "R?" V 2730 1450 50  0000 C CNN
F 1 "2,4K" V 2650 1450 50  0000 C CNN
F 2 "" H 2650 1450 60  0001 C CNN
F 3 "" H 2650 1450 60  0001 C CNN
	1    2650 1450
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:R-RESCUE-dac R52
U 1 1 4E29B49F
P 4150 1450
AR Path="/4E29B474/4E29B49F" Ref="R52"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B49F" Ref="R?"  Part="1" 
F 0 "R?" V 4230 1450 50  0000 C CNN
F 1 "2,4K" V 4150 1450 50  0000 C CNN
F 2 "" H 4150 1450 60  0001 C CNN
F 3 "" H 4150 1450 60  0001 C CNN
	1    4150 1450
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:C-RESCUE-dac C60
U 1 1 4E29B499
P 3650 3000
AR Path="/4E29B474/4E29B499" Ref="C60"  Part="1" 
AR Path="/5BE593FD/5BE9B629/4E29B499" Ref="C?"  Part="1" 
F 0 "C?" H 3700 3100 50  0000 L CNN
F 1 "680p" H 3700 2900 50  0000 L CNN
F 2 "" H 3650 3000 60  0001 C CNN
F 3 "" H 3650 3000 60  0001 C CNN
	1    3650 3000
	-1   0    0    1   
$EndComp
Wire Wire Line
	8750 2700 8900 2700
Text GLabel 8900 2700 2    60   Input ~ 0
+15VA
Wire Wire Line
	8750 2700 8750 2600
Wire Wire Line
	9450 2200 9850 2200
Wire Wire Line
	8350 2300 8200 2300
Wire Wire Line
	8200 2300 8200 2650
Wire Wire Line
	8200 2650 7750 2650
Text GLabel 7750 2650 0    60   Input ~ 0
GND
Wire Wire Line
	9450 950  9450 1300
$Comp
L dac-rescue:OP275-RESCUE-dac U?
U 1 1 5BE862D4
P 8850 2200
AR Path="/4E29B474/5BE862D4" Ref="U?"  Part="1" 
AR Path="/5BE593FD/5BE9B629/5BE862D4" Ref="U?"  Part="1" 
F 0 "U?" H 8800 2400 60  0000 L CNN
F 1 "OP275" H 8800 1950 60  0000 L CNN
F 2 "" H 8850 2200 60  0001 C CNN
F 3 "" H 8850 2200 60  0001 C CNN
	1    8850 2200
	1    0    0    1   
$EndComp
Text GLabel 8900 1700 2    60   Input ~ 0
-15VA
Wire Wire Line
	8900 1700 8750 1700
Wire Wire Line
	8750 1700 8750 1800
$Comp
L dac-rescue:R-RESCUE-dac R?
U 1 1 5BE862DE
P 8800 1300
AR Path="/4E29B474/5BE862DE" Ref="R?"  Part="1" 
AR Path="/5BE593FD/5BE9B629/5BE862DE" Ref="R?"  Part="1" 
F 0 "R?" V 8900 1250 50  0000 C CNN
F 1 "1,8K" V 8800 1300 50  0000 C CNN
F 2 "" H 8800 1300 60  0001 C CNN
F 3 "" H 8800 1300 60  0001 C CNN
	1    8800 1300
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:C-RESCUE-dac C?
U 1 1 5BE862E5
P 8750 950
AR Path="/4E29B474/5BE862E5" Ref="C?"  Part="1" 
AR Path="/5BE593FD/5BE9B629/5BE862E5" Ref="C?"  Part="1" 
F 0 "C?" H 8800 1050 50  0000 L CNN
F 1 "2,2nF" H 8800 850 50  0000 L CNN
F 2 "" H 8750 950 60  0001 C CNN
F 3 "" H 8750 950 60  0001 C CNN
	1    8750 950 
	0    1    1    0   
$EndComp
Wire Wire Line
	9350 2200 9450 2200
Wire Wire Line
	8950 950  9450 950 
Wire Wire Line
	9050 1300 9450 1300
Wire Wire Line
	8550 1300 8200 1300
Wire Wire Line
	8200 1300 8200 2100
Wire Wire Line
	8350 2100 8200 2100
Connection ~ 8200 2100
Wire Wire Line
	8200 2100 7750 2100
Wire Wire Line
	8200 950  8200 1300
Wire Wire Line
	8200 950  8550 950 
Connection ~ 8200 1300
$Comp
L dac-rescue:OP275-RESCUE-dac U?
U 1 1 5BE862F8
P 8850 2200
AR Path="/4E29B474/5BE862F8" Ref="U?"  Part="1" 
AR Path="/5BE593FD/5BE9B629/5BE862F8" Ref="U?"  Part="1" 
F 0 "U?" H 8800 2400 60  0000 L CNN
F 1 "OP275" H 8800 1950 60  0000 L CNN
F 2 "" H 8850 2200 60  0001 C CNN
F 3 "" H 8850 2200 60  0001 C CNN
	1    8850 2200
	1    0    0    1   
$EndComp
Text GLabel 8900 1700 2    60   Input ~ 0
-15VA
$Comp
L dac-rescue:R-RESCUE-dac R?
U 1 1 5BE86302
P 8800 1300
AR Path="/4E29B474/5BE86302" Ref="R?"  Part="1" 
AR Path="/5BE593FD/5BE9B629/5BE86302" Ref="R?"  Part="1" 
F 0 "R?" V 8900 1250 50  0000 C CNN
F 1 "1,8K" V 8800 1300 50  0000 C CNN
F 2 "" H 8800 1300 60  0001 C CNN
F 3 "" H 8800 1300 60  0001 C CNN
	1    8800 1300
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:C-RESCUE-dac C?
U 1 1 5BE86309
P 8750 950
AR Path="/4E29B474/5BE86309" Ref="C?"  Part="1" 
AR Path="/5BE593FD/5BE9B629/5BE86309" Ref="C?"  Part="1" 
F 0 "C?" H 8800 1050 50  0000 L CNN
F 1 "2,2nF" H 8800 850 50  0000 L CNN
F 2 "" H 8750 950 60  0001 C CNN
F 3 "" H 8750 950 60  0001 C CNN
	1    8750 950 
	0    1    1    0   
$EndComp
Connection ~ 9450 1300
Wire Wire Line
	9450 1300 9450 2200
Connection ~ 9450 2200
Wire Wire Line
	8750 4850 8900 4850
Text GLabel 8900 4850 2    60   Input ~ 0
+15VA
Wire Wire Line
	8750 4850 8750 4750
Wire Wire Line
	9450 4350 9850 4350
Wire Wire Line
	8350 4450 8200 4450
Wire Wire Line
	8200 4450 8200 4800
Wire Wire Line
	8200 4800 7750 4800
Text GLabel 7750 4800 0    60   Input ~ 0
GND
Wire Wire Line
	9450 3100 9450 3450
$Comp
L dac-rescue:OP275-RESCUE-dac U?
U 1 1 5BE8A80A
P 8850 4350
AR Path="/4E29B474/5BE8A80A" Ref="U?"  Part="1" 
AR Path="/5BE593FD/5BE9B629/5BE8A80A" Ref="U?"  Part="1" 
F 0 "U?" H 8800 4550 60  0000 L CNN
F 1 "OP275" H 8800 4100 60  0000 L CNN
F 2 "" H 8850 4350 60  0001 C CNN
F 3 "" H 8850 4350 60  0001 C CNN
	1    8850 4350
	1    0    0    1   
$EndComp
Text GLabel 8900 3850 2    60   Input ~ 0
-15VA
Wire Wire Line
	8900 3850 8750 3850
Wire Wire Line
	8750 3850 8750 3950
$Comp
L dac-rescue:R-RESCUE-dac R?
U 1 1 5BE8A814
P 8800 3450
AR Path="/4E29B474/5BE8A814" Ref="R?"  Part="1" 
AR Path="/5BE593FD/5BE9B629/5BE8A814" Ref="R?"  Part="1" 
F 0 "R?" V 8900 3400 50  0000 C CNN
F 1 "1,8K" V 8800 3450 50  0000 C CNN
F 2 "" H 8800 3450 60  0001 C CNN
F 3 "" H 8800 3450 60  0001 C CNN
	1    8800 3450
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:C-RESCUE-dac C?
U 1 1 5BE8A81B
P 8750 3100
AR Path="/4E29B474/5BE8A81B" Ref="C?"  Part="1" 
AR Path="/5BE593FD/5BE9B629/5BE8A81B" Ref="C?"  Part="1" 
F 0 "C?" H 8800 3200 50  0000 L CNN
F 1 "2,2nF" H 8800 3000 50  0000 L CNN
F 2 "" H 8750 3100 60  0001 C CNN
F 3 "" H 8750 3100 60  0001 C CNN
	1    8750 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	9350 4350 9450 4350
Wire Wire Line
	8950 3100 9450 3100
Wire Wire Line
	9050 3450 9450 3450
Wire Wire Line
	8550 3450 8200 3450
Wire Wire Line
	8200 3450 8200 4250
Wire Wire Line
	8350 4250 8200 4250
Connection ~ 8200 4250
Wire Wire Line
	8200 4250 7750 4250
Wire Wire Line
	8200 3100 8200 3450
Wire Wire Line
	8200 3100 8550 3100
Connection ~ 8200 3450
$Comp
L dac-rescue:OP275-RESCUE-dac U?
U 1 1 5BE8A82E
P 8850 4350
AR Path="/4E29B474/5BE8A82E" Ref="U?"  Part="1" 
AR Path="/5BE593FD/5BE9B629/5BE8A82E" Ref="U?"  Part="1" 
F 0 "U?" H 8800 4550 60  0000 L CNN
F 1 "OP275" H 8800 4100 60  0000 L CNN
F 2 "" H 8850 4350 60  0001 C CNN
F 3 "" H 8850 4350 60  0001 C CNN
	1    8850 4350
	1    0    0    1   
$EndComp
Text GLabel 8900 3850 2    60   Input ~ 0
-15VA
$Comp
L dac-rescue:R-RESCUE-dac R?
U 1 1 5BE8A836
P 8800 3450
AR Path="/4E29B474/5BE8A836" Ref="R?"  Part="1" 
AR Path="/5BE593FD/5BE9B629/5BE8A836" Ref="R?"  Part="1" 
F 0 "R?" V 8900 3400 50  0000 C CNN
F 1 "1,8K" V 8800 3450 50  0000 C CNN
F 2 "" H 8800 3450 60  0001 C CNN
F 3 "" H 8800 3450 60  0001 C CNN
	1    8800 3450
	0    1    1    0   
$EndComp
$Comp
L dac-rescue:C-RESCUE-dac C?
U 1 1 5BE8A83D
P 8750 3100
AR Path="/4E29B474/5BE8A83D" Ref="C?"  Part="1" 
AR Path="/5BE593FD/5BE9B629/5BE8A83D" Ref="C?"  Part="1" 
F 0 "C?" H 8800 3200 50  0000 L CNN
F 1 "2,2nF" H 8800 3000 50  0000 L CNN
F 2 "" H 8750 3100 60  0001 C CNN
F 3 "" H 8750 3100 60  0001 C CNN
	1    8750 3100
	0    1    1    0   
$EndComp
Connection ~ 9450 3450
Wire Wire Line
	9450 3450 9450 4350
Connection ~ 9450 4350
Text HLabel 7750 2100 0    50   Input ~ 0
TDAOL
Text HLabel 9850 2200 2    50   Input ~ 0
OL
Text HLabel 9850 4350 2    50   Input ~ 0
OR
Text HLabel 7750 4250 0    50   Input ~ 0
TDAOR
$EndSCHEMATC
