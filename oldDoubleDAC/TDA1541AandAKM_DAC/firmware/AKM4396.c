

#include <avr/io.h>
#include <util/delay.h>

#include "main.h"
#include "AKM4396.h"

t_AKMregs g_AKMregs;

void AKM_CLK(void)
{
  CBI(PORTD, CCLK);
  _delay_ms(1);
  SBI(PORTD, CCLK); // Data is latched here
  _delay_ms(1);
}

void AKM_BITWR(unsigned char bit)
{
  (bit != 0)? SBI(PORTD, CDTI) : CBI(PORTD, CDTI);
  AKM_CLK();
}

unsigned char write_AKM(unsigned char address, unsigned char value)
{
  unsigned char i;

  SBI(PORTD, CCLK); // Clock to 1
  _delay_ms(1);
  CBI(PORTD, AK4396CS); // Chip select/ to 0
  // C1 = 0, C0 = 0
  AKM_BITWR(0);
  AKM_BITWR(0);
  // R/W to write: 1
  AKM_BITWR(1);
  // Write the register address
  for (i = 4; i > 0; i--)
    {
      AKM_BITWR((address >> i) & 1);
    }
  AKM_BITWR(address & 1);
  // Write the data
  for (i = 7; i > 0; i--)
    {
      AKM_BITWR((value >> i) & 1);
    }
  AKM_BITWR(value & 1);
  SBI(PORTD, AK4396CS); // Chip select/ to 1
  return 0;
}

void mute_AK4396(char mute)
{
  if (mute)
    g_AKMregs.ctrl2 = g_AKMregs.ctrl2 | 1;
  else
    g_AKMregs.ctrl2 = g_AKMregs.ctrl2 & (~1);
  write_AKM(AKMCTRL2, g_AKMregs.ctrl2);
}

void reset_AK4396(void)
{
  CBI(PORTD, POWERDOWN);
  _delay_ms_S(5);
  SBI(PORTD, POWERDOWN);
  _delay_ms_S(5);
}

void Config_AK4396(int frequency)
{
  // Set the structure to the default values because the
  // component can only be written
  g_AKMregs.ctrl1 = 0x05;
  g_AKMregs.ctrl2 = 0x02;
  g_AKMregs.ctrl3 = 0x00; // PCM mode selection (not changed)
  g_AKMregs.Lattenuation = 0xFF; // volume maximum
  g_AKMregs.Rattenuation = 0xFF;
  // Write the frequency


  // Reset the ic once at power up
  reset_AK4396();
  // AKS 0 0 0 0 MODE2 MODE1 MODE0 RSTN
  // 24bits I2s: 011
  g_AKMregs.ctrl1 = 0b00000111;
  write_AKM(AKMCTRL1, g_AKMregs.ctrl1);
  g_AKMregs.ctrl2 = 0b00000010;
  write_AKM(AKMCTRL2, g_AKMregs.ctrl2);

  // Mute
  //mute_AK4396(1);
}

void setRollOffFilter(char filterslow)
{
  static char prev = 0;

  if (filterslow != prev)
    {
      if (filterslow)
	g_AKMregs.ctrl2 |=  0b00100000;
      else
	g_AKMregs.ctrl2 &= ~0b00100000;
      write_AKM(AKMCTRL2, g_AKMregs.ctrl2);
      prev = filterslow;
    }
}

// Sampling speed control (table 6)
void update_AKM_DFS0(char f96khz)
{
  if (f96khz)
    {
      if ((g_AKMregs.ctrl2 & DOUBLE_SPEED) == 0) // Not yet in double speed
	{
	  mute_AK4396(1);
	  _delay_ms_S(20);
	  g_AKMregs.ctrl2 |= DOUBLE_SPEED;
	  write_AKM(AKMCTRL2, g_AKMregs.ctrl2);
	  mute_AK4396(0);
	}
    }
  else
      if ((g_AKMregs.ctrl2 & DOUBLE_SPEED) != 0) // in double speed
	{
	  mute_AK4396(1);
	  _delay_ms_S(20);
	  g_AKMregs.ctrl2 &= ~DOUBLE_SPEED;
	  write_AKM(AKMCTRL2, g_AKMregs.ctrl2);
	  mute_AK4396(0);
	}
}

