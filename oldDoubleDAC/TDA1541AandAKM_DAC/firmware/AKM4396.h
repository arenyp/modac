
#define AK4396_ADDRESS  0x00

// AKM4396 register
#define AKMCTRL1             0x00
#define AKMCTRL2             0x01
#define AKMCTRL3             0x02
#define AKML_ATTEN           0x03
#define AKMR_ATTEN           0x04

#define NORMAL_SPEED  0x00 // 30 - 54   Khz
#define DOUBLE_SPEED  0x08 // 54 - 108  Khz
#define QUAD_SPEED    0x10 // 120 - 216 Khz

typedef struct s_AKMregs
{
  unsigned char ctrl1;
  unsigned char ctrl2;
  unsigned char ctrl3;
  unsigned char Lattenuation;
  unsigned char Rattenuation;
}              t_AKMregs;

void Config_AK4396(int frequency);
void setRollOffFilter(char filterslow);
void mute_AK4396(char mute);
