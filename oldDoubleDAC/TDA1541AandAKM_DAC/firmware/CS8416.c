#include "main.h"
#include "CS8416.h"
#include "i2cmaster.h"


unsigned char read_CS8416(unsigned char address)
{
  unsigned char r;
  
  i2c_start_wait(CS8416_ADDRESS + I2C_WRITE);       // set device address and write mode
  i2c_write(address);                               // write the address
  i2c_rep_start(CS8416_ADDRESS + I2C_READ);
  r = i2c_read(0);                                  // read one byte
  i2c_stop();
  return r;  
}

void write_CS8416(unsigned char address, unsigned char value)
{
  i2c_start_wait(CS8416_ADDRESS + I2C_WRITE);       // set device address and write mode
  i2c_write(address);                               // write the address
  i2c_write(value);                                 // write the data
  i2c_stop();
}

void reset_CS8416(void)
{
  CBI(PORTC, AUTORST); // RST/ to 0
  _delay_ms_S(5);
  SBI(PORTC, AUTORST);
  _delay_ms_S(5);
}

void CS8416_Configure_spdif_receiver(void)
{
  reset_CS8416();

  //      SOMS SSF SORES1-0 SOJUST SODEL SOSPOL SOLRPOL
  // I2S:    x   x      x x      0     1      0       1

  write_CS8416(CSSER_DATA_FORMAT, 0b10100101); // Master, Omclk = 64fs, 24bits, SODEL, SOLRPOL
  //      0 FSWCLK 0 0 PDUR TRUNC Reserved Reserved
  //      0      0 0 0    1     1        0        0
  write_CS8416(CSCTRL0,           0b00001100); // FORCE LOCAL CLOCK, PDUR, TRUNC
  write_CS8416(CSCTRL1,           0b0000000); // Interrupt active hight, hold audio sample on error, MCLK = 256*FS
  //write_CS8416(CSCTRL2,           0b01000010); // Auto de-emphasis, GPO-O to interrupt output
  //write_CS8416(CSCTRL3,           0b00000000); // GPO-1 and GPO-2 to GRND
  write_CS8416(CSCTRL4,           0b10000001); // Run, input multiplexer on RXP0
}

void CS8416_set_mux_input(char mux_input)
{
  switch (mux_input)
    {
    case MUX_SPDIF:
      write_CS8416(CSCTRL4,           0b10000001); // Run, input multiplexer on RXP0
      break;
    case MUX_TOSLINK:
      write_CS8416(CSCTRL4,           0b10001001); // Run, input multiplexer on RXP1
      break;
    default:
      break;
    };
}

/*
void Configure_spdif_receiver(void)
{
  reset_CS8416();

  //      SOMS SSF SORES1-0 SOJUST SODEL SOSPOL SOLRPOL
  // I2S:    x   x      x x      0     1      0       1

  write_CS8416(CSSER_DATA_FORMAT, 0b10000101); // Master, Omclk = 64fs, 24bits, SODEL, SOLRPOL
  //      0 FSWCLK 0 0 PDUR TRUNC Reserved Reserved
  //      0      0 0 0    1     1        0        0
  write_CS8416(CSCTRL0,           0b00001100); // FORCE LOCAL CLOCK, PDUR, TRUNC
  write_CS8416(CSCTRL1,           0b0000000); // Interrupt active hight, hold audio sample on error, MCLK = 256*FS
  //write_CS8416(CSCTRL2,           0b01000010); // Auto de-emphasis, GPO-O to interrupt output
  //write_CS8416(CSCTRL3,           0b00000000); // GPO-1 and GPO-2 to GRND
  write_CS8416(CSCTRL4,           0b10000001); // Run, input multiplexer on RXP0
}
*/
