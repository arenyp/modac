
#define CS8416_ADDRESS  0x20

// CS8416 registers
#define CSCTRL0                    0x00
#define CSCTRL1                    0x01
#define CSCTRL2                    0x02
#define CSCTRL3                    0x03
#define CSCTRL4                    0x04
#define CSSER_DATA_FORMAT          0x05
#define CSREC_VERROR               0x06
#define CSINT_MASK                 0x07
#define CSINT_MODE_MSB             0x08
#define CSINT_MODE_LSB             0x09
#define CSRECEIVER_CHANNEL_STATUS  0x0A               
#define CSAUDIO_FORMAT             0x0B
#define CSRECEIVER_ERROR           0x0C
#define CSINTERRUPT_STATUS         0x0D
#define CSOMCK_RMCK_RATIO          0x18
#define CSCHANA0_STATUS            0x19
#define CSCHANA1_STATUS            0x1A
#define CSCHANB0_STATUS            0x1E
#define CSCHANB1_STATUS            0x1F

#define MUX_SPDIF                  0
#define MUX_TOSLINK                1

void CS8416_Configure_spdif_receiver(void);
void CS8416_set_mux_input(char mux_input);
