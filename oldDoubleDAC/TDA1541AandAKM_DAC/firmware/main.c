
/******************************************************************************
 Title:    DAC audio
 Author:   Patrick Areny arenyp@yahoo.fr
 Date:     2009
 Software: AVR-GCC 3.3 
 Hardware: ATMEGA8515 with internal rc oscillator

 Description:
	Initialise the IO ports
	Initialiset the external interrupts
	Sleeps ont timer waiting that configure button is pressed or
	an alarm on the inputs.
*******************************************************************************/

#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/signal.h>
#include <avr/eeprom.h>
#include <avr/sleep.h>
#include <util/delay.h>

#include "i2cmaster.h"
#include "AKM4396.h"
#include "CS8416.h"
#include "main.h"

t_dac     g_dacstate;
extern t_AKMregs g_AKMregs;

void _delay_ms_S(int millisecond)
{
  int mul;
  int res;
  int i;
  
  if (millisecond > 260)
    {
      mul = millisecond / 260;
      res = millisecond % 260;
      for (i = 0; i < mul; i++)
	_delay_ms(260);
      _delay_ms(res);
    }
  else
    _delay_ms(millisecond);
}

#define DELAI_BOUTON 10

char test_bouton_actif(char numb)
{
  static char prev = 1;
  char        ret;

  numb = numb & 1; // 0 or 1
  ret = 0;
  if (!((1 << numb) & PINB))
    {
      _delay_ms(DELAI_BOUTON);
      if (!((1 << numb) & PINB))
	{
	  if (prev == 1) // 1 to 0 transition
	    ret = 1;
	  prev = 0;
	}
    }
  else
    {
      _delay_ms(DELAI_BOUTON);
      if (((1 << numb) & PINB))
	prev = 1;
    }
  return ret;
}

char test_switch(char numb)
{
  char        ret2;
  char        ret;

  do
    {
      ret = ((1 << numb) & PIND) != 0;
      _delay_ms(DELAI_BOUTON);
      ret2 = ((1 << numb) & PIND) != 0;
      _delay_ms(DELAI_BOUTON);
    } 
  while (ret2 != ret);
  if (ret)
    return 1;
  return 0;
}

char test_switchB(char numb)
{
  char        ret2;
  char        ret;

  do
    {
      ret = ((1 << numb) & PINB) != 0;
      _delay_ms(DELAI_BOUTON);
      ret2 = ((1 << numb) & PINB) != 0;
      _delay_ms(DELAI_BOUTON);
    } 
  while (ret2 != ret);
  if (ret)
    return 1;
  return 0;
}

#define RELD 800

// The relay connects the rca GND to the dac and active filter boards
void connect_RCA_GND(void)
{
  SBI(PORTC,OUT_GND_CONNECT);
  _delay_ms_S(RELD); // Wait for the relay to change his state
  CBI(PORTC,OUT_GND_CONNECT);
}

void disconnect_RCA_GND(void)
{
  SBI(PORTC,OUT_GND_DISCONNECT);
  _delay_ms_S(RELD); // Wait for the relay to change his state
  CBI(PORTC,OUT_GND_DISCONNECT);
}

// A 74HCT138 3bit to 1 decoder sets one transistor pair at time
void change_relay_state(unsigned char state)
{
  PORTB = 0x00 | (state << 2);
  _delay_ms(RELD);
  PORTB = 0x00;
}

void Set_dac(char dac_selection)
{
  if (dac_selection == DAC_AKM)
    {
      mute_AK4396(0);
      change_relay_state(SRC_AKM);
    }
  else
    {
      mute_AK4396(1);
      change_relay_state(SRC_TDA);
    }
}

//#define DAC1

// Put the relay connections on the selected source
void Set_source(char source)
{
  switch(source)
    {
    case SOURCE_AUX:
      change_relay_state(SRC_AUX);
      change_relay_state(SRC_RCA);
      break;
    case SOURCE_PHONO:
      // Connect the ground to the rca
#ifdef DAC1
      connect_RCA_GND();
#endif
      // Phono gnd always connected to the box
      change_relay_state(SRC_PHONO);
      change_relay_state(SRC_RCA);
      break;
    case SOURCE_DAC_SPDIF:
#ifdef DAC1
      disconnect_RCA_GND();
#endif
      change_relay_state(SRC_SPDIF);
      // CS81546 multiplexer on SPDIF source
      CS8416_set_mux_input(MUX_SPDIF);
      break;
    case SOURCE_DAC_TOSLINK:
      //disconnect_RCA_GND();
      change_relay_state(SRC_SPDIF);
      // CS81546 multiplexer on TOSLINK source
      CS8416_set_mux_input(MUX_TOSLINK);
      break;
    }
  g_dacstate.source = source;
}


void INT0_init(void)
{
  // INT0 low -> interruption
  CBI(MCUCR, ISC01);
  CBI(MCUCR, ISC00);
  SBI(GIFR, INTF0);
  SBI(GICR, INT0);
}

// INT0 interrupt, for SPDIF change/error handling
ISR(INT0_vect)
{
  // Interrupt disabled
  CBI(GIFR, INTF0);
  CBI(GICR, INT0);
  // Do nothing, it just wakes up from idle mode
}

// Enable the timer interrupt and the INT0/PD2 input interrupt
void configure_Interrupts(void)
{
  //set_sleep_mode(SLEEP_MODE_IDLE);  
}

void configure_Timer(void)
{
}

void enter_idle_mode(void)
{
  // reset the timer
  
  // cpu in idle mode, (IO clock and timers are activated)
  sleep_enable();
  sei();
  sleep_cpu();
  sleep_disable();
}

void setled(char led, char state)
{
  if (state)
    SBI(PORTA, led);
  else
    CBI(PORTA, led);
}

// DAC 1: TDA & AKM 2 entres analogiques, spdif, toslink
//        Selecteur 3 canaux sur IN_SELECT0 IN_SELECT1
//        Selecteur de dac sur D_SELECT
// DAC 2: AKM       1 entree analogique,  spdif, toslink
//        Selecteur 2 entrées sur IN_SELECT0 analogique/numerique
//        Selecteur de spdif/toslink sur D_SELECT
int main(void)
{
  //	uint8_t port;
  char source;
  char dac_selection;
  char tmp_source;
  char tmp_dac;
  char toggle;
  char m;

  // port A: 8 bit led output
  DDRA  = 0xFF;
  PORTA = 0x00;

  // port B: IN1, IN2, OUTS0, OUTS1, OUTS2 (not used: AVR_MOSI, AVR_MISO, AVR_SCK)
  DDRB  = 0x00 | (1 << OUTS0) | (1 << OUTS1) | (1 << OUTS2);
  PORTB = 0x00 | (0 << OUTS0) | (0 << OUTS1) | (0 << OUTS2);

  // Port C [3..7]  OUT_GND_CONNECT OUT_GND_DISCONNECT, AUTORST SCL SDA -> CS8416 spdif to I2S
  DDRC  = 0x00 | (1 << OUT_GND_CONNECT) | (1 << OUT_GND_DISCONNECT) | (1 << AUTORST); // 4,7k pull up on SDA and SDL
  PORTC = 0x00 | (0 << SDA) | (0 << SCL);

  // Port D: TX RX INT0 D_SELECT AK4396CS POWERDOWN CDTI CCLK
  DDRD  = 0x00 | (1 << AK4396CS) | (1 << POWERDOWN) | (1 << CDTI) | (1 << CCLK);
  PORTD = 0x00 | (1 << AK4396CS) | (1 << POWERDOWN) | (0 << CDTI) | (0 << CCLK);

  i2c_init();
  INT0_init();

  // Wait 2 seconds
  _delay_ms_S(2000);
  // Read the current dac
  g_dacstate.bAK4369 = 0;
  g_dacstate.AK4396_SlowRollOffFilter = 0;

  CS8416_Configure_spdif_receiver();
  Config_AK4396(0);

  // Read selection
  //source = SOURCE_DAC_SPDIF;
  source = SOURCE_DAC_TOSLINK;
  tmp_source = source;
  //dac_selection = DAC_TDA1541A;
  dac_selection = DAC_AKM;
  Set_source(source);
  Set_dac(dac_selection);
  setled(4, source & 1);
  setled(5, source >> 1);

  sei();                         // enable interrupts

  // Power led to on
  setled(7,1);

  setled(0, 1);
  setled(1, 1);
  disconnect_RCA_GND();

  setRollOffFilter(g_dacstate.AK4396_SlowRollOffFilter);
  setled(1, g_dacstate.AK4396_SlowRollOffFilter);

  for (m = 0, toggle = 1;; m++)                       // loop forever
    {
      //connect_RCA_GND();
      _delay_ms_S(20);
      if ((m & 3) == 0)
	setled(0, (toggle++) & 1);
#ifdef DEV_TEST
      if (test_bouton_actif(0))
	{
	  if (dac_selection == DAC_AKM)
	    {
	      dac_selection = DAC_TDA1541A;
	    }
	  else
	    {
	      dac_selection = DAC_AKM;
	    }
	  Set_source(source, dac_selection);
	  //g_dacstate.AK4396_SlowRollOffFilter ^= 1; // Invert the value 0/1
	}
      //disconnect_RCA_GND();
      // Change the input (tda akm phono tuner)
      if (test_bouton_actif(1))
	{
	  source = (source + 1) & 3;
	  // debug
	  // dac_selection = DAC_AKM;
	  //  source = SOURCE_DAC_TOSLINK;

	  Set_source(source, dac_selection);
	  setled(4, source & 1);
	  setled(5, source >> 1);
	}
#endif /*DEV_TEST*/

#ifdef DAC1
      // DAC
      if (test_switch(D_SELECT))
	tmp_dac = DAC_AKM;
      else
	tmp_dac = DAC_TDA1541A;
      // Source
      // Mouse buttons used to change source
      char but1, but2;
      //but1 = test_switchB(BIN1);
      //but2 = test_switchB(BIN2);
      but1 = test_bouton_actif(BIN1);
      but2 = test_bouton_actif(BIN2);
      if (but1)
	{
	  tmp_source = (source + 1) < 3? (source + 1) : 3;
	}
      if (but2)
	{
	  tmp_source = (source - 1) > 0? (source - 1) : 0;
	}
#else
      tmp_dac = DAC_AKM;
      // 0 ANALOG  / 1 DIGITAL
      tmp_source = test_switch(IN_SELECT0);
      // O TOSLINK / 1 SPDIF
      if (tmp_source)
	tmp_source = SOURCE_AUX;	
      else
	tmp_source = test_switch(D_SELECT)? SOURCE_DAC_SPDIF : SOURCE_DAC_TOSLINK;
#endif
      if (tmp_source != source)
	{
	  source = tmp_source;
	  Set_source(source);
	  setled(4, source & 1);
	  setled(5, source >> 1);
	}
      if (tmp_dac != dac_selection)
	{
	  dac_selection = tmp_dac;
	  if (dac_selection == DAC_AKM)
	    {
	      setled(2, 1);
	      setled(3, 0);
	    }
	  else
	    {
	      setled(2, 0);
	      setled(3, 1);
	    }
	  Set_dac(dac_selection);
	}
      //_delay_ms(500);
      //enter_idle_mode();
    }
}
