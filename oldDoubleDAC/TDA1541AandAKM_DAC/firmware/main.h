
#define SBI(port, bit) ((port) |= (1 << (bit)))
#define CBI(port, bit) ((port) &= ~(1 << (bit)))

// PORTA -> leds

// PORTB
#define BIN1   PB0
#define BIN2   PB1
#define OUTS0  PB2
#define OUTS1  PB3
#define OUTS2  PB4

// PORTC
#define OUT_GND_CONNECT    PC3
#define OUT_GND_DISCONNECT PC4
#define AUTORST            PC5
#define SCL                PC6
#define SDA                PC7

// PORTD
#define TX             PD0
#define RX             PD1
#define IN_SELECT0     PD0
#define IN_SELECT1     PD1
#define CS8416INT      PD2
#define D_SELECT       PD3
#define AK4396CS       PD4
#define POWERDOWN      PD5
#define CDTI           PD6
#define CCLK           PD7

// Relay values for audio sources
#define NO_SOURCE            0
#define SRC_AKM              1
#define SRC_TDA              2
#define SRC_PHONO            3
#define SRC_AUX              4
#define SRC_RCA              5
#define SRC_SPDIF            6
#define NO_SOURCE2           7

// Audio sources
#define SOURCE_PHONO         0
#define SOURCE_AUX           1
#define SOURCE_DAC_SPDIF     2
#define SOURCE_DAC_TOSLINK   3

// Dac selection
#define DAC_AKM              0
#define DAC_TDA1541A         1

typedef struct s_dac
{
  char       benabled;   // Sound enabled
  int        freq;       // Frequency
  int        err;        // Input error
  char       bAK4369;    // 1: uses the AK4369 0: uses the TDA1541A
  char       blostClock; // 1 in case of a spdif clock problem
  char       external44khzclk; // 1 if the 44khz quartz (redbook) is used on the spdif receiver
  char       source;
  char       AK4396_SlowRollOffFilter;
}              t_dac;

void _delay_ms_S(int millisecond);

