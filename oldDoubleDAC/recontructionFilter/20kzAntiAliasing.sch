EESchema Schematic File Version 4
LIBS:power
LIBS:74xx
LIBS:ltc1563-3
LIBS:filtrereconstruction-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date "15 nov 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 20kzAntiAliasing-rescue:R R6
U 1 1 545B6472
P 6950 4050
AR Path="/5461DA93/545B6472" Ref="R6"  Part="1" 
AR Path="/5461E4E7/545B6472" Ref="R12"  Part="1" 
F 0 "R6" V 7030 4050 40  0000 C CNN
F 1 "116,1K" V 6957 4051 40  0000 C CNN
F 2 "~" V 6880 4050 30  0000 C CNN
F 3 "~" H 6950 4050 30  0000 C CNN
	1    6950 4050
	1    0    0    -1  
$EndComp
$Comp
L 20kzAntiAliasing-rescue:R R4
U 1 1 545B651E
P 6600 3200
AR Path="/5461DA93/545B651E" Ref="R4"  Part="1" 
AR Path="/5461E4E7/545B651E" Ref="R10"  Part="1" 
F 0 "R4" V 6680 3200 40  0000 C CNN
F 1 "116,1K" V 6607 3201 40  0000 C CNN
F 2 "~" V 6530 3200 30  0000 C CNN
F 3 "~" H 6600 3200 30  0000 C CNN
	1    6600 3200
	0    -1   -1   0   
$EndComp
$Comp
L 20kzAntiAliasing-rescue:R R2
U 1 1 545B6525
P 4450 3400
AR Path="/5461DA93/545B6525" Ref="R2"  Part="1" 
AR Path="/5461E4E7/545B6525" Ref="R8"  Part="1" 
F 0 "R2" V 4530 3400 40  0000 C CNN
F 1 "116,1K" V 4457 3401 40  0000 C CNN
F 2 "~" V 4380 3400 30  0000 C CNN
F 3 "~" H 4450 3400 30  0000 C CNN
	1    4450 3400
	0    -1   -1   0   
$EndComp
$Comp
L 20kzAntiAliasing-rescue:R R5
U 1 1 545B652B
P 6600 3400
AR Path="/5461DA93/545B652B" Ref="R5"  Part="1" 
AR Path="/5461E4E7/545B652B" Ref="R11"  Part="1" 
F 0 "R5" V 6680 3400 40  0000 C CNN
F 1 "116,1K" V 6607 3401 40  0000 C CNN
F 2 "~" V 6530 3400 30  0000 C CNN
F 3 "~" H 6600 3400 30  0000 C CNN
	1    6600 3400
	0    -1   -1   0   
$EndComp
$Comp
L 20kzAntiAliasing-rescue:R R1
U 1 1 545B6531
P 3700 3600
AR Path="/5461DA93/545B6531" Ref="R1"  Part="1" 
AR Path="/5461E4E7/545B6531" Ref="R7"  Part="1" 
F 0 "R1" V 3780 3600 40  0000 C CNN
F 1 "116,1K" V 3707 3601 40  0000 C CNN
F 2 "~" V 3630 3600 30  0000 C CNN
F 3 "~" H 3700 3600 30  0000 C CNN
	1    3700 3600
	0    -1   -1   0   
$EndComp
$Comp
L 20kzAntiAliasing-rescue:R R3
U 1 1 545B6537
P 4450 3600
AR Path="/5461DA93/545B6537" Ref="R3"  Part="1" 
AR Path="/5461E4E7/545B6537" Ref="R9"  Part="1" 
F 0 "R3" V 4530 3600 40  0000 C CNN
F 1 "116,1K" V 4457 3601 40  0000 C CNN
F 2 "~" V 4380 3600 30  0000 C CNN
F 3 "~" H 4450 3600 30  0000 C CNN
	1    4450 3600
	0    -1   -1   0   
$EndComp
Text HLabel 7250 2950 2    60   Output ~ 0
VOUT
Text HLabel 3050 3600 0    60   Input ~ 0
VIN
Text HLabel 6750 2100 2    60   Input ~ 0
+5V
$Comp
L 20kzAntiAliasing-rescue:C C4
U 1 1 545B66BD
P 6550 2400
AR Path="/5461DA93/545B66BD" Ref="C4"  Part="1" 
AR Path="/5461E4E7/545B66BD" Ref="C6"  Part="1" 
AR Path="/545B66BD" Ref="C4"  Part="1" 
F 0 "C4" H 6550 2500 40  0000 L CNN
F 1 "0.1uF" H 6556 2315 40  0000 L CNN
F 2 "~" H 6588 2250 30  0000 C CNN
F 3 "~" H 6550 2400 60  0000 C CNN
	1    6550 2400
	1    0    0    -1  
$EndComp
$Comp
L 20kzAntiAliasing-rescue:C C3
U 1 1 545B66D6
P 4550 4100
AR Path="/5461DA93/545B66D6" Ref="C3"  Part="1" 
AR Path="/5461E4E7/545B66D6" Ref="C5"  Part="1" 
AR Path="/545B66D6" Ref="C3"  Part="1" 
F 0 "C3" H 4550 4200 40  0000 L CNN
F 1 "0.1uF" H 4556 4015 40  0000 L CNN
F 2 "~" H 4588 3950 30  0000 C CNN
F 3 "~" H 4550 4100 60  0000 C CNN
	1    4550 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3100 6100 3100
Wire Wire Line
	6100 3100 6100 2100
Wire Wire Line
	4800 2100 6100 2100
Wire Wire Line
	4800 2100 4800 3100
Wire Wire Line
	4800 3100 4900 3100
Connection ~ 6100 2100
Wire Wire Line
	4900 3200 4100 3200
Wire Wire Line
	4100 3400 4200 3400
Connection ~ 4100 3400
Wire Wire Line
	3950 3600 4100 3600
Connection ~ 4100 3600
Wire Wire Line
	4700 3400 4900 3400
Wire Wire Line
	4700 3600 4750 3600
Wire Wire Line
	3450 3600 3050 3600
Wire Wire Line
	4750 3600 4750 4450
Wire Wire Line
	4750 4450 6950 4450
Wire Wire Line
	6950 4450 6950 4300
Connection ~ 4750 3600
Wire Wire Line
	6950 3200 6950 3400
Wire Wire Line
	6950 3200 6850 3200
Wire Wire Line
	6850 3400 6950 3400
Connection ~ 6950 3400
Wire Wire Line
	6000 3600 6950 3600
Connection ~ 6950 3600
Wire Wire Line
	6350 3400 6000 3400
Wire Wire Line
	6000 3200 6250 3200
Wire Wire Line
	6250 3200 6250 2950
Wire Wire Line
	6250 2950 7250 2950
Connection ~ 6250 3200
Wire Wire Line
	3950 3800 4550 3800
Wire Wire Line
	4900 3300 4850 3300
Wire Wire Line
	4850 3300 4850 3500
Wire Wire Line
	4850 3700 4900 3700
Wire Wire Line
	4850 4200 6150 4200
Wire Wire Line
	6150 4200 6150 3700
Connection ~ 4850 3700
Wire Wire Line
	4850 3500 4900 3500
Connection ~ 4850 3500
Wire Wire Line
	6150 3700 6000 3700
Wire Wire Line
	6150 3500 6000 3500
Connection ~ 6150 3700
Wire Wire Line
	6150 3300 6000 3300
Connection ~ 6150 3500
Wire Wire Line
	4550 4300 4550 4700
Wire Wire Line
	4550 4700 4850 4700
Wire Wire Line
	4550 3900 4550 3800
Connection ~ 4550 3800
Wire Wire Line
	6550 2600 6550 2700
Wire Wire Line
	6550 2700 6900 2700
Wire Wire Line
	6550 2200 6550 2100
Connection ~ 6550 2100
Text Label 6900 2700 2    60   ~ 0
AGND
Text Label 4850 4700 2    60   ~ 0
AGND
Text HLabel 5050 4700 2    60   Input ~ 0
AGND
Text HLabel 3950 3800 0    60   Input ~ 0
-5V
Wire Wire Line
	4100 3200 4100 3400
Connection ~ 4850 4700
Connection ~ 4850 4200
Wire Wire Line
	6000 3800 6350 3800
Text Label 4150 3800 0    60   ~ 0
V-
Text Label 6350 3800 0    60   ~ 0
V-
$Comp
L ltc1563-3:LTC1563-3 U1
U 1 1 5461EBD0
P 5450 3950
AR Path="/5461DA93/5461EBD0" Ref="U1"  Part="1" 
AR Path="/5461E4E7/5461EBD0" Ref="U2"  Part="1" 
F 0 "U1" H 5450 3950 60  0000 C CNN
F 1 "LTC1563-3" H 5450 3850 60  0000 C CNN
F 2 "~" H 5450 3950 60  0000 C CNN
F 3 "~" H 5450 3950 60  0000 C CNN
	1    5450 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 2100 6550 2100
Wire Wire Line
	4100 3400 4100 3600
Wire Wire Line
	4100 3600 4200 3600
Wire Wire Line
	4750 3600 4900 3600
Wire Wire Line
	6950 3400 6950 3600
Wire Wire Line
	6950 3600 6950 3800
Wire Wire Line
	6250 3200 6350 3200
Wire Wire Line
	4850 3700 4850 4200
Wire Wire Line
	4850 3500 4850 3700
Wire Wire Line
	6150 3700 6150 3500
Wire Wire Line
	6150 3500 6150 3300
Wire Wire Line
	4550 3800 4900 3800
Wire Wire Line
	6550 2100 6750 2100
Wire Wire Line
	4850 4700 5050 4700
Wire Wire Line
	4850 4200 4850 4700
$EndSCHEMATC
