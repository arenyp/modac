EESchema Schematic File Version 4
LIBS:power
LIBS:74xx
LIBS:ltc1563-3
LIBS:filtrereconstruction-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date "15 nov 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 6150 2450 1150 1000
U 5461DA93
F0 "filtre_L" 50
F1 "20kzAntiAliasing.sch" 50
F2 "VOUT" O R 7300 2650 60 
F3 "VIN" I L 6150 2650 60 
F4 "+5V" I L 6150 3000 60 
F5 "AGND" I L 6150 3150 60 
F6 "-5V" I L 6150 3300 60 
$EndSheet
$Sheet
S 6150 3850 1150 1000
U 5461E4E7
F0 "filtre_R" 50
F1 "20kzAntiAliasing.sch" 50
F2 "VOUT" O R 7300 4050 60 
F3 "VIN" I L 6150 4050 60 
F4 "+5V" I L 6150 4400 60 
F5 "AGND" I L 6150 4550 60 
F6 "-5V" I L 6150 4700 60 
$EndSheet
$Comp
L filtrereconstruction-rescue:CONN_3- K3
U 1 1 5461E603
P 9000 3450
F 0 "K3" V 8950 3450 50  0000 C CNN
F 1 "CONN_3" V 9050 3450 40  0000 C CNN
F 2 "~" H 9000 3450 60  0000 C CNN
F 3 "~" H 9000 3450 60  0000 C CNN
	1    9000 3450
	1    0    0    -1  
$EndComp
$Comp
L filtrereconstruction-rescue:CP1- C1
U 1 1 5461E702
P 4300 3350
F 0 "C1" H 4350 3450 50  0000 L CNN
F 1 "10uF" H 4350 3250 50  0000 L CNN
F 2 "~" H 4300 3350 60  0000 C CNN
F 3 "~" H 4300 3350 60  0000 C CNN
	1    4300 3350
	1    0    0    -1  
$EndComp
$Comp
L filtrereconstruction-rescue:CP1- C2
U 1 1 5461E739
P 4300 4000
F 0 "C2" H 4350 4100 50  0000 L CNN
F 1 "10uF" H 4350 3900 50  0000 L CNN
F 2 "~" H 4300 4000 60  0000 C CNN
F 3 "~" H 4300 4000 60  0000 C CNN
	1    4300 4000
	1    0    0    -1  
$EndComp
$Comp
L filtrereconstruction-rescue:CONN_3- K2
U 1 1 5461E86D
P 2950 3650
F 0 "K2" V 2900 3650 50  0000 C CNN
F 1 "CONN_3" V 3000 3650 40  0000 C CNN
F 2 "~" H 2950 3650 60  0000 C CNN
F 3 "~" H 2950 3650 60  0000 C CNN
	1    2950 3650
	-1   0    0    1   
$EndComp
$Comp
L filtrereconstruction-rescue:CONN_3- K1
U 1 1 5461E8C2
P 2950 2400
F 0 "K1" V 2900 2400 50  0000 C CNN
F 1 "CONN_3" V 3000 2400 40  0000 C CNN
F 2 "~" H 2950 2400 60  0000 C CNN
F 3 "~" H 2950 2400 60  0000 C CNN
	1    2950 2400
	-1   0    0    1   
$EndComp
Wire Wire Line
	3300 3550 3450 3550
Wire Wire Line
	3450 3550 3450 3000
Wire Wire Line
	3450 3000 4300 3000
Wire Wire Line
	4300 3000 4300 3150
Wire Wire Line
	5050 3650 4300 3650
Wire Wire Line
	3300 3750 3450 3750
Wire Wire Line
	3450 3750 3450 4400
Wire Wire Line
	3450 4400 4300 4400
Wire Wire Line
	4300 4400 4300 4200
Wire Wire Line
	4300 3550 4300 3650
Connection ~ 4300 3650
Wire Wire Line
	3300 2300 5700 2300
Wire Wire Line
	5700 2300 5700 2650
Wire Wire Line
	5700 2650 6150 2650
Wire Wire Line
	3300 2400 5600 2400
Wire Wire Line
	5600 2400 5600 4050
Wire Wire Line
	5600 4050 6150 4050
Wire Wire Line
	5050 2500 5050 3150
Wire Wire Line
	5050 3150 6150 3150
Wire Wire Line
	5050 2500 3300 2500
Connection ~ 5050 3150
Wire Wire Line
	5050 4550 6150 4550
Connection ~ 5050 3650
Connection ~ 4300 3000
Wire Wire Line
	5200 3000 5200 4400
Wire Wire Line
	5200 4400 6150 4400
Connection ~ 5200 3000
Wire Wire Line
	6150 4700 6000 4700
Wire Wire Line
	4850 4700 4850 4400
Connection ~ 4300 4400
Wire Wire Line
	6000 4700 6000 3300
Wire Wire Line
	6000 3300 6150 3300
Connection ~ 6000 4700
Wire Wire Line
	7300 2650 8100 2650
Wire Wire Line
	8100 2650 8100 3350
Wire Wire Line
	8100 3350 8650 3350
Wire Wire Line
	8650 3550 8100 3550
Wire Wire Line
	8100 3550 8100 4050
Wire Wire Line
	8100 4050 7300 4050
Wire Wire Line
	8650 3450 7800 3450
Text Label 7800 3450 0    60   ~ 0
AGND
Text Label 4650 3650 0    60   ~ 0
AGND
Text Label 3650 3000 0    60   ~ 0
+5V
Text Label 3600 4400 0    60   ~ 0
-5V
Text Label 3600 2300 0    60   ~ 0
INL
Text Label 3600 2400 0    60   ~ 0
INR
Text Label 8250 3350 0    60   ~ 0
OUTL
Text Label 8250 3550 0    60   ~ 0
OUTR
$Comp
L filtrereconstruction-rescue:CONN_1- P1
U 1 1 5461FC85
P 8950 1500
F 0 "P1" H 9030 1500 40  0000 L CNN
F 1 "CONN_1" H 8950 1555 30  0001 C CNN
F 2 "~" H 8950 1500 60  0000 C CNN
F 3 "~" H 8950 1500 60  0000 C CNN
	1    8950 1500
	1    0    0    -1  
$EndComp
$Comp
L filtrereconstruction-rescue:CONN_1- P2
U 1 1 5461FD64
P 8950 1650
F 0 "P2" H 9030 1650 40  0000 L CNN
F 1 "CONN_1" H 8950 1705 30  0001 C CNN
F 2 "~" H 8950 1650 60  0000 C CNN
F 3 "~" H 8950 1650 60  0000 C CNN
	1    8950 1650
	1    0    0    -1  
$EndComp
$Comp
L filtrereconstruction-rescue:CONN_1- P3
U 1 1 5461FD6A
P 8950 1800
F 0 "P3" H 9030 1800 40  0000 L CNN
F 1 "CONN_1" H 8950 1855 30  0001 C CNN
F 2 "~" H 8950 1800 60  0000 C CNN
F 3 "~" H 8950 1800 60  0000 C CNN
	1    8950 1800
	1    0    0    -1  
$EndComp
$Comp
L filtrereconstruction-rescue:CONN_1- P4
U 1 1 5461FD70
P 8950 1950
F 0 "P4" H 9030 1950 40  0000 L CNN
F 1 "CONN_1" H 8950 2005 30  0001 C CNN
F 2 "~" H 8950 1950 60  0000 C CNN
F 3 "~" H 8950 1950 60  0000 C CNN
	1    8950 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 1500 8650 1500
Wire Wire Line
	8650 1500 8650 1650
Wire Wire Line
	8650 2100 8250 2100
Wire Wire Line
	8800 1950 8650 1950
Connection ~ 8650 1950
Wire Wire Line
	8650 1800 8800 1800
Connection ~ 8650 1800
Wire Wire Line
	8800 1650 8650 1650
Connection ~ 8650 1650
Text Label 8250 2100 0    60   ~ 0
AGND
Wire Wire Line
	4300 3650 3300 3650
Wire Wire Line
	4300 3650 4300 3800
Wire Wire Line
	5050 3150 5050 3650
Wire Wire Line
	5050 3650 5050 4550
Wire Wire Line
	4300 3000 5200 3000
Wire Wire Line
	5200 3000 6150 3000
Wire Wire Line
	4300 4400 4850 4400
Wire Wire Line
	6000 4700 4850 4700
Wire Wire Line
	8650 1950 8650 2100
Wire Wire Line
	8650 1800 8650 1950
Wire Wire Line
	8650 1650 8650 1800
$EndSCHEMATC
