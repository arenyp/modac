EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x03_Female J1
U 1 1 5BE796B0
P 1150 2100
F 0 "J1" H 1044 1775 50  0000 C CNN
F 1 "18VAC GND 18VAC" H 1044 1866 50  0000 C CNN
F 2 "Connector_Phoenix_MC:PhoenixContact_MCV_1,5_3-GF-3.81_1x03_P3.81mm_Vertical_ThreadedFlange_MountHole" H 1150 2100 50  0001 C CNN
F 3 "~" H 1150 2100 50  0001 C CNN
F 4 "1793057" H 1150 2100 50  0001 C CNN "Ref Farnell"
	1    1150 2100
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J2
U 1 1 5BE79ABC
P 4200 4000
F 0 "J2" H 4094 3675 50  0000 C CNN
F 1 "12VAC" H 4094 3766 50  0000 C CNN
F 2 "Connector_Phoenix_MC:PhoenixContact_MCV_1,5_2-GF-3.81_1x02_P3.81mm_Vertical_ThreadedFlange_MountHole" H 4200 4000 50  0001 C CNN
F 3 "~" H 4200 4000 50  0001 C CNN
F 4 "1793055" H 4200 4000 50  0001 C CNN "Ref Farnell"
	1    4200 4000
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Female J6
U 1 1 5BE79ECD
P 6050 2050
F 0 "J6" H 6077 2076 50  0000 L CNN
F 1 "V+ GNDA V-" H 6077 1985 50  0000 L CNN
F 2 "Connector_Phoenix_MC:PhoenixContact_MCV_1,5_3-GF-3.81_1x03_P3.81mm_Vertical_ThreadedFlange_MountHole" H 6050 2050 50  0001 C CNN
F 3 "~" H 6050 2050 50  0001 C CNN
F 4 "1793057" H 6050 2050 50  0001 C CNN "Ref Farnell"
	1    6050 2050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J4
U 1 1 5BE79FEA
P 6900 5100
F 0 "J4" V 6747 5148 50  0000 L CNN
F 1 "12VAC" V 6838 5148 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 6900 5100 50  0001 C CNN
F 3 "~" H 6900 5100 50  0001 C CNN
F 4 "1716993" H 6900 5100 50  0001 C CNN "Ref Farnell"
	1    6900 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	5850 1950 5650 1950
Wire Wire Line
	5850 2150 5650 2150
Wire Wire Line
	5650 2150 5650 2500
Text Label 2150 2100 0    50   ~ 0
GNDA
Wire Wire Line
	1350 2000 1750 2000
Wire Wire Line
	1350 2200 1750 2200
Wire Wire Line
	1750 2000 1750 1600
Wire Wire Line
	3350 1600 3350 1800
Connection ~ 2200 1600
Wire Wire Line
	2200 1600 3350 1600
Wire Wire Line
	1750 2200 1750 2550
Wire Wire Line
	3350 2550 3350 2400
Text Label 2850 1600 0    50   ~ 0
AC1
Text Label 2850 2550 0    50   ~ 0
AC2
Text Label 5500 1500 0    50   ~ 0
V+
Text Label 5500 2500 0    50   ~ 0
V-
Text Label 4450 2050 0    50   ~ 0
GNDA
Wire Wire Line
	6800 4100 6800 4450
Connection ~ 6900 3700
Connection ~ 6800 4450
Wire Wire Line
	6800 4450 6800 4500
Text Label 5700 3700 0    50   ~ 0
12VAC
$Comp
L Device:CP1 C5
U 1 1 5BE850DE
P 8800 4350
F 0 "C5" H 8915 4396 50  0000 L CNN
F 1 "100µF35V" H 8915 4305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 8800 4350 50  0001 C CNN
F 3 "~" H 8800 4350 50  0001 C CNN
F 4 "2841960" H 8800 4350 50  0001 C CNN "Ref Farnell"
	1    8800 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 4200 8800 4100
Connection ~ 8800 4100
Wire Wire Line
	8800 4100 9750 4100
Wire Wire Line
	8800 4500 8800 4600
Connection ~ 8800 4600
Wire Wire Line
	8800 4600 9750 4600
$Comp
L Connector:Conn_01x02_Female J8
U 1 1 5BE86A50
P 10100 4300
F 0 "J8" H 10127 4276 50  0000 L CNN
F 1 "12V GNDD" H 10127 4185 50  0000 L CNN
F 2 "Connector_Phoenix_MC:PhoenixContact_MCV_1,5_2-GF-3.81_1x02_P3.81mm_Vertical_ThreadedFlange_MountHole" H 10100 4300 50  0001 C CNN
F 3 "~" H 10100 4300 50  0001 C CNN
F 4 "1793055" H 10100 4300 50  0001 C CNN "Ref Farnell"
	1    10100 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 4100 9750 4300
Wire Wire Line
	9750 4300 9900 4300
Wire Wire Line
	9900 4400 9750 4400
Wire Wire Line
	9750 4400 9750 4600
Text Notes 5850 5300 0    50   ~ 0
To Speaker protection boards
$Comp
L Connector:Conn_01x02_Female J3
U 1 1 5BE886E7
P 5900 5100
F 0 "J3" V 5747 5148 50  0000 L CNN
F 1 "12VAC" V 5838 5148 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 5900 5100 50  0001 C CNN
F 3 "~" H 5900 5100 50  0001 C CNN
F 4 "1716993" H 5900 5100 50  0001 C CNN "Ref Farnell"
	1    5900 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	6800 4450 5800 4450
Wire Wire Line
	5800 4450 5800 4900
Wire Wire Line
	5900 4900 5900 4750
Wire Wire Line
	5900 4750 6900 4750
Connection ~ 6900 4750
Wire Wire Line
	6900 4750 6900 4900
Text Label 9500 4600 0    50   ~ 0
GNDD
Text Label 9500 4100 0    50   ~ 0
12V
Text Label 2350 1600 0    50   ~ 0
18VAC
Text Notes 10150 4150 0    50   ~ 0
Digital Power\n
$Comp
L Connector:Conn_01x01_Female J5
U 1 1 5BE8D641
P 5200 1200
F 0 "J5" H 5227 1226 50  0000 L CNN
F 1 "GNDA" H 5227 1135 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 5200 1200 50  0001 C CNN
F 3 "~" H 5200 1200 50  0001 C CNN
	1    5200 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 1200 4450 1200
Text Label 4450 1200 0    50   ~ 0
GNDA
Text Notes 4550 1050 0    50   ~ 0
To earth
$Comp
L Connector:Conn_01x03_Female J7
U 1 1 5BE809F0
P 6050 2800
F 0 "J7" H 6077 2826 50  0000 L CNN
F 1 "V+ GNDA V-" H 6077 2735 50  0000 L CNN
F 2 "Connector_Phoenix_MC:PhoenixContact_MCV_1,5_3-GF-3.81_1x03_P3.81mm_Vertical_ThreadedFlange_MountHole" H 6050 2800 50  0001 C CNN
F 3 "~" H 6050 2800 50  0001 C CNN
F 4 "1793057" H 6050 2800 50  0001 C CNN "Ref Farnell"
	1    6050 2800
	1    0    0    -1  
$EndComp
Text Label 5500 2800 0    50   ~ 0
GNDA
Text Label 5500 2700 0    50   ~ 0
V+
Text Label 5500 2900 0    50   ~ 0
V-
Wire Wire Line
	5500 2700 5850 2700
Wire Wire Line
	5500 2800 5850 2800
Wire Wire Line
	5500 2900 5850 2900
$Comp
L Device:CP1 C6
U 1 1 5BEB4F08
P 4450 1800
F 0 "C6" H 4565 1846 50  0000 L CNN
F 1 "6800µF50v" H 4565 1755 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D22.0mm_P10.00mm_SnapIn" H 4450 1800 50  0001 C CNN
F 3 "~" H 4450 1800 50  0001 C CNN
F 4 "2841962" H 4450 1800 50  0001 C CNN "Ref Farnell"
	1    4450 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C7
U 1 1 5BEB621F
P 4450 2300
F 0 "C7" H 4335 2254 50  0000 R CNN
F 1 "6800µF50v" H 4335 2345 50  0000 R CNN
F 2 "Capacitor_THT:CP_Radial_D22.0mm_P10.00mm_SnapIn" H 4450 2300 50  0001 C CNN
F 3 "~" H 4450 2300 50  0001 C CNN
F 4 "2841962" H 4450 2300 50  0001 C CNN "Ref Farnell"
	1    4450 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 1950 4450 2050
Wire Wire Line
	4450 2050 4450 2150
Connection ~ 4450 2050
Wire Wire Line
	4450 2450 4450 2500
$Comp
L Device:Fuse F1
U 1 1 5BF526DF
P 6300 4100
F 0 "F1" V 6103 4100 50  0000 C CNN
F 1 "500mA63V" V 6194 4100 50  0000 C CNN
F 2 "Fuse:Fuse_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6230 4100 50  0001 C CNN
F 3 "~" H 6300 4100 50  0001 C CNN
F 4 "7785712" V 6300 4100 50  0001 C CNN "Ref Farnell"
	1    6300 4100
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5BF5BA18
P 2200 1450
F 0 "TP1" H 2258 1570 50  0000 L CNN
F 1 "18VAC" H 2258 1479 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2400 1450 50  0001 C CNN
F 3 "~" H 2400 1450 50  0001 C CNN
	1    2200 1450
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5BF5BB24
P 3900 1400
F 0 "TP2" H 3958 1520 50  0000 L CNN
F 1 "V+" H 3958 1429 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 4100 1400 50  0001 C CNN
F 3 "~" H 4100 1400 50  0001 C CNN
	1    3900 1400
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5BF5D2CF
P 3900 2650
F 0 "TP3" H 3842 2677 50  0000 R CNN
F 1 "V-" H 3842 2768 50  0000 R CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 4100 2650 50  0001 C CNN
F 3 "~" H 4100 2650 50  0001 C CNN
	1    3900 2650
	-1   0    0    1   
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 5BF5ED0E
P 8800 4000
F 0 "TP4" H 8858 4120 50  0000 L CNN
F 1 "12v" H 8858 4029 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 9000 4000 50  0001 C CNN
F 3 "~" H 9000 4000 50  0001 C CNN
	1    8800 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 4100 8800 4000
Wire Wire Line
	3900 1500 3900 1400
Wire Wire Line
	2200 1600 2200 1450
Wire Wire Line
	3900 2650 3900 2500
$Comp
L Device:D_Bridge_+AA- D1
U 1 1 5D3CC019
P 3350 2100
F 0 "D1" H 3691 2146 50  0000 L CNN
F 1 "D_Bridge_+AA-" H 3691 2055 50  0000 L CNN
F 2 "Diode_THT:Diode_Bridge_Vishay_GBL" H 3350 2100 50  0001 C CNN
F 3 "~" H 3350 2100 50  0001 C CNN
F 4 "2889170" H 3350 2100 50  0001 C CNN "Ref Farnell"
	1    3350 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1500 3650 1500
Wire Wire Line
	3650 1500 3650 2100
Wire Wire Line
	3050 2500 3050 2100
Wire Wire Line
	3050 2500 3900 2500
Connection ~ 9750 4600
Text Label 9950 4600 0    50   ~ 0
GNDA
$Comp
L Device:CP1 C8
U 1 1 5E1177C7
P 3300 6550
F 0 "C8" H 3415 6596 50  0000 L CNN
F 1 "47µF35V" H 3415 6505 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 3300 6550 50  0001 C CNN
F 3 "~" H 3300 6550 50  0001 C CNN
F 4 "2841906" H 3300 6550 50  0001 C CNN "Ref Farnell"
	1    3300 6550
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C9
U 1 1 5E11EEA0
P 3300 7000
F 0 "C9" H 3415 7046 50  0000 L CNN
F 1 "47µF35V" H 3415 6955 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 3300 7000 50  0001 C CNN
F 3 "~" H 3300 7000 50  0001 C CNN
F 4 "2841906" H 3300 7000 50  0001 C CNN "Ref Farnell"
	1    3300 7000
	-1   0    0    1   
$EndComp
Wire Wire Line
	3300 6400 3300 6350
Wire Wire Line
	3300 7200 3300 7150
Wire Wire Line
	2550 6900 2550 6800
Wire Wire Line
	3300 6800 3300 6700
Connection ~ 2550 6800
Wire Wire Line
	2550 6800 2550 6650
Wire Wire Line
	3300 6800 3300 6850
Connection ~ 3300 6800
Wire Wire Line
	2250 6350 2100 6350
Wire Wire Line
	2250 7200 2100 7200
Text Label 4050 6800 0    50   ~ 0
GNDA
Text Label 1850 6350 0    50   ~ 0
V+
Text Label 1850 7200 0    50   ~ 0
V-
Text Label 4050 7200 0    50   ~ 0
-15
$Comp
L Regulator_Linear:L7815 U1
U 1 1 5E14EA9A
P 2550 6350
F 0 "U1" H 2550 6592 50  0000 C CNN
F 1 "L7815" H 2550 6501 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-263-2" H 2575 6200 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 2550 6300 50  0001 C CNN
F 4 "2318483" H 2550 6350 50  0001 C CNN "Ref Farnell"
	1    2550 6350
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:L7915 U2
U 1 1 5E14EC84
P 2550 7200
F 0 "U2" H 2550 7050 50  0000 C CNN
F 1 "L7915" H 2550 6959 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-263-2" H 2550 7000 50  0001 C CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/c9/16/86/41/c7/2b/45/f2/CD00000450.pdf/files/CD00000450.pdf/jcr:content/translations/en.CD00000450.pdf" H 2550 7200 50  0001 C CNN
F 4 "2532880" H 2550 7200 50  0001 C CNN "Ref Farnell"
	1    2550 7200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J9
U 1 1 5E156DE2
P 5250 6550
F 0 "J9" H 5223 6480 50  0000 R CNN
F 1 "Conn_01x03_Male" H 5223 6571 50  0000 R CNN
F 2 "lib:0533750310" H 5250 6550 50  0001 C CNN
F 3 "~" H 5250 6550 50  0001 C CNN
F 4 "3107299" H 5250 6550 50  0001 C CNN "Ref Farnell"
	1    5250 6550
	-1   0    0    1   
$EndComp
Wire Wire Line
	5050 6450 4800 6450
Wire Wire Line
	4800 6650 5050 6650
Wire Wire Line
	5050 6550 4800 6550
Text Label 4800 6550 0    50   ~ 0
GNDA
Text Label 4800 6450 0    50   ~ 0
-15
Text Label 4800 6650 0    50   ~ 0
+15
$Comp
L Device:Fuse F2
U 1 1 5E1990AA
P 4150 1500
F 0 "F2" V 3953 1500 50  0000 C CNN
F 1 "5A" V 4044 1500 50  0000 C CNN
F 2 "Fuse:Fuse_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4080 1500 50  0001 C CNN
F 3 "~" H 4150 1500 50  0001 C CNN
F 4 "2850022" V 4150 1500 50  0001 C CNN "Ref Farnell"
	1    4150 1500
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J10
U 1 1 5E2FFE2B
P 5250 6950
F 0 "J10" H 5223 6880 50  0000 R CNN
F 1 "Conn_01x03_Male" H 5223 6971 50  0000 R CNN
F 2 "lib:0533750310" H 5250 6950 50  0001 C CNN
F 3 "~" H 5250 6950 50  0001 C CNN
F 4 "3107299" H 5250 6950 50  0001 C CNN "Ref Farnell"
	1    5250 6950
	-1   0    0    1   
$EndComp
Wire Wire Line
	5050 6850 4800 6850
Wire Wire Line
	4800 7050 5050 7050
Wire Wire Line
	5050 6950 4800 6950
Text Label 4800 6950 0    50   ~ 0
GNDA
Text Label 4800 6850 0    50   ~ 0
-15
Text Label 4800 7050 0    50   ~ 0
+15
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E135A88
P 2100 6350
F 0 "#FLG0101" H 2100 6425 50  0001 C CNN
F 1 "PWR_FLAG" H 2100 6524 50  0000 C CNN
F 2 "" H 2100 6350 50  0001 C CNN
F 3 "~" H 2100 6350 50  0001 C CNN
	1    2100 6350
	1    0    0    -1  
$EndComp
Connection ~ 2100 6350
Wire Wire Line
	2100 6350 1850 6350
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5E135B0E
P 2100 7200
F 0 "#FLG0102" H 2100 7275 50  0001 C CNN
F 1 "PWR_FLAG" H 2100 7373 50  0000 C CNN
F 2 "" H 2100 7200 50  0001 C CNN
F 3 "~" H 2100 7200 50  0001 C CNN
	1    2100 7200
	-1   0    0    1   
$EndComp
Connection ~ 2100 7200
Wire Wire Line
	2100 7200 1850 7200
Wire Wire Line
	4450 2050 5850 2050
$Comp
L Device:Fuse F3
U 1 1 5E1A1F36
P 4150 2500
F 0 "F3" V 3953 2500 50  0000 C CNN
F 1 "5A" V 4044 2500 50  0000 C CNN
F 2 "Fuse:Fuse_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4080 2500 50  0001 C CNN
F 3 "~" H 4150 2500 50  0001 C CNN
F 4 "2850022" V 4150 2500 50  0001 C CNN "Ref Farnell"
	1    4150 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 1600 2200 1600
Wire Wire Line
	1750 2550 3350 2550
Wire Wire Line
	1350 2100 2150 2100
Wire Wire Line
	4450 1500 4450 1650
Wire Wire Line
	5650 1500 5650 1950
$Comp
L Device:Fuse F5
U 1 1 5E158917
P 3100 7200
F 0 "F5" V 2903 7200 50  0000 C CNN
F 1 "500mA" V 2994 7200 50  0000 C CNN
F 2 "Fuse:Fuse_1206_3216Metric" V 3030 7200 50  0001 C CNN
F 3 "~" H 3100 7200 50  0001 C CNN
F 4 "9921834" V 3100 7200 50  0001 C CNN "Ref Farnell"
	1    3100 7200
	0    1    1    0   
$EndComp
Wire Wire Line
	3300 6800 4050 6800
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5E1779A1
P 7500 1950
F 0 "H1" V 7454 2100 50  0000 L CNN
F 1 "MountingHole_Pad" V 7545 2100 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 7500 1950 50  0001 C CNN
F 3 "~" H 7500 1950 50  0001 C CNN
	1    7500 1950
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5E177B6D
P 7500 2150
F 0 "H2" V 7454 2300 50  0000 L CNN
F 1 "MountingHole_Pad" V 7545 2300 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 7500 2150 50  0001 C CNN
F 3 "~" H 7500 2150 50  0001 C CNN
	1    7500 2150
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5E177C48
P 7500 2350
F 0 "H3" V 7454 2500 50  0000 L CNN
F 1 "MountingHole_Pad" V 7545 2500 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 7500 2350 50  0001 C CNN
F 3 "~" H 7500 2350 50  0001 C CNN
	1    7500 2350
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5E177C4E
P 7500 2550
F 0 "H4" V 7454 2700 50  0000 L CNN
F 1 "MountingHole_Pad" V 7545 2700 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 7500 2550 50  0001 C CNN
F 3 "~" H 7500 2550 50  0001 C CNN
	1    7500 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	7400 1950 7250 1950
Wire Wire Line
	7250 1950 7250 2150
Wire Wire Line
	7400 2550 7250 2550
Connection ~ 7250 2550
Wire Wire Line
	7250 2550 6950 2550
Wire Wire Line
	7400 2350 7250 2350
Connection ~ 7250 2350
Wire Wire Line
	7250 2350 7250 2550
Wire Wire Line
	7250 2150 7400 2150
Connection ~ 7250 2150
Wire Wire Line
	7250 2150 7250 2350
Text Label 6950 2550 0    50   ~ 0
GNDA
$Comp
L Connector:Conn_01x03_Male J12
U 1 1 5EC5423E
P 5250 6100
F 0 "J12" H 5223 6030 50  0000 R CNN
F 1 "Conn_01x03_Male" H 5223 6121 50  0000 R CNN
F 2 "lib:0533750310" H 5250 6100 50  0001 C CNN
F 3 "~" H 5250 6100 50  0001 C CNN
F 4 "3107299" H 5250 6100 50  0001 C CNN "Ref Farnell"
	1    5250 6100
	-1   0    0    1   
$EndComp
Wire Wire Line
	5050 6000 4800 6000
Wire Wire Line
	4800 6200 5050 6200
Wire Wire Line
	5050 6100 4800 6100
Text Label 4800 6100 0    50   ~ 0
GNDA
Text Label 4800 6000 0    50   ~ 0
-15
Text Label 4800 6200 0    50   ~ 0
+15
Wire Wire Line
	4300 1500 4450 1500
Connection ~ 4450 1500
Wire Wire Line
	4450 1500 5650 1500
Wire Wire Line
	4300 2500 4450 2500
Connection ~ 4450 2500
Wire Wire Line
	4450 2500 5650 2500
Wire Wire Line
	4000 2500 3900 2500
Connection ~ 3900 2500
Wire Wire Line
	4000 1500 3900 1500
Connection ~ 3900 1500
Text Label 4050 6350 0    50   ~ 0
+15
$Comp
L Device:Fuse F4
U 1 1 5E152EAE
P 3100 6350
F 0 "F4" V 2903 6350 50  0000 C CNN
F 1 "500mA" V 2994 6350 50  0000 C CNN
F 2 "Fuse:Fuse_1206_3216Metric" V 3030 6350 50  0001 C CNN
F 3 "~" H 3100 6350 50  0001 C CNN
F 4 "9921834" V 3100 6350 50  0001 C CNN "Ref Farnell"
	1    3100 6350
	0    1    1    0   
$EndComp
Wire Wire Line
	2850 6350 2950 6350
Wire Wire Line
	2850 7200 2950 7200
Wire Wire Line
	3250 7200 3300 7200
Connection ~ 3300 7200
Wire Wire Line
	3300 7200 4050 7200
Wire Wire Line
	3250 6350 3300 6350
Connection ~ 3300 6350
Wire Wire Line
	3300 6350 4050 6350
$Comp
L dk_Diodes-Bridge-Rectifiers:MDB8S D2
U 1 1 602B0893
P 7600 4100
F 0 "D2" H 8044 4153 60  0000 L CNN
F 1 "MDB8S" H 8044 4047 60  0000 L CNN
F 2 "digikey-footprints:SMD-4_5x4.4mm_P4mm" H 7800 4300 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/MDB8S-D.pdf" H 7800 4400 60  0001 L CNN
F 4 "MDB8SFSCT-ND" H 7800 4500 60  0001 L CNN "Digi-Key_PN"
F 5 "MDB8S" H 7800 4600 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 7800 4700 60  0001 L CNN "Category"
F 7 "Diodes - Bridge Rectifiers" H 7800 4800 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/MDB8S-D.pdf" H 7800 4900 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/MDB8S/MDB8SFSCT-ND/3137113" H 7800 5000 60  0001 L CNN "DK_Detail_Page"
F 10 "BRIDGE RECT 1P 800V 1A MICRODIP" H 7800 5100 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 7800 5200 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7800 5300 60  0001 L CNN "Status"
	1    7600 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 4600 7150 4100
Wire Wire Line
	7150 4100 7200 4100
Wire Wire Line
	7150 4600 8800 4600
Wire Wire Line
	8000 4100 8800 4100
Wire Wire Line
	6900 3700 6900 4750
Wire Wire Line
	7600 4500 7050 4500
Connection ~ 6800 4500
Wire Wire Line
	6800 4500 6800 4900
$Comp
L Device:Transformer_1P_1S T1
U 1 1 6090E0B8
P 5200 3900
F 0 "T1" H 5200 4281 50  0000 C CNN
F 1 "Transformer_1P_1S" H 5200 4190 50  0000 C CNN
F 2 "lib:MYRRADS44267" H 5200 3900 50  0001 C CNN
F 3 "~" H 5200 3900 50  0001 C CNN
	1    5200 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 4100 4800 4100
Wire Wire Line
	4400 4000 4800 4000
Wire Wire Line
	4800 4000 4800 3700
$Comp
L power:PWR_FLAG #FLG0105
U 1 1 609326A7
P 7050 4500
F 0 "#FLG0105" H 7050 4575 50  0001 C CNN
F 1 "PWR_FLAG" H 7050 4673 50  0000 C CNN
F 2 "" H 7050 4500 50  0001 C CNN
F 3 "~" H 7050 4500 50  0001 C CNN
	1    7050 4500
	1    0    0    -1  
$EndComp
Connection ~ 7050 4500
Wire Wire Line
	7050 4500 6800 4500
Wire Wire Line
	6450 4100 6800 4100
Wire Wire Line
	5600 3700 6900 3700
Wire Wire Line
	5600 4100 6150 4100
Wire Wire Line
	6900 3700 7050 3700
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 6093CD57
P 7050 3700
F 0 "#FLG0104" H 7050 3775 50  0001 C CNN
F 1 "PWR_FLAG" H 7050 3873 50  0000 C CNN
F 2 "" H 7050 3700 50  0001 C CNN
F 3 "~" H 7050 3700 50  0001 C CNN
	1    7050 3700
	1    0    0    -1  
$EndComp
Connection ~ 7050 3700
Wire Wire Line
	7050 3700 7600 3700
Wire Wire Line
	2550 6800 3300 6800
Wire Wire Line
	9750 4600 9950 4600
$EndSCHEMATC
