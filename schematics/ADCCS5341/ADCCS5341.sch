EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C7
U 1 1 5EF2F4C9
P 6700 1250
F 0 "C7" H 6815 1296 50  0000 L CNN
F 1 "1uF" H 6815 1205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6738 1100 50  0001 C CNN
F 3 "~" H 6700 1250 50  0001 C CNN
	1    6700 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C13
U 1 1 5EF33E3D
P 7100 3450
F 0 "C13" H 7215 3496 50  0000 L CNN
F 1 "0.1uF" H 7215 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7138 3300 50  0001 C CNN
F 3 "~" H 7100 3450 50  0001 C CNN
	1    7100 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5EF83687
P 4500 3250
F 0 "R2" V 4293 3250 50  0000 C CNN
F 1 "100K" V 4384 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4430 3250 50  0001 C CNN
F 3 "~" H 4500 3250 50  0001 C CNN
	1    4500 3250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8350 1050 8350 1150
Wire Wire Line
	6700 1100 6700 950 
Wire Wire Line
	6700 950  7150 950 
Wire Wire Line
	7100 3600 7100 3750
Wire Wire Line
	6700 1550 6700 1400
Wire Wire Line
	7900 1550 8350 1550
Wire Wire Line
	8350 1550 8350 1450
$Comp
L Device:C C9
U 1 1 5F1FA3A1
P 7900 1300
F 0 "C9" H 8015 1346 50  0000 L CNN
F 1 "1uF" H 8015 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7938 1150 50  0001 C CNN
F 3 "~" H 7900 1300 50  0001 C CNN
	1    7900 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5F1FAA8A
P 8350 1300
F 0 "C10" H 8465 1346 50  0000 L CNN
F 1 "0.1uF" H 8465 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8388 1150 50  0001 C CNN
F 3 "~" H 8350 1300 50  0001 C CNN
	1    8350 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 1150 7900 1050
Wire Wire Line
	7900 1050 8350 1050
Wire Wire Line
	7900 1450 7900 1550
Text Label 8150 1550 2    50   ~ 0
GND
Text Label 6900 950  2    50   ~ 0
AN5V
Text Label 7350 3750 2    50   ~ 0
GND
Text Label 7150 1550 2    50   ~ 0
GND
Wire Wire Line
	7100 3750 7350 3750
Text Label 8350 1050 2    50   ~ 0
3.3V
Text Label 9850 2750 2    50   ~ 0
GND
Text Label 7750 2750 2    50   ~ 0
MCLK
Text Label 7750 3350 2    50   ~ 0
LRCK
Text Label 7750 3250 2    50   ~ 0
SCLK
$Comp
L Device:C C15
U 1 1 5F21AA9B
P 9500 3000
F 0 "C15" H 9615 3046 50  0000 L CNN
F 1 "1uF" H 9615 2955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9538 2850 50  0001 C CNN
F 3 "~" H 9500 3000 50  0001 C CNN
	1    9500 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C17
U 1 1 5F21AD9D
P 9900 3000
F 0 "C17" H 10015 3046 50  0000 L CNN
F 1 "0.1uF" H 10015 2955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9938 2850 50  0001 C CNN
F 3 "~" H 9900 3000 50  0001 C CNN
	1    9900 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C16
U 1 1 5F2202BD
P 9900 2400
F 0 "C16" H 10015 2446 50  0000 L CNN
F 1 "0.1uF" H 10015 2355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9938 2250 50  0001 C CNN
F 3 "~" H 9900 2400 50  0001 C CNN
	1    9900 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 2750 9900 2750
Wire Wire Line
	9900 2550 9900 2750
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5F24028D
P 1950 6900
F 0 "H4" V 1904 7050 50  0000 L CNN
F 1 "MountingHole_Pad" V 1995 7050 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1950 6900 50  0001 C CNN
F 3 "~" H 1950 6900 50  0001 C CNN
	1    1950 6900
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5F243318
P 1950 6650
F 0 "H3" V 1904 6800 50  0000 L CNN
F 1 "MountingHole_Pad" V 1995 6800 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1950 6650 50  0001 C CNN
F 3 "~" H 1950 6650 50  0001 C CNN
	1    1950 6650
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5F243CA2
P 1950 6400
F 0 "H2" V 1904 6550 50  0000 L CNN
F 1 "MountingHole_Pad" V 1995 6550 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1950 6400 50  0001 C CNN
F 3 "~" H 1950 6400 50  0001 C CNN
	1    1950 6400
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5F243CAC
P 1950 6150
F 0 "H1" V 1904 6300 50  0000 L CNN
F 1 "MountingHole_Pad" V 1995 6300 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1950 6150 50  0001 C CNN
F 3 "~" H 1950 6150 50  0001 C CNN
	1    1950 6150
	0    1    1    0   
$EndComp
Wire Wire Line
	1850 6150 1700 6150
Wire Wire Line
	1700 6150 1700 6400
Wire Wire Line
	1700 6900 1850 6900
Wire Wire Line
	1850 6650 1700 6650
Connection ~ 1700 6650
Wire Wire Line
	1700 6650 1700 6900
Wire Wire Line
	1700 6400 1850 6400
Connection ~ 1700 6400
Wire Wire Line
	1700 6400 1700 6650
Wire Wire Line
	1700 6900 1350 6900
Text Label 1350 6900 2    50   ~ 0
GND
Wire Wire Line
	1500 1800 1850 1800
Wire Wire Line
	1500 1900 1850 1900
Wire Wire Line
	1500 2000 1850 2000
Text Label 1850 1800 2    50   ~ 0
MCLK
Text Label 1850 1900 2    50   ~ 0
LRCK
Text Label 1850 2000 2    50   ~ 0
SCLK
Text Label 1850 2100 2    50   ~ 0
SDATA
Text Label 1850 2650 2    50   ~ 0
GND
Text Label 1850 2450 2    50   ~ 0
3.3V
Wire Wire Line
	1500 1200 1850 1200
$Comp
L Device:R R1
U 1 1 5F27353F
P 2100 1850
F 0 "R1" V 1893 1850 50  0000 C CNN
F 1 "10K" V 1984 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2030 1850 50  0001 C CNN
F 3 "~" H 2100 1850 50  0001 C CNN
	1    2100 1850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2100 1700 2100 1600
Wire Wire Line
	2100 1600 2300 1600
$Comp
L Connector:Conn_01x03_Female J2
U 1 1 5F289BE0
P 9800 4350
F 0 "J2" H 9828 4376 50  0000 L CNN
F 1 "Conn_01x03_Female" H 9828 4285 50  0000 L CNN
F 2 "modac:0533750310" H 9800 4350 50  0001 C CNN
F 3 "~" H 9800 4350 50  0001 C CNN
F 4 "1205983" H 9800 4350 50  0001 C CNN "Ref Farnell"
	1    9800 4350
	1    0    0    -1  
$EndComp
Connection ~ 1700 6900
Text Label 7150 950  0    50   ~ 0
VA
Wire Wire Line
	7750 2750 7900 2750
Connection ~ 9500 2750
Connection ~ 9500 3150
Wire Wire Line
	9500 3150 9900 3150
Wire Wire Line
	9900 2850 9900 2750
Connection ~ 9900 2750
Wire Wire Line
	9500 2850 9500 2750
Wire Wire Line
	8750 3050 8850 3050
Wire Wire Line
	8750 3250 8850 3250
Text Label 8850 3050 0    50   ~ 0
AINR
Text Label 8850 3250 0    50   ~ 0
AINL
Wire Wire Line
	2100 2100 2100 2000
Wire Wire Line
	1500 2100 2100 2100
Text Label 1850 1200 2    50   ~ 0
RST\
Text Label 7750 3050 2    50   ~ 0
GND
Wire Wire Line
	7900 3350 7750 3350
Wire Wire Line
	7900 3250 7750 3250
Wire Wire Line
	7900 2950 7750 2950
Text Label 7750 2950 2    50   ~ 0
SDATA
Wire Wire Line
	8750 3350 8850 3350
Text Label 8850 3350 0    50   ~ 0
RST\
Text Label 8850 2950 0    50   ~ 0
VA
Wire Wire Line
	8750 2950 8850 2950
Wire Wire Line
	7900 3050 7750 3050
Wire Wire Line
	7900 2850 7750 2850
Wire Wire Line
	7900 2650 7750 2650
Wire Wire Line
	8750 2650 8850 2650
Wire Wire Line
	8750 3150 9500 3150
Wire Wire Line
	8750 2750 9300 2750
Wire Wire Line
	9300 2750 9300 2250
Wire Wire Line
	9400 2850 9400 2750
Wire Wire Line
	9400 2750 9500 2750
Wire Wire Line
	8750 2850 9400 2850
Text Label 7750 2850 2    50   ~ 0
VL
Text Label 7900 1050 2    50   ~ 0
VL
Text Label 8850 2650 0    50   ~ 0
MODE1
Text Label 7750 2650 2    50   ~ 0
MODE0
Text Notes 2350 1600 0    50   ~ 0
I2S Mode
Text Label 2300 1600 2    50   ~ 0
VL
$Comp
L Device:R R6
U 1 1 5EDE67ED
P 5200 2400
F 0 "R6" V 4993 2400 50  0000 C CNN
F 1 "634R" V 5084 2400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5130 2400 50  0001 C CNN
F 3 "~" H 5200 2400 50  0001 C CNN
F 4 "2326918" H 5200 2400 50  0001 C CNN "Ref Farnell"
	1    5200 2400
	0    -1   1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5EDE6D49
P 5800 3350
F 0 "R8" V 5593 3350 50  0000 C CNN
F 1 "91R" V 5684 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5730 3350 50  0001 C CNN
F 3 "~" H 5800 3350 50  0001 C CNN
F 4 "1894170" H 5800 3350 50  0001 C CNN "Ref Farnell"
	1    5800 3350
	0    -1   1    0   
$EndComp
$Comp
L Device:CP1 C3
U 1 1 5EDE77E2
P 5200 2650
F 0 "C3" V 5452 2650 50  0000 C CNN
F 1 "470pF" V 5361 2650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5200 2650 50  0001 C CNN
F 3 "~" H 5200 2650 50  0001 C CNN
F 4 "1005988" H 5200 2650 50  0001 C CNN "Ref Farnell"
	1    5200 2650
	0    -1   1    0   
$EndComp
$Comp
L Device:C C5
U 1 1 5EDE7E59
P 6050 3550
F 0 "C5" H 6165 3596 50  0000 L CNN
F 1 "2700pF" H 6165 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 6088 3400 50  0001 C CNN
F 3 "~" H 6050 3550 50  0001 C CNN
F 4 "2773247" H 6050 3550 50  0001 C CNN "Ref Farnell"
	1    6050 3550
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OP249 U1
U 1 1 5EDEF0F3
P 5200 3350
F 0 "U1" H 5200 2983 50  0000 C CNN
F 1 "OP249" H 5200 3074 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 5200 3350 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/OP249.pdf" H 5200 3350 50  0001 C CNN
F 4 "9604863" H 5200 3350 50  0001 C CNN "Ref Farnell"
	1    5200 3350
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:OP249 U1
U 2 1 5EDF1A91
P 5250 5150
F 0 "U1" H 5250 5517 50  0000 C CNN
F 1 "OP249" H 5250 5426 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 5250 5150 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/OP249.pdf" H 5250 5150 50  0001 C CNN
F 4 "9604863" H 5250 5150 50  0001 C CNN "Ref Farnell"
	2    5250 5150
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OP249 U1
U 3 1 5EDF48BF
P 7900 4350
F 0 "U1" H 7858 4396 50  0000 L CNN
F 1 "OP249" H 7858 4305 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 7900 4350 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/OP249.pdf" H 7900 4350 50  0001 C CNN
	3    7900 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5EE8819D
P 4500 3700
F 0 "R3" V 4293 3700 50  0000 C CNN
F 1 "100K" V 4384 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4430 3700 50  0001 C CNN
F 3 "~" H 4500 3700 50  0001 C CNN
	1    4500 3700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4900 3250 4900 2650
Wire Wire Line
	4900 2400 5050 2400
Wire Wire Line
	5350 2400 6050 2400
Wire Wire Line
	6050 2400 6050 3350
Wire Wire Line
	6050 3350 5950 3350
Connection ~ 6050 3350
Wire Wire Line
	6050 3350 6050 3400
Wire Wire Line
	6050 3350 6200 3350
Wire Wire Line
	5650 3350 5600 3350
Wire Wire Line
	5600 3350 5600 2650
Wire Wire Line
	5600 2650 5350 2650
Connection ~ 5600 3350
Wire Wire Line
	5600 3350 5500 3350
Wire Wire Line
	5050 2650 4900 2650
Connection ~ 4900 2650
Wire Wire Line
	4900 2650 4900 2400
Wire Wire Line
	4500 3400 4500 3450
Wire Wire Line
	4900 3450 4500 3450
Wire Wire Line
	4500 3450 4500 3550
Connection ~ 4500 3450
Wire Wire Line
	4500 3850 4500 3900
Wire Wire Line
	4500 3900 6050 3900
Wire Wire Line
	6050 3900 6050 3700
Wire Wire Line
	4500 3100 4500 2900
Text Label 4500 2900 0    50   ~ 0
VA
$Comp
L Device:R R4
U 1 1 5EECEED4
P 4550 5050
F 0 "R4" V 4343 5050 50  0000 C CNN
F 1 "100K" V 4434 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4480 5050 50  0001 C CNN
F 3 "~" H 4550 5050 50  0001 C CNN
	1    4550 5050
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5EECEEDE
P 5250 4200
F 0 "R7" V 5043 4200 50  0000 C CNN
F 1 "634R" V 5134 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5180 4200 50  0001 C CNN
F 3 "~" H 5250 4200 50  0001 C CNN
F 4 "2326918" H 5250 4200 50  0001 C CNN "Ref Farnell"
	1    5250 4200
	0    -1   1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5EECEEE8
P 5850 5150
F 0 "R9" V 5643 5150 50  0000 C CNN
F 1 "91R" V 5734 5150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5780 5150 50  0001 C CNN
F 3 "~" H 5850 5150 50  0001 C CNN
F 4 "1894170" H 5850 5150 50  0001 C CNN "Ref Farnell"
	1    5850 5150
	0    -1   1    0   
$EndComp
$Comp
L Device:CP1 C4
U 1 1 5EECEEF2
P 5250 4450
F 0 "C4" V 5502 4450 50  0000 C CNN
F 1 "470pF" V 5411 4450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5250 4450 50  0001 C CNN
F 3 "~" H 5250 4450 50  0001 C CNN
F 4 "1005988" H 5250 4450 50  0001 C CNN "Ref Farnell"
	1    5250 4450
	0    -1   1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5EECEF10
P 4550 5500
F 0 "R5" V 4343 5500 50  0000 C CNN
F 1 "100K" V 4434 5500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4480 5500 50  0001 C CNN
F 3 "~" H 4550 5500 50  0001 C CNN
	1    4550 5500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4950 5050 4950 4450
Wire Wire Line
	4950 4200 5100 4200
Wire Wire Line
	5400 4200 6100 4200
Wire Wire Line
	6100 4200 6100 5150
Wire Wire Line
	6100 5150 6000 5150
Connection ~ 6100 5150
Wire Wire Line
	6100 5150 6100 5200
Wire Wire Line
	6100 5150 6250 5150
Wire Wire Line
	5700 5150 5650 5150
Wire Wire Line
	5650 5150 5650 4450
Wire Wire Line
	5650 4450 5400 4450
Connection ~ 5650 5150
Wire Wire Line
	5650 5150 5550 5150
Wire Wire Line
	5100 4450 4950 4450
Connection ~ 4950 4450
Wire Wire Line
	4950 4450 4950 4200
Wire Wire Line
	4550 5200 4550 5250
Wire Wire Line
	4950 5250 4550 5250
Wire Wire Line
	4550 5250 4550 5350
Connection ~ 4550 5250
Wire Wire Line
	4550 5650 4550 5700
Wire Wire Line
	4550 5700 6100 5700
Wire Wire Line
	6100 5700 6100 5500
Wire Wire Line
	4550 4900 4550 4700
Text Label 4550 4700 0    50   ~ 0
VA
Wire Wire Line
	4500 3450 3900 3450
Wire Wire Line
	3900 5250 4550 5250
Text Label 9350 4350 0    50   ~ 0
GND
Text Label 6250 5150 0    50   ~ 0
AINR
Text Label 6200 3350 0    50   ~ 0
AINL
$Comp
L csADC:CS5341 U3
U 1 1 5EC0C31C
P 8000 3400
F 0 "U3" H 8325 4415 50  0000 C CNN
F 1 "CS5341" H 8325 4324 50  0000 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 8000 3350 50  0001 C CNN
F 3 "" H 8000 3350 50  0001 C CNN
F 4 "777-CS5341-CZZ" H 8000 3400 50  0001 C CNN "Mouser"
	1    8000 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 4250 9350 4250
Wire Wire Line
	9600 4450 9350 4450
Text Label 9350 4250 0    50   ~ 0
V+
Text Label 9350 4450 0    50   ~ 0
V-
$Comp
L Regulator_Linear:L78L05_SOT89 U2
U 1 1 5EC3D1DC
P 6250 950
F 0 "U2" H 6250 1192 50  0000 C CNN
F 1 "L78L05_SOT89" H 6250 1101 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3_Handsoldering" H 6250 1150 50  0001 C CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/15/55/e5/aa/23/5b/43/fd/CD00000446.pdf/files/CD00000446.pdf/jcr:content/translations/en.CD00000446.pdf" H 6250 900 50  0001 C CNN
F 4 "1467762" H 6250 950 50  0001 C CNN "Ref Farnell"
	1    6250 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 2250 9500 2250
$Comp
L Device:CP C14
U 1 1 5EC443CF
P 9500 2450
F 0 "C14" H 9618 2496 50  0000 L CNN
F 1 "22µF" H 9618 2405 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 9538 2300 50  0001 C CNN
F 3 "~" H 9500 2450 50  0001 C CNN
	1    9500 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 2300 9500 2250
Connection ~ 9500 2250
Wire Wire Line
	9500 2250 9900 2250
Wire Wire Line
	9500 2600 9500 2750
$Comp
L Device:CP C12
U 1 1 5EC5F73D
P 8800 4550
F 0 "C12" H 8918 4596 50  0000 L CNN
F 1 "10µF" H 8918 4505 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 8838 4400 50  0001 C CNN
F 3 "~" H 8800 4550 50  0001 C CNN
F 4 "2841905" H 8800 4550 50  0001 C CNN "Ref Farnell"
	1    8800 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C11
U 1 1 5EC43BFF
P 8800 4150
F 0 "C11" H 8918 4196 50  0000 L CNN
F 1 "10µF" H 8918 4105 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 8838 4000 50  0001 C CNN
F 3 "~" H 8800 4150 50  0001 C CNN
F 4 "2841905" H 8800 4150 50  0001 C CNN "Ref Farnell"
	1    8800 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 4350 8800 4300
Wire Wire Line
	8800 4350 9600 4350
Wire Wire Line
	8800 4350 8800 4400
Connection ~ 8800 4350
Wire Wire Line
	8250 4000 8800 4000
Wire Wire Line
	8250 4000 8250 4050
Wire Wire Line
	8800 4700 8250 4700
Wire Wire Line
	8250 4700 8250 4650
Wire Wire Line
	8800 4000 9350 4000
Wire Wire Line
	9350 4000 9350 4250
Connection ~ 8800 4000
Wire Wire Line
	9350 4450 9350 4700
Wire Wire Line
	9350 4700 8800 4700
Connection ~ 8800 4700
Text Label 5700 950  0    50   ~ 0
V+
Wire Wire Line
	5700 950  5950 950 
Wire Wire Line
	6550 950  6700 950 
Connection ~ 6700 950 
Wire Wire Line
	6250 1250 6250 1550
Wire Wire Line
	6250 1550 6700 1550
Wire Wire Line
	3900 4450 3750 4450
$Comp
L Device:CP1 C2
U 1 1 5EEFC818
P 3600 4450
F 0 "C2" V 3852 4450 50  0000 C CNN
F 1 "10uF15V" V 3761 4450 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 3600 4450 50  0001 C CNN
F 3 "~" H 3600 4450 50  0001 C CNN
F 4 "2841905" H 3600 4450 50  0001 C CNN "Ref Farnell"
	1    3600 4450
	0    1    -1   0   
$EndComp
Wire Wire Line
	3400 4300 3400 4200
Wire Wire Line
	3250 4300 3400 4300
Text Label 3400 4200 2    50   ~ 0
GND
Wire Wire Line
	3250 4200 3400 4200
Wire Wire Line
	3900 4050 3750 4050
Wire Wire Line
	3350 4450 3450 4450
Wire Wire Line
	3350 4400 3350 4450
Wire Wire Line
	3250 4400 3350 4400
Wire Wire Line
	3350 4050 3450 4050
Wire Wire Line
	3350 4100 3350 4050
Wire Wire Line
	3250 4100 3350 4100
$Comp
L Connector:Conn_01x04_Female J1
U 1 1 5EF836AF
P 3050 4200
F 0 "J1" H 3078 4176 50  0000 L CNN
F 1 "Conn_01x04_Female" H 3078 4085 50  0000 L CNN
F 2 "modac:ConnectorMicromatch-4" H 3050 4200 50  0001 C CNN
F 3 "~" H 3050 4200 50  0001 C CNN
F 4 "3784710" H 3050 4200 50  0001 C CNN "Ref Farnell"
	1    3050 4200
	-1   0    0    -1  
$EndComp
$Comp
L Device:CP1 C1
U 1 1 5EF8369B
P 3600 4050
F 0 "C1" V 3852 4050 50  0000 C CNN
F 1 "10uF15V" V 3761 4050 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 3600 4050 50  0001 C CNN
F 3 "~" H 3600 4050 50  0001 C CNN
F 4 "2841905" H 3600 4050 50  0001 C CNN "Ref Farnell"
	1    3600 4050
	0    1    -1   0   
$EndComp
Wire Wire Line
	3900 4450 3900 5250
Wire Wire Line
	3900 3450 3900 4050
$Sheet
S 3250 700  1200 800 
U 5EC92C11
F0 "ADCIndetifier" 50
F1 "identifier.sch" 50
F2 "GND" I L 3250 1400 50 
F3 "VDD" I L 3250 1300 50 
F4 "SDI" I L 3250 1050 50 
F5 "CS" I L 3250 850 50 
F6 "CLK" I L 3250 1150 50 
F7 "SDO" O L 3250 950 50 
$EndSheet
Wire Wire Line
	1500 850  3250 850 
Wire Wire Line
	1500 950  3250 950 
Wire Wire Line
	3250 1300 2950 1300
Wire Wire Line
	3250 1400 2950 1400
Wire Wire Line
	2200 1300 2200 1050
Wire Wire Line
	1500 1300 2200 1300
Wire Wire Line
	2200 1050 3250 1050
Wire Wire Line
	2350 1400 2350 1150
Wire Wire Line
	2350 1150 3250 1150
Wire Wire Line
	1500 1400 2350 1400
Text Label 2950 1300 2    50   ~ 0
3.3V
Text Label 2950 1400 2    50   ~ 0
GND
Text Notes 1000 3050 0    50   ~ 0
Mode0 is the clock speed mode.\n
Text Label 8550 5750 0    50   ~ 0
MODE1
Wire Wire Line
	8550 5750 8250 5750
$Comp
L Device:R R10
U 1 1 5ED0751A
P 8250 5500
F 0 "R10" H 8320 5546 50  0000 L CNN
F 1 "5K" H 8320 5455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8180 5500 50  0001 C CNN
F 3 "~" H 8250 5500 50  0001 C CNN
	1    8250 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 5350 8250 5200
Wire Wire Line
	8250 5200 8550 5200
Wire Wire Line
	8250 5650 8250 5750
Connection ~ 8250 5750
Wire Wire Line
	8250 5750 8250 5850
Wire Wire Line
	8250 6150 8250 6250
Wire Wire Line
	8250 6250 8550 6250
Text Label 8550 5200 2    50   ~ 0
3.3V
Text Label 8550 6250 0    50   ~ 0
GND
$Comp
L Device:R R11
U 1 1 5ED29D3E
P 8250 6000
F 0 "R11" H 8320 6046 50  0000 L CNN
F 1 "5K" H 8320 5955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8180 6000 50  0001 C CNN
F 3 "~" H 8250 6000 50  0001 C CNN
	1    8250 6000
	1    0    0    -1  
$EndComp
Text Notes 8350 6400 0    50   ~ 0
Tie M0 and M1 to VDD for slave mode\n
Text Label 9750 5750 0    50   ~ 0
MODE0
NoConn ~ 1500 1500
Wire Wire Line
	9750 5750 9450 5750
$Comp
L Device:R R12
U 1 1 5ED4E5CC
P 9450 5500
F 0 "R12" H 9520 5546 50  0000 L CNN
F 1 "5K" H 9520 5455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9380 5500 50  0001 C CNN
F 3 "~" H 9450 5500 50  0001 C CNN
	1    9450 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 5350 9450 5200
Wire Wire Line
	9450 5200 9750 5200
Wire Wire Line
	9450 5650 9450 5750
Connection ~ 9450 5750
Wire Wire Line
	9450 5750 9450 5850
Wire Wire Line
	9450 6150 9450 6250
Wire Wire Line
	9450 6250 9750 6250
Text Label 9750 5200 2    50   ~ 0
3.3V
Text Label 9750 6250 0    50   ~ 0
GND
$Comp
L Device:R R13
U 1 1 5ED4E5DF
P 9450 6000
F 0 "R13" H 9520 6046 50  0000 L CNN
F 1 "5K" H 9520 5955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9380 6000 50  0001 C CNN
F 3 "~" H 9450 6000 50  0001 C CNN
	1    9450 6000
	1    0    0    -1  
$EndComp
$Sheet
S 750  750  750  2000
U 5F1B95AC
F0 "ADCCon" 50
F1 "ADCCon.sch" 50
F2 "RST" I R 1500 1200 50 
F3 "CDTI" I R 1500 1300 50 
F4 "SCLK" I R 1500 2000 50 
F5 "GND" I R 1500 2650 50 
F6 "LRCK" I R 1500 1900 50 
F7 "CCLK" I R 1500 1400 50 
F8 "SDATA" I R 1500 2100 50 
F9 "CSADC" I R 1500 1500 50 
F10 "MCLK" I R 1500 1800 50 
F11 "3.3V" I R 1500 2450 50 
F12 "CSIDENT" I R 1500 850 50 
F13 "SDO" I R 1500 950 50 
$EndSheet
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5F27ABB1
P 8250 4000
F 0 "#FLG0101" H 8250 4075 50  0001 C CNN
F 1 "PWR_FLAG" H 8250 4173 50  0000 C CNN
F 2 "" H 8250 4000 50  0001 C CNN
F 3 "~" H 8250 4000 50  0001 C CNN
	1    8250 4000
	1    0    0    -1  
$EndComp
Connection ~ 8250 4000
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5F27B5ED
P 8250 4700
F 0 "#FLG0102" H 8250 4775 50  0001 C CNN
F 1 "PWR_FLAG" H 8250 4873 50  0000 C CNN
F 2 "" H 8250 4700 50  0001 C CNN
F 3 "~" H 8250 4700 50  0001 C CNN
	1    8250 4700
	-1   0    0    1   
$EndComp
Connection ~ 8250 4700
Wire Wire Line
	7100 3150 7100 3300
Wire Wire Line
	7100 3150 7900 3150
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5F2A7D22
P 2050 2450
F 0 "#FLG0103" H 2050 2525 50  0001 C CNN
F 1 "PWR_FLAG" H 2050 2623 50  0000 C CNN
F 2 "" H 2050 2450 50  0001 C CNN
F 3 "~" H 2050 2450 50  0001 C CNN
	1    2050 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 2450 2050 2450
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 5FA32858
P 2050 2650
F 0 "#FLG0104" H 2050 2725 50  0001 C CNN
F 1 "PWR_FLAG" H 2050 2823 50  0000 C CNN
F 2 "" H 2050 2650 50  0001 C CNN
F 3 "~" H 2050 2650 50  0001 C CNN
	1    2050 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 2650 2050 2650
Text Label 7100 3150 2    50   ~ 0
3.3V
Wire Wire Line
	7150 1550 6700 1550
Connection ~ 6700 1550
$Comp
L Device:C C6
U 1 1 5EECEEFC
P 6100 5350
F 0 "C6" H 6215 5396 50  0000 L CNN
F 1 "2700pF" H 6215 5305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 6138 5200 50  0001 C CNN
F 3 "~" H 6100 5350 50  0001 C CNN
F 4 "2773247" H 6100 5350 50  0001 C CNN "Ref Farnell"
	1    6100 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 4650 8250 4650
Wire Wire Line
	7800 4050 8250 4050
$EndSCHEMATC
