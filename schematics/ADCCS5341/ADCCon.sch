EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4000 3950 4300 3950
Wire Wire Line
	4300 3750 4000 3750
Wire Wire Line
	4000 3250 4300 3250
Wire Wire Line
	4300 3350 4000 3350
Wire Wire Line
	4000 3550 4300 3550
Wire Wire Line
	4000 3150 4300 3150
Text Label 4300 2950 0    50   ~ 0
MCLK
Text Label 4300 3250 0    50   ~ 0
SDATA
Text Label 4300 3750 0    50   ~ 0
CCLK
Text Label 4300 3050 0    50   ~ 0
GND
Text Label 4300 3950 0    50   ~ 0
CDTO
Wire Wire Line
	4000 3650 4300 3650
Text Label 4300 3650 0    50   ~ 0
CSIDENT
Text Label 4300 3350 0    50   ~ 0
LRCK
Text Label 4300 3150 0    50   ~ 0
SCLK
Text Label 4300 3850 0    50   ~ 0
CDTI
Text Label 4300 3550 0    50   ~ 0
RST
Text HLabel 7300 2650 2    50   Input ~ 0
CSIDENT
Text HLabel 7300 2750 2    50   Input ~ 0
SDO
Text HLabel 7300 3950 2    50   Input ~ 0
RST
Text HLabel 7300 3850 2    50   Input ~ 0
CDTI
Text HLabel 7300 3150 2    50   Input ~ 0
SCLK
Text HLabel 7300 4150 2    50   Input ~ 0
GND
Text HLabel 7300 3250 2    50   Input ~ 0
LRCK
Text HLabel 7300 3650 2    50   Input ~ 0
CCLK
Text HLabel 7300 3350 2    50   Input ~ 0
SDATA
Text HLabel 7300 3750 2    50   Input ~ 0
CSADC
Text HLabel 7300 3050 2    50   Input ~ 0
MCLK
Text HLabel 7300 4350 2    50   Input ~ 0
3.3V
Text Label 7050 3050 0    50   ~ 0
MCLK
Text Label 7050 3350 0    50   ~ 0
SDATA
Text Label 7050 4150 0    50   ~ 0
GND
Text Label 7050 3750 0    50   ~ 0
CSADC
Text Label 7050 3650 0    50   ~ 0
CCLK
Text Label 7050 4350 0    50   ~ 0
3.3V
Text Label 7050 3250 0    50   ~ 0
LRCK
Text Label 7050 3150 0    50   ~ 0
SCLK
Text Label 7050 3850 0    50   ~ 0
CDTI
Text Label 7050 3950 0    50   ~ 0
RST
Wire Wire Line
	7300 4350 7050 4350
Wire Wire Line
	7300 4150 7050 4150
Wire Wire Line
	7300 3950 7050 3950
Wire Wire Line
	7300 3850 7050 3850
Wire Wire Line
	7300 3750 7050 3750
Wire Wire Line
	7300 3650 7050 3650
Wire Wire Line
	7300 3350 7050 3350
Wire Wire Line
	7300 3250 7050 3250
Wire Wire Line
	7300 3150 7050 3150
Wire Wire Line
	7300 3050 7050 3050
Text Label 6950 2650 0    50   ~ 0
CSIDENT
Wire Wire Line
	6950 2650 7300 2650
Wire Wire Line
	6950 2750 7300 2750
Text Label 6950 2750 0    50   ~ 0
CDTO
Wire Wire Line
	4000 3850 4300 3850
Wire Wire Line
	4000 2950 4300 2950
Wire Wire Line
	4000 3050 4300 3050
$Comp
L Connector:Conn_01x12_Female J3
U 1 1 608FE86E
P 3800 3450
F 0 "J3" H 3692 2625 50  0000 C CNN
F 1 "Conn_01x12_Female" H 3692 2716 50  0000 C CNN
F 2 "modac:ConnectorMicromatch-12" H 3800 3450 50  0001 C CNN
F 3 "~" H 3800 3450 50  0001 C CNN
	1    3800 3450
	-1   0    0    1   
$EndComp
Text Label 4300 3450 0    50   ~ 0
CSADC
Wire Wire Line
	4000 3450 4300 3450
Wire Wire Line
	4000 2850 4300 2850
Text Label 4300 2850 0    50   ~ 0
3.3V
$EndSCHEMATC
