EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PowerAmp-rescue:OPA2544-Amplifier-cache-PowerAmp-rescue-PowerAmp-rescue U3
U 1 1 5BE5ED56
P 7500 3900
F 0 "U3" H 7500 5050 60  0000 C CNN
F 1 "OPA2544" H 7500 4944 60  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-11_P3.4x5.08mm_StaggerOdd_Lead4.85mm_Vertical" H 7500 3900 60  0001 C CNN
F 3 "" H 7500 3900 60  0000 C CNN
	1    7500 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 4150 6450 4150
Text Label 1950 3850 0    50   ~ 0
AUDR
Text Label 1950 3700 0    50   ~ 0
AUDL
$Comp
L PowerAmp-rescue:CONN_3-Amplifier-cache-PowerAmp-rescue-PowerAmp-rescue K1
U 1 1 5BE5F901
P 1250 1450
F 0 "K1" H 1377 1478 50  0000 L CNN
F 1 "V+ GND V-" H 1377 1394 40  0000 L CNN
F 2 "Connector_Phoenix_MC:PhoenixContact_MCV_1,5_3-GF-3.81_1x03_P3.81mm_Vertical_ThreadedFlange_MountHole" H 1250 1450 60  0001 C CNN
F 3 "" H 1250 1450 60  0000 C CNN
	1    1250 1450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7350 2950 7350 2500
Wire Wire Line
	7350 2500 7700 2500
Wire Wire Line
	7700 2950 7700 2500
Connection ~ 7700 2500
Wire Wire Line
	7700 2500 8050 2500
Wire Wire Line
	7350 5250 7650 5250
Wire Wire Line
	7350 4850 7350 5250
Wire Wire Line
	7650 4850 7650 5250
Connection ~ 7650 5250
Wire Wire Line
	7650 5250 8050 5250
Text Label 1850 1350 2    50   ~ 0
V+
Text Label 1850 1550 2    50   ~ 0
V-
Text Label 1850 1450 2    50   ~ 0
GNDA
Text Label 9400 3900 0    50   ~ 0
GNDA
$Comp
L PowerAmp-rescue:R-Amplifier-cache-PowerAmp-rescue-PowerAmp-rescue R2
U 1 1 5BE63512
P 6100 5300
F 0 "R2" H 6171 5338 40  0000 L CNN
F 1 "30K" H 6171 5262 40  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6030 5300 30  0001 C CNN
F 3 "" H 6100 5300 30  0000 C CNN
F 4 "1469914" H 6100 5300 50  0001 C CNN "Ref Farnell"
	1    6100 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 5550 6100 5650
Wire Wire Line
	6100 4950 6100 5000
Connection ~ 6100 5000
Wire Wire Line
	6100 5000 6100 5050
$Comp
L PowerAmp-rescue:R-Amplifier-cache-PowerAmp-rescue-PowerAmp-rescue R3
U 1 1 5BE65C95
P 6550 4700
F 0 "R3" H 6621 4738 40  0000 L CNN
F 1 "30K" H 6621 4662 40  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6480 4700 30  0001 C CNN
F 3 "" H 6550 4700 30  0000 C CNN
F 4 "1469914" H 6550 4700 50  0001 C CNN "Ref Farnell"
	1    6550 4700
	1    0    0    -1  
$EndComp
$Comp
L PowerAmp-rescue:R-Amplifier-cache-PowerAmp-rescue-PowerAmp-rescue R4
U 1 1 5BE65C9B
P 6550 5300
F 0 "R4" H 6621 5338 40  0000 L CNN
F 1 "30K" H 6621 5262 40  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6480 5300 30  0001 C CNN
F 3 "" H 6550 5300 30  0000 C CNN
F 4 "1469914" H 6550 5300 50  0001 C CNN "Ref Farnell"
	1    6550 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 5550 6550 5650
Wire Wire Line
	6550 5650 6650 5650
Text Label 6650 5650 0    50   ~ 0
GNDA
Wire Wire Line
	6550 4950 6550 5000
Connection ~ 6550 5000
Wire Wire Line
	6550 5000 6550 5050
Wire Wire Line
	6100 4450 6100 4400
Wire Wire Line
	6100 4400 6050 4400
Wire Wire Line
	6550 4450 6550 4400
Wire Wire Line
	6550 4400 6500 4400
Wire Wire Line
	6300 5000 6300 3650
Wire Wire Line
	6300 3650 6950 3650
Wire Wire Line
	6100 5000 6300 5000
Wire Wire Line
	6750 5000 6750 4350
Wire Wire Line
	6750 4350 6950 4350
Wire Wire Line
	6550 5000 6750 5000
Text Label 8100 3550 0    50   ~ 0
AMPL
Text Label 6050 4400 0    50   ~ 0
AMPL
Text Label 6500 4400 0    50   ~ 0
AMPR
Text Label 8100 4250 0    50   ~ 0
AMPR
Text Label 8050 2500 0    50   ~ 0
V+
Text Label 8050 5250 0    50   ~ 0
V-
$Comp
L PowerAmp-rescue:C-Amplifier-cache-PowerAmp-rescue-PowerAmp-rescue C7
U 1 1 5BE6BC9C
P 2500 1100
F 0 "C7" H 2615 1138 40  0000 L CNN
F 1 "0.1µF" H 2615 1062 40  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.2mm_W5.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 2538 950 30  0001 C CNN
F 3 "" H 2500 1100 60  0000 C CNN
F 4 "1890265" H 2500 1100 50  0001 C CNN "Ref Farnell"
	1    2500 1100
	-1   0    0    -1  
$EndComp
$Comp
L Device:CP1 C3
U 1 1 5BE6BEB2
P 3400 1150
F 0 "C3" H 3515 1196 50  0000 L CNN
F 1 "6800µF50V" H 3515 1105 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D22.0mm_P10.00mm_SnapIn" H 3400 1150 50  0001 C CNN
F 3 "~" H 3400 1150 50  0001 C CNN
F 4 "2841962" H 3400 1150 50  0001 C CNN "Ref Farnell"
	1    3400 1150
	-1   0    0    -1  
$EndComp
$Comp
L PowerAmp-rescue:C-Amplifier-cache-PowerAmp-rescue-PowerAmp-rescue C8
U 1 1 5BE6BF58
P 2500 1800
F 0 "C8" H 2615 1838 40  0000 L CNN
F 1 "0.1µF" H 2615 1762 40  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.2mm_W5.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 2538 1650 30  0001 C CNN
F 3 "" H 2500 1800 60  0000 C CNN
F 4 "1890265" H 2500 1800 50  0001 C CNN "Ref Farnell"
	1    2500 1800
	-1   0    0    -1  
$EndComp
$Comp
L Device:CP1 C4
U 1 1 5BE6BF64
P 3400 1750
F 0 "C4" H 3515 1796 50  0000 L CNN
F 1 "6800µF50V" H 3515 1705 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D22.0mm_P10.00mm_SnapIn" H 3400 1750 50  0001 C CNN
F 3 "~" H 3400 1750 50  0001 C CNN
F 4 "2841962" H 3400 1750 50  0001 C CNN "Ref Farnell"
	1    3400 1750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3400 1450 3400 1600
Wire Wire Line
	3400 1450 3400 1300
Connection ~ 3400 1450
Wire Wire Line
	2500 1600 2500 1450
Connection ~ 2500 1450
Wire Wire Line
	2500 1450 1600 1450
Wire Wire Line
	2500 1450 2500 1300
Wire Wire Line
	3400 1000 3400 800 
Wire Wire Line
	2050 800  2050 1350
Wire Wire Line
	2050 1350 1600 1350
Wire Wire Line
	2500 900  2500 800 
Wire Wire Line
	2500 800  2050 800 
Wire Wire Line
	2050 2100 2050 1550
Wire Wire Line
	2050 1550 1600 1550
Wire Wire Line
	2500 2000 2500 2100
Wire Wire Line
	2500 2100 2050 2100
Wire Wire Line
	6100 5650 6550 5650
Connection ~ 6550 5650
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5BEA0EC3
P 1550 5350
F 0 "H1" H 1650 5401 50  0000 L CNN
F 1 "MountingHole_Pad" H 1650 5310 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1550 5350 50  0001 C CNN
F 3 "~" H 1550 5350 50  0001 C CNN
	1    1550 5350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5BEA0F8A
P 1550 5700
F 0 "H2" H 1650 5751 50  0000 L CNN
F 1 "MountingHole_Pad" H 1650 5660 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1550 5700 50  0001 C CNN
F 3 "~" H 1550 5700 50  0001 C CNN
	1    1550 5700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5BEA0FF4
P 1550 6050
F 0 "H3" H 1650 6101 50  0000 L CNN
F 1 "MountingHole_Pad" H 1650 6010 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1550 6050 50  0001 C CNN
F 3 "~" H 1550 6050 50  0001 C CNN
	1    1550 6050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5BEA1065
P 1550 6400
F 0 "H4" H 1650 6451 50  0000 L CNN
F 1 "MountingHole_Pad" H 1650 6360 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1550 6400 50  0001 C CNN
F 3 "~" H 1550 6400 50  0001 C CNN
	1    1550 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 4250 10100 4250
Text Notes 11150 4450 2    50   ~ 0
Put a UPC1237 speaker protection board after this.\nIt is not here because of the needed connexion to an AC transformer.
Wire Wire Line
	1550 5450 1300 5450
Wire Wire Line
	1300 5450 1300 5800
Wire Wire Line
	1300 5800 1550 5800
Wire Wire Line
	1300 5800 1300 6150
Wire Wire Line
	1300 6150 1550 6150
Connection ~ 1300 5800
Wire Wire Line
	1300 6150 1300 6500
Wire Wire Line
	1300 6500 1550 6500
Connection ~ 1300 6150
Text Label 1100 5450 0    50   ~ 0
GNDA
Wire Wire Line
	1300 5450 1100 5450
Connection ~ 1300 5450
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5BF08F6D
P 1900 800
F 0 "#FLG0101" H 1900 875 50  0001 C CNN
F 1 "PWR_FLAG" V 1900 928 50  0000 L CNN
F 2 "" H 1900 800 50  0001 C CNN
F 3 "~" H 1900 800 50  0001 C CNN
	1    1900 800 
	0    -1   1    0   
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5BF0901F
P 1900 2100
F 0 "#FLG0102" H 1900 2175 50  0001 C CNN
F 1 "PWR_FLAG" V 1900 2228 50  0000 L CNN
F 2 "" H 1900 2100 50  0001 C CNN
F 3 "~" H 1900 2100 50  0001 C CNN
	1    1900 2100
	0    -1   1    0   
$EndComp
Wire Wire Line
	2050 800  1900 800 
Connection ~ 2050 800 
Wire Wire Line
	1900 2100 2050 2100
Connection ~ 2050 2100
$Comp
L power:PWR_FLAG #FLG0105
U 1 1 5BF169D8
P 3650 1350
F 0 "#FLG0105" H 3650 1425 50  0001 C CNN
F 1 "PWR_FLAG" V 3650 1478 50  0000 L CNN
F 2 "" H 3650 1350 50  0001 C CNN
F 3 "~" H 3650 1350 50  0001 C CNN
	1    3650 1350
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J2
U 1 1 5CEF59E8
P 10400 3950
F 0 "J2" H 10427 3926 50  0000 L CNN
F 1 "R GND GND L" H 10427 3835 50  0000 L CNN
F 2 "lib:PhxContactSPTAF_4P" H 10400 3950 50  0001 C CNN
F 3 "~" H 10400 3950 50  0001 C CNN
F 4 "2580667" H 10400 3950 50  0001 C CNN "Ref Farnell"
	1    10400 3950
	1    0    0    1   
$EndComp
Wire Wire Line
	10200 3850 10200 3900
Wire Wire Line
	9400 3900 10200 3900
Connection ~ 10200 3900
Wire Wire Line
	10200 3900 10200 3950
Wire Wire Line
	2500 1450 3400 1450
Connection ~ 2500 2100
Wire Wire Line
	2500 2100 3400 2100
Connection ~ 2500 800 
Wire Wire Line
	2500 800  3400 800 
$Comp
L Connector:TestPoint TP2
U 1 1 5DCFE58A
P 2200 3950
F 0 "TP2" H 2142 3977 50  0000 R CNN
F 1 "AUDL" H 2142 4068 50  0000 R CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2400 3950 50  0001 C CNN
F 3 "~" H 2400 3950 50  0001 C CNN
	1    2200 3950
	-1   0    0    1   
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5DCFE786
P 2200 3550
F 0 "TP1" H 2258 3670 50  0000 L CNN
F 1 "AUDR" H 2258 3579 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2400 3550 50  0001 C CNN
F 3 "~" H 2400 3550 50  0001 C CNN
	1    2200 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 5DD07A90
P 6450 4050
F 0 "TP4" H 6508 4170 50  0000 L CNN
F 1 "PREAMPR" H 6508 4079 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 6650 4050 50  0001 C CNN
F 3 "~" H 6650 4050 50  0001 C CNN
	1    6450 4050
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5DD07C05
P 6450 3350
F 0 "TP3" H 6508 3470 50  0000 L CNN
F 1 "PREAMPL" H 6508 3379 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 6650 3350 50  0001 C CNN
F 3 "~" H 6650 3350 50  0001 C CNN
	1    6450 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3350 6450 3450
Wire Wire Line
	6450 3450 6950 3450
Wire Wire Line
	6450 4050 6450 4150
Text Label 1500 3800 0    50   ~ 0
GNDA
Text Notes 6050 5900 0    50   ~ 0
Non inverting\nUout = 2 x UPreamp
$Comp
L Connector:Conn_01x04_Female J1
U 1 1 5E2594E6
P 1100 3750
F 0 "J1" H 994 4035 50  0000 C CNN
F 1 "Conn_01x04_Female" H 994 3944 50  0000 C CNN
F 2 "lib:ConnectorMicromatch-4" H 1100 3750 50  0001 C CNN
F 3 "~" H 1100 3750 50  0001 C CNN
F 4 "3784710" H 1100 3750 50  0001 C CNN "Ref Farnell"
	1    1100 3750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1300 3850 1400 3850
Wire Wire Line
	1400 3850 1400 3800
Wire Wire Line
	1400 3750 1300 3750
Wire Wire Line
	1400 3800 1500 3800
Connection ~ 1400 3800
Wire Wire Line
	1400 3800 1400 3750
Wire Wire Line
	2550 3700 2200 3700
Wire Wire Line
	2200 3950 2200 3850
Connection ~ 2200 3850
Wire Wire Line
	2200 3700 2200 3550
Connection ~ 2200 3700
Wire Wire Line
	1800 3700 1800 3650
Wire Wire Line
	1800 3650 1300 3650
Wire Wire Line
	1800 3700 2200 3700
Wire Wire Line
	1300 3950 1800 3950
Wire Wire Line
	1800 3950 1800 3850
Wire Wire Line
	1800 3850 2200 3850
Text Notes 1050 4150 0    50   ~ 0
Analog audio in
Wire Wire Line
	10100 3750 10200 3750
Wire Wire Line
	10100 3550 10100 3750
Wire Wire Line
	8050 3550 10100 3550
Wire Wire Line
	10200 4050 10100 4050
Wire Wire Line
	10100 4050 10100 4250
$Comp
L PowerAmp-rescue:R-Amplifier-cache-PowerAmp-rescue-PowerAmp-rescue R1
U 1 1 5BE633E0
P 6100 4700
F 0 "R1" H 6171 4738 40  0000 L CNN
F 1 "30K" H 6171 4662 40  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6030 4700 30  0001 C CNN
F 3 "" H 6100 4700 30  0000 C CNN
F 4 "1469914" H 6100 4700 50  0001 C CNN "Ref Farnell"
	1    6100 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5EC1BE7C
P 2700 3700
F 0 "C1" V 2448 3700 50  0000 C CNN
F 1 "1µF" V 2539 3700 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.2mm_W5.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 2738 3550 50  0001 C CNN
F 3 "~" H 2700 3700 50  0001 C CNN
F 4 "1890265" H 2700 3700 50  0001 C CNN "Ref Farnell"
	1    2700 3700
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5EC1C669
P 3050 3850
F 0 "C2" V 2798 3850 50  0000 C CNN
F 1 "1µF" V 2889 3850 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.2mm_W5.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 3088 3700 50  0001 C CNN
F 3 "~" H 3050 3850 50  0001 C CNN
F 4 "1890265" H 3050 3850 50  0001 C CNN "Ref Farnell"
	1    3050 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	2200 3850 2900 3850
Wire Wire Line
	3400 1900 3400 2100
Wire Wire Line
	3400 1450 3650 1450
Wire Wire Line
	3650 1450 3650 1350
Text Label 3550 800  0    50   ~ 0
V+
Text Label 3550 2100 0    50   ~ 0
V-
Wire Wire Line
	3200 3850 3900 3850
Wire Wire Line
	4950 3850 4950 4150
Wire Wire Line
	4950 4150 6450 4150
Connection ~ 6450 4150
Wire Wire Line
	2850 3700 3450 3700
Wire Wire Line
	4950 3700 4950 3450
Wire Wire Line
	4950 3450 6450 3450
Connection ~ 6450 3450
$Comp
L PowerAmp-rescue:R-Amplifier-cache-PowerAmp-rescue-PowerAmp-rescue R5
U 1 1 5EC76CB2
P 3450 4350
F 0 "R5" H 3521 4388 40  0000 L CNN
F 1 "22K" H 3521 4312 40  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3380 4350 30  0001 C CNN
F 3 "" H 3450 4350 30  0000 C CNN
F 4 "1577677" H 3450 4350 50  0001 C CNN "Ref Farnell"
	1    3450 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 4600 3450 4700
$Comp
L PowerAmp-rescue:R-Amplifier-cache-PowerAmp-rescue-PowerAmp-rescue R6
U 1 1 5EC76CBD
P 3900 4350
F 0 "R6" H 3971 4388 40  0000 L CNN
F 1 "22K" H 3971 4312 40  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3830 4350 30  0001 C CNN
F 3 "" H 3900 4350 30  0000 C CNN
F 4 "1577677" H 3900 4350 50  0001 C CNN "Ref Farnell"
	1    3900 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 4600 3900 4700
Wire Wire Line
	3900 4700 4000 4700
Text Label 4000 4700 0    50   ~ 0
GNDA
Wire Wire Line
	3450 4700 3900 4700
Connection ~ 3900 4700
Wire Wire Line
	3450 3700 3450 4100
Connection ~ 3450 3700
Wire Wire Line
	3450 3700 4950 3700
Wire Wire Line
	3900 4100 3900 3850
Connection ~ 3900 3850
Wire Wire Line
	3900 3850 4950 3850
Wire Wire Line
	3400 800  3550 800 
Connection ~ 3400 800 
Wire Wire Line
	3400 2100 3550 2100
Connection ~ 3400 2100
$EndSCHEMATC
