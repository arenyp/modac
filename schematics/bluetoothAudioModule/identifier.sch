EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Memory_EEPROM:AT25xxx U2
U 1 1 5EC8AF81
P 5300 3250
F 0 "U2" H 5300 3731 50  0000 C CNN
F 1 "AT25xxx" H 5300 3640 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5300 3250 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8707-SEEPROM-AT25010B-020B-040B-Datasheet.pdf" H 5300 3250 50  0001 C CNN
	1    5300 3250
	1    0    0    -1  
$EndComp
Text HLabel 5250 3650 0    50   Input ~ 0
GND
Text HLabel 4950 2950 0    50   Input ~ 0
VDD
Text HLabel 5950 3250 2    50   Input ~ 0
SDI
Text HLabel 5950 3350 2    50   Input ~ 0
SDO
Text HLabel 4650 3350 0    50   Input ~ 0
CS
Wire Wire Line
	4900 3350 4650 3350
Wire Wire Line
	5700 3250 5950 3250
Wire Wire Line
	5700 3350 5950 3350
Wire Wire Line
	4950 2950 5300 2950
Wire Wire Line
	5300 3550 5300 3650
Wire Wire Line
	5300 3650 5250 3650
Text HLabel 5950 3150 2    50   Input ~ 0
CLK
Wire Wire Line
	5700 3150 5950 3150
Wire Wire Line
	4900 3150 4650 3150
Wire Wire Line
	4650 3250 4900 3250
Text Label 5000 2950 0    50   ~ 0
VDD
Text Label 4650 3250 0    50   ~ 0
VDD
Text Label 4650 3150 0    50   ~ 0
VDD
Text Notes 3400 3900 0    50   ~ 0
This device by giving an identifier to the board, allows to mix DACs ADCs and others like toslink IO.
$EndSCHEMATC
