EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:CP1 C4
U 1 1 5BE6D6CD
P 4650 5700
F 0 "C4" H 4765 5746 50  0000 L CNN
F 1 "0.1uF" H 4765 5655 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.2mm_W5.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 4650 5700 50  0001 C CNN
F 3 "~" H 4650 5700 50  0001 C CNN
	1    4650 5700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C19
U 1 1 5BE6E0D5
P 4650 6200
F 0 "C19" H 4535 6154 50  0000 R CNN
F 1 "0.1uF" H 4535 6245 50  0000 R CNN
F 2 "Capacitor_THT:C_Rect_L7.2mm_W5.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 4650 6200 50  0001 C CNN
F 3 "~" H 4650 6200 50  0001 C CNN
	1    4650 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 5850 4650 5950
Wire Wire Line
	3550 5650 3550 5450
Wire Wire Line
	4650 5450 4650 5550
Wire Wire Line
	3550 6250 3550 6450
Wire Wire Line
	4650 6450 4650 6350
Connection ~ 4650 5950
Wire Wire Line
	4650 5950 5300 5950
Wire Wire Line
	4650 5950 4650 6050
Wire Wire Line
	5300 1000 5550 1000
Wire Wire Line
	5550 1000 5550 1600
Wire Wire Line
	5550 1600 5200 1600
Connection ~ 5550 1000
Wire Wire Line
	4550 1100 4700 1100
Connection ~ 4650 5450
Connection ~ 4650 6450
Wire Wire Line
	4650 5250 4900 5250
Wire Wire Line
	4650 6650 4900 6650
Text HLabel 3850 900  0    50   Input ~ 0
DACL
Text HLabel 7850 1100 2    50   Input ~ 0
PREAMPL
Text HLabel 4000 2350 0    50   Input ~ 0
GNDA
Wire Wire Line
	4300 3100 4450 3100
Text HLabel 3850 2900 0    50   Input ~ 0
DACR
Text HLabel 7850 3100 2    50   Input ~ 0
PREAMPR
Text Label 4400 2350 2    50   ~ 0
GNDA
Text Label 4400 4350 0    50   ~ 0
GNDA
Text Label 5300 5950 0    50   ~ 0
GNDA
Text HLabel 4900 5250 2    50   Input ~ 0
+15V
Text HLabel 4900 6650 2    50   Input ~ 0
-15V
Wire Wire Line
	6600 1200 6600 1400
Wire Wire Line
	6600 1400 7400 1400
Wire Wire Line
	7400 1400 7400 1100
Wire Wire Line
	7400 1100 7300 1100
Connection ~ 7400 1100
Wire Wire Line
	6600 1200 6700 1200
Wire Wire Line
	6600 3200 6600 3400
Wire Wire Line
	6600 3400 7400 3400
Wire Wire Line
	7400 3400 7400 3100
Wire Wire Line
	7400 3100 7300 3100
Connection ~ 7400 3100
Wire Wire Line
	6600 3200 6700 3200
Wire Wire Line
	3550 6450 3050 6450
Wire Wire Line
	3050 6450 3050 6300
Connection ~ 3550 6450
Wire Wire Line
	3050 5700 3050 5450
Wire Wire Line
	3050 5450 3550 5450
Connection ~ 3550 5450
Wire Wire Line
	3550 5450 4650 5450
Wire Wire Line
	3550 6450 4650 6450
$Comp
L Device:R_POT_TRIM RV2
U 1 1 5BF0FA58
P 5050 1600
F 0 "RV2" H 4980 1646 50  0000 R CNN
F 1 "20K" H 4980 1555 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296W_Vertical" H 5050 1600 50  0001 C CNN
F 3 "~" H 5050 1600 50  0001 C CNN
	1    5050 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 1100 4550 1400
$Comp
L Device:R_POT_TRIM RV1
U 1 1 5BF0FC94
P 4900 3600
F 0 "RV1" H 4830 3646 50  0000 R CNN
F 1 "20K" H 4830 3555 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296W_Vertical" H 4900 3600 50  0001 C CNN
F 3 "~" H 4900 3600 50  0001 C CNN
	1    4900 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3000 5550 3000
Connection ~ 5550 3000
Wire Wire Line
	5050 1450 5050 1400
Wire Wire Line
	5050 1400 4550 1400
Connection ~ 4550 1400
Wire Wire Line
	4300 3100 4300 3400
Wire Wire Line
	5550 3600 5050 3600
Wire Wire Line
	5550 3000 5550 3600
Wire Wire Line
	4900 3450 4900 3400
Wire Wire Line
	4900 3400 4300 3400
Connection ~ 4300 3400
NoConn ~ 5050 1750
NoConn ~ 4900 3750
Wire Wire Line
	4650 6450 4650 6650
Wire Wire Line
	4650 5250 4650 5450
$Comp
L Amplifier_Operational:OPA1602 U2
U 1 1 5CF06BBE
P 5000 1000
F 0 "U2" H 5000 1367 50  0000 C CNN
F 1 "OPA1602" H 5000 1276 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5000 1000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa1604.pdf" H 5000 1000 50  0001 C CNN
F 4 "3116895" H 5000 1000 50  0001 C CNN "Ref Farnell"
	1    5000 1000
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA1602 U2
U 2 1 5CF06D9F
P 4750 3000
F 0 "U2" H 4750 3367 50  0000 C CNN
F 1 "OPA1602" H 4750 3276 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4750 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa1604.pdf" H 4750 3000 50  0001 C CNN
F 4 "3116895" H 4750 3000 50  0001 C CNN "Ref Farnell"
	2    4750 3000
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA1602 U2
U 3 1 5CF06E32
P 3150 6000
F 0 "U2" H 3108 6046 50  0000 L CNN
F 1 "OPA1602" H 3108 5955 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3150 6000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa1604.pdf" H 3150 6000 50  0001 C CNN
	3    3150 6000
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA1602 U5
U 1 1 5CF071F7
P 7000 1100
F 0 "U5" H 7000 1467 50  0000 C CNN
F 1 "OPA1602" H 7000 1376 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7000 1100 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa1604.pdf" H 7000 1100 50  0001 C CNN
F 4 "3116895" H 7000 1100 50  0001 C CNN "Ref Farnell"
	1    7000 1100
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA1602 U5
U 2 1 5CF07276
P 7000 3100
F 0 "U5" H 7000 3467 50  0000 C CNN
F 1 "OPA1602" H 7000 3376 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7000 3100 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa1604.pdf" H 7000 3100 50  0001 C CNN
F 4 "3116895" H 7000 3100 50  0001 C CNN "Ref Farnell"
	2    7000 3100
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA1602 U5
U 3 1 5CF072E8
P 3650 5950
F 0 "U5" H 3608 5996 50  0000 L CNN
F 1 "OPA1602" H 3608 5905 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3650 5950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa1604.pdf" H 3650 5950 50  0001 C CNN
	3    3650 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3000 6700 3000
Wire Wire Line
	5550 1000 6700 1000
$Comp
L Device:R R15
U 1 1 5EC3D4C8
P 4300 3950
F 0 "R15" H 4370 3996 50  0000 L CNN
F 1 "1k" H 4370 3905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4230 3950 50  0001 C CNN
F 3 "~" H 4300 3950 50  0001 C CNN
	1    4300 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R16
U 1 1 5EC458FA
P 4550 1950
F 0 "R16" H 4620 1996 50  0000 L CNN
F 1 "1k" H 4620 1905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4480 1950 50  0001 C CNN
F 3 "~" H 4550 1950 50  0001 C CNN
	1    4550 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 1100 7850 1100
Wire Wire Line
	7400 3100 7850 3100
Wire Wire Line
	4550 2100 4550 2350
Wire Wire Line
	4550 1400 4550 1800
Wire Wire Line
	4300 3400 4300 3800
Wire Wire Line
	4300 4100 4300 4350
Wire Wire Line
	4300 4350 4400 4350
Wire Wire Line
	3850 900  4700 900 
Wire Wire Line
	4000 2350 4550 2350
Wire Wire Line
	3850 2900 4450 2900
Text Notes 1050 1100 0    50   ~ 0
Convert to RCA audio Level (2.9V to 1.7V)\nBypass with 0R if not needed?
$EndSCHEMATC
