Bill of materials for the Digital board:

Farnell references

CY8C5888AXI-LP096                                         2630758 x1 or 2932641
USBLC6-2SC6                                               1269406 x1
Con Micro USB                                             2470822 x2
Connector 2 points Phoenix Contact MCV 1,5/ 2-GF-3,81     1793055 x1

Fuse                                250mA 32V             2786391 x1

0.1uF                                                     
1nF
22nF                                                      1414682 x6
200R                                                      2447602 or 1670217  x6

Connector 14 points Micro-MaTch                           3784769 x5

Cable Micromatch  4pts                                    1056208 x10
Cable Micromatch 14pts                                    1056223 x10
                                              
TPS560200DBVT        3121812  0.84€
10µH Wurth inductor  1635988  2.14€
10µf Ceramic low ESR 2528784  0.7 * 2
10µf Ceramic         1867958  0.32
20k 1%               2057640  0.05
61,9K 1%             2303732  0.07


17.2032mhz Quartz                          Mouser         449-LFXTAL063050BULK x1

Clock generator parameters:
44100 * 256 = 11 289 600   / 11059200 = 1.021
48000 * 256 = 12 288 000   1.11
96000 * 256 = 24 576 000   2.22


