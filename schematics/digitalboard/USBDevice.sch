EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6950 3300 2    50   Input ~ 0
D+
Text HLabel 6950 3400 2    50   Input ~ 0
D-
Text HLabel 7000 3800 2    50   Input ~ 0
GND
$Comp
L Connector:USB_B_Micro J?
U 1 1 5E25AF4F
P 6450 3300
F 0 "J?" H 6505 3767 50  0000 C CNN
F 1 "USB_B_Micro" H 6505 3676 50  0000 C CNN
F 2 "" H 6600 3250 50  0001 C CNN
F 3 "~" H 6600 3250 50  0001 C CNN
	1    6450 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3300 6950 3300
Wire Wire Line
	6950 3400 6750 3400
Wire Wire Line
	7000 3800 6450 3800
Wire Wire Line
	6350 3800 6350 3700
Wire Wire Line
	6450 3700 6450 3800
Connection ~ 6450 3800
Wire Wire Line
	6450 3800 6350 3800
NoConn ~ 6750 3500
NoConn ~ 6750 3100
$EndSCHEMATC
