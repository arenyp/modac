EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4950 4250 5250 4250
Wire Wire Line
	4950 4350 5250 4350
Wire Wire Line
	4950 4150 5250 4150
Wire Wire Line
	5250 3850 4950 3850
Wire Wire Line
	4950 3950 5250 3950
Wire Wire Line
	4950 3650 5250 3650
Wire Wire Line
	5250 3750 4950 3750
Wire Wire Line
	4950 3450 5250 3450
Wire Wire Line
	4950 3550 5250 3550
Wire Wire Line
	5250 3250 4950 3250
Text Label 6200 3350 0    50   ~ 0
MCLK4
Text Label 5250 3950 0    50   ~ 0
SDATA4
Text Label 5250 3650 0    50   ~ 0
GND
Text Label 5250 4150 0    50   ~ 0
CSADC4
Text Label 5250 3850 0    50   ~ 0
CCLK4
Text Label 5250 4350 0    50   ~ 0
3.3V
Text Label 5250 4050 0    50   ~ 0
GND
Text Label 5250 4250 0    50   ~ 0
5V
Wire Wire Line
	4950 3150 5250 3150
Wire Wire Line
	4950 3050 5250 3050
Text Label 5250 3050 0    50   ~ 0
GPIO40
Text Label 5250 3150 0    50   ~ 0
GPIO41
$Comp
L Connector:Conn_01x14_Female J?
U 1 1 5EFDBC50
P 4750 3750
AR Path="/5E13E7DE/5EFDBC50" Ref="J?"  Part="1" 
AR Path="/5EFDBC50" Ref="J?"  Part="1" 
AR Path="/5E13E7DE/5EFA53E6/5EFDBC50" Ref="J?"  Part="1" 
F 0 "J?" H 4642 2825 50  0000 C CNN
F 1 "Conn_01x14_Female" H 4642 2916 50  0000 C CNN
F 2 "" H 4750 3750 50  0001 C CNN
F 3 "~" H 4750 3750 50  0001 C CNN
	1    4750 3750
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5EFDBC56
P 5900 3350
AR Path="/5E13E7DE/5EFDBC56" Ref="R?"  Part="1" 
AR Path="/5EFDBC56" Ref="R?"  Part="1" 
AR Path="/5E13E7DE/5EFA53E6/5EFDBC56" Ref="R?"  Part="1" 
F 0 "R?" V 6107 3350 50  0000 C CNN
F 1 "200R" V 6016 3350 50  0000 C CNN
F 2 "" V 5830 3350 50  0001 C CNN
F 3 "~" H 5900 3350 50  0001 C CNN
	1    5900 3350
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5EFDBC5C
P 5700 3550
AR Path="/5E13E7DE/5EFDBC5C" Ref="C?"  Part="1" 
AR Path="/5EFDBC5C" Ref="C?"  Part="1" 
AR Path="/5E13E7DE/5EFA53E6/5EFDBC5C" Ref="C?"  Part="1" 
F 0 "C?" H 5815 3596 50  0000 L CNN
F 1 "20n" H 5815 3505 50  0000 L CNN
F 2 "" H 5738 3400 50  0001 C CNN
F 3 "~" H 5700 3550 50  0001 C CNN
	1    5700 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3350 5700 3350
Wire Wire Line
	5700 3350 5700 3400
Wire Wire Line
	4950 3350 5700 3350
Connection ~ 5700 3350
Wire Wire Line
	5700 3700 5700 4050
Wire Wire Line
	4950 4050 5700 4050
Wire Wire Line
	6050 3350 6200 3350
Text Label 5250 3750 0    50   ~ 0
LRCK4
Text Label 5250 3550 0    50   ~ 0
SCLK4
Text Label 5250 3450 0    50   ~ 0
CDTI4
Text Label 5250 3250 0    50   ~ 0
RST4
Text HLabel 8250 3050 2    50   Input ~ 0
GPIO40
Text HLabel 8250 3150 2    50   Input ~ 0
GPIO41
Text HLabel 8250 4350 2    50   Input ~ 0
RST4
Text HLabel 8250 4250 2    50   Input ~ 0
CDTI4
Text HLabel 8250 3550 2    50   Input ~ 0
SCLK4
Text HLabel 8250 4550 2    50   Input ~ 0
GND
Text HLabel 8250 3650 2    50   Input ~ 0
LRCK4
Text HLabel 8250 4050 2    50   Input ~ 0
CCLK4
Text HLabel 8250 3750 2    50   Input ~ 0
SDATA4
Text HLabel 8250 4150 2    50   Input ~ 0
CSADC4
Text HLabel 8250 3450 2    50   Input ~ 0
MCLK4
Text HLabel 8250 4650 2    50   Input ~ 0
5V
Text HLabel 8250 4750 2    50   Input ~ 0
3.3V
Text Label 8000 3450 0    50   ~ 0
MCLK4
Text Label 8000 3750 0    50   ~ 0
SDATA4
Text Label 8000 4550 0    50   ~ 0
GND
Text Label 8000 4150 0    50   ~ 0
CSADC4
Text Label 8000 4050 0    50   ~ 0
CCLK4
Text Label 8000 4750 0    50   ~ 0
3.3V
Text Label 8000 4650 0    50   ~ 0
5V
Text Label 8000 3050 0    50   ~ 0
GPIO40
Text Label 8000 3150 0    50   ~ 0
GPIO41
Text Label 8000 3650 0    50   ~ 0
LRCK4
Text Label 8000 3550 0    50   ~ 0
SCLK4
Text Label 8000 4250 0    50   ~ 0
CDTI4
Text Label 8000 4350 0    50   ~ 0
RST4
Wire Wire Line
	8250 4750 8000 4750
Wire Wire Line
	8250 4650 8000 4650
Wire Wire Line
	8250 4550 8000 4550
Wire Wire Line
	8250 4350 8000 4350
Wire Wire Line
	8250 4250 8000 4250
Wire Wire Line
	8250 4150 8000 4150
Wire Wire Line
	8250 4050 8000 4050
Wire Wire Line
	8250 3750 8000 3750
Wire Wire Line
	8250 3650 8000 3650
Wire Wire Line
	8250 3550 8000 3550
Wire Wire Line
	8250 3450 8000 3450
Wire Wire Line
	8250 3150 8000 3150
Wire Wire Line
	8250 3050 8000 3050
$EndSCHEMATC
