EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x01_Female J5
U 1 1 5CB12040
P 5100 1150
F 0 "J5" H 5127 1176 50  0000 L CNN
F 1 "Conn_01x01_Female" H 5127 1085 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 5100 1150 50  0001 C CNN
F 3 "~" H 5100 1150 50  0001 C CNN
	1    5100 1150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J6
U 1 1 5CB1F54E
P 5100 1350
F 0 "J6" H 5127 1376 50  0000 L CNN
F 1 "Modac Digital Board" H 5127 1285 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 5100 1350 50  0001 C CNN
F 3 "~" H 5100 1350 50  0001 C CNN
	1    5100 1350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J3
U 1 1 5CB46CD5
P 5100 750
F 0 "J3" H 5127 776 50  0000 L CNN
F 1 "Conn_01x01_Female" H 5127 685 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 5100 750 50  0001 C CNN
F 3 "~" H 5100 750 50  0001 C CNN
	1    5100 750 
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J4
U 1 1 5CB46CDB
P 5100 950
F 0 "J4" H 5127 976 50  0000 L CNN
F 1 "Conn_01x01_Female" H 5127 885 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 5100 950 50  0001 C CNN
F 3 "~" H 5100 950 50  0001 C CNN
	1    5100 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 750  4750 750 
Wire Wire Line
	4750 750  4750 950 
Wire Wire Line
	4750 1350 4900 1350
Wire Wire Line
	4750 1350 4500 1350
Connection ~ 4750 1350
Wire Wire Line
	4900 1150 4750 1150
Connection ~ 4750 1150
Wire Wire Line
	4750 1150 4750 1350
Wire Wire Line
	4900 950  4750 950 
Connection ~ 4750 950 
Wire Wire Line
	4750 950  4750 1150
Text Label 4500 1350 0    50   ~ 0
GND
Wire Wire Line
	7350 5050 7000 5050
Wire Wire Line
	7350 5150 7000 5150
Text Label 7000 5050 0    50   ~ 0
GND
Text Label 7000 5150 0    50   ~ 0
3.3V
Wire Wire Line
	2600 4750 2600 4650
Wire Wire Line
	2600 5050 2600 5150
Wire Wire Line
	2600 5150 2750 5150
Connection ~ 2600 5150
Text Label 2600 4650 0    50   ~ 0
3.3V
Wire Wire Line
	2700 6050 2750 6050
Text Label 2750 6050 0    50   ~ 0
GND
$Comp
L Device:R R18
U 1 1 5D661552
P 2050 4900
F 0 "R18" H 2120 4946 50  0000 L CNN
F 1 "3K" H 2120 4855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1980 4900 50  0001 C CNN
F 3 "~" H 2050 4900 50  0001 C CNN
	1    2050 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 4750 2050 4650
Wire Wire Line
	2050 5050 2050 5150
Wire Wire Line
	2050 5150 2200 5150
Connection ~ 2050 5150
Wire Wire Line
	2050 5150 2050 5250
Text Label 2050 4650 0    50   ~ 0
3.3V
Text Label 2200 5150 0    50   ~ 0
SEL0
Text Label 2750 5150 0    50   ~ 0
SEL1
Wire Wire Line
	3150 4750 3150 4650
Wire Wire Line
	3150 5050 3150 5150
Wire Wire Line
	3150 5150 3300 5150
Connection ~ 3150 5150
Wire Wire Line
	3150 5150 3150 5250
Text Label 3150 4650 0    50   ~ 0
3.3V
Text Label 3300 5150 0    50   ~ 0
SEL2
Text Label 6950 4650 0    50   ~ 0
SEL0
Text Label 6950 4750 0    50   ~ 0
SEL1
Text Label 6950 4850 0    50   ~ 0
SEL2
Wire Wire Line
	6950 4650 7350 4650
Wire Wire Line
	7350 4750 6950 4750
Wire Wire Line
	6950 4850 7350 4850
Wire Wire Line
	3150 5250 2700 5250
Wire Wire Line
	2700 5250 2700 5350
Wire Wire Line
	2500 5350 2500 5250
Wire Wire Line
	2500 5250 2050 5250
Wire Wire Line
	2600 5150 2600 5350
Wire Wire Line
	2700 5850 2700 5950
Wire Wire Line
	2600 5850 2600 5950
Wire Wire Line
	2600 5950 2700 5950
Connection ~ 2700 5950
Wire Wire Line
	2700 5950 2700 6050
Wire Wire Line
	2600 5950 2500 5950
Wire Wire Line
	2500 5950 2500 5850
Connection ~ 2600 5950
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J13
U 1 1 5C8A9142
P 2600 5550
F 0 "J13" V 2604 5730 50  0000 L CNN
F 1 "SEL 2 1 0" V 2695 5730 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 2600 5550 50  0001 C CNN
F 3 "~" H 2600 5550 50  0001 C CNN
	1    2600 5550
	0    1    1    0   
$EndComp
Wire Wire Line
	6150 3300 6500 3300
Text Label 6500 3300 0    50   ~ 0
GND
$Sheet
S 2200 850  500  450 
U 5E1D512D
F0 "Power" 50
F1 "Power.sch" 50
F2 "3.3V" I R 2700 1050 50 
F3 "GND" I R 2700 1200 50 
$EndSheet
Wire Wire Line
	2700 1050 2850 1050
Wire Wire Line
	2700 1200 2850 1200
Text Label 2850 1050 0    50   ~ 0
3.3V
Text Label 2850 1200 0    50   ~ 0
GND
Text Label 6500 3100 0    50   ~ 0
TX
Text Label 6500 3200 0    50   ~ 0
RX
$Sheet
S 7350 2300 1550 3250
U 5E13E7DE
F0 "MSU" 50
F1 "MCU.sch" 50
F2 "3.3V" I L 7350 5150 50 
F3 "VSS" I L 7350 5050 50 
F4 "SEL0" I L 7350 4650 50 
F5 "SEL1" I L 7350 4750 50 
F6 "SEL2" I L 7350 4850 50 
F7 "RX" I L 7350 3200 50 
F8 "TX" O L 7350 3100 50 
F9 "VOLUME" I L 7350 3950 50 
$EndSheet
Wire Wire Line
	6150 3100 7350 3100
Wire Wire Line
	6150 3200 7350 3200
$Comp
L Connector:Conn_01x03_Male J8
U 1 1 6032719F
P 5950 3200
F 0 "J8" H 5922 3132 50  0000 R CNN
F 1 "Conn_01x03_Male" H 5922 3223 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5950 3200 50  0001 C CNN
F 3 "~" H 5950 3200 50  0001 C CNN
	1    5950 3200
	1    0    0    1   
$EndComp
Text Label 2750 3450 0    50   ~ 0
GND
Text Label 2700 2500 0    50   ~ 0
3.3V
Text Label 3100 3000 0    50   ~ 0
VOLUME
$Comp
L Device:C C?
U 1 1 60381434
P 2700 3200
AR Path="/5E13E7DE/60381434" Ref="C?"  Part="1" 
AR Path="/60381434" Ref="C1"  Part="1" 
F 0 "C1" H 2815 3246 50  0000 L CNN
F 1 "0.1uF" H 2815 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2738 3050 50  0001 C CNN
F 3 "~" H 2700 3200 50  0001 C CNN
	1    2700 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 3000 2700 3050
Connection ~ 2700 3000
Wire Wire Line
	2700 3350 2700 3450
Wire Wire Line
	2700 3450 2750 3450
$Comp
L Device:R R?
U 1 1 6038143E
P 2700 2700
AR Path="/5E13E7DE/6038143E" Ref="R?"  Part="1" 
AR Path="/6038143E" Ref="R6"  Part="1" 
F 0 "R6" H 2770 2746 50  0000 L CNN
F 1 "1K" H 2770 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2630 2700 50  0001 C CNN
F 3 "~" H 2700 2700 50  0001 C CNN
	1    2700 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2500 2700 2550
Wire Wire Line
	2700 2850 2700 3000
Wire Wire Line
	2700 3000 3100 3000
Text Label 1850 2900 0    50   ~ 0
GND
Wire Wire Line
	1650 2900 1850 2900
Text Notes 950  3550 0    50   ~ 0
It is better to use analog volume. In order to get the full 24bit resolution even at low volume.
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J7
U 1 1 60425B5C
P 1350 2900
F 0 "J7" H 1400 3117 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 1400 3026 50  0000 C CNN
F 2 "lib:ConnectorMicromatch-4" H 1350 2900 50  0001 C CNN
F 3 "~" H 1350 2900 50  0001 C CNN
	1    1350 2900
	1    0    0    -1  
$EndComp
Text Label 850  3000 0    50   ~ 0
GND
Wire Wire Line
	850  3000 1150 3000
Wire Wire Line
	1650 3000 1950 3000
Wire Wire Line
	1950 3000 1950 3100
Wire Wire Line
	1950 3100 700  3100
Wire Wire Line
	700  3100 700  2900
Wire Wire Line
	700  2900 1150 2900
Connection ~ 1950 3000
Wire Wire Line
	1950 3000 2700 3000
Text Notes 900  2550 0    50   ~ 0
Re use the analog volume module
Text Label 6950 3950 0    50   ~ 0
VOLUME
Wire Wire Line
	7350 3950 6950 3950
$Comp
L Device:R R13
U 1 1 6051261F
P 2600 4900
F 0 "R13" H 2670 4946 50  0000 L CNN
F 1 "3K" H 2670 4855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2530 4900 50  0001 C CNN
F 3 "~" H 2600 4900 50  0001 C CNN
	1    2600 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 60512944
P 3150 4900
F 0 "R14" H 3220 4946 50  0000 L CNN
F 1 "3K" H 3220 4855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3080 4900 50  0001 C CNN
F 3 "~" H 3150 4900 50  0001 C CNN
	1    3150 4900
	1    0    0    -1  
$EndComp
$EndSCHEMATC
