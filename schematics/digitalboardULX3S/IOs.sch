EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	1100 3650 1400 3650
Wire Wire Line
	1100 3750 1400 3750
Wire Wire Line
	1100 3550 1400 3550
Wire Wire Line
	1400 3250 1100 3250
Wire Wire Line
	1100 2850 1400 2850
Wire Wire Line
	1400 2650 1100 2650
Text Label 2450 2750 0    50   ~ 0
MCLK1
Text Label 1400 2850 0    50   ~ 0
D1RST
Text Label 1400 2950 0    50   ~ 0
SCLK1
Text Label 1400 3050 0    50   ~ 0
SDATA1
Text Label 1400 3150 0    50   ~ 0
LRCK1
Text Label 1400 3350 0    50   ~ 0
GND
Text Label 1400 3550 0    50   ~ 0
CSDAC1
Text Label 1400 3250 0    50   ~ 0
CCLK
Text Label 1400 3450 0    50   ~ 0
CDTI
Text Label 1400 3750 0    50   ~ 0
3.3V
Text Label 1400 2650 0    50   ~ 0
GND
Wire Wire Line
	1100 6950 1400 6950
Wire Wire Line
	1100 7050 1400 7050
Wire Wire Line
	1100 6850 1400 6850
Wire Wire Line
	1400 6550 1100 6550
Wire Wire Line
	1100 6150 1400 6150
Wire Wire Line
	1400 5950 1100 5950
Text Label 2450 6050 0    50   ~ 0
MCLK3
Text Label 1400 6150 0    50   ~ 0
D3RST
Text Label 1400 6250 0    50   ~ 0
SCLK3
Text Label 1400 6350 0    50   ~ 0
SDATA3
Text Label 1400 6450 0    50   ~ 0
LRCK3
Text Label 1400 6650 0    50   ~ 0
GND
Text Label 1400 6850 0    50   ~ 0
CSDAC3
Text Label 1400 6550 0    50   ~ 0
CCLK
Text Label 1400 6750 0    50   ~ 0
CDTI
Text Label 1400 7050 0    50   ~ 0
3.3V
Text Label 1400 5950 0    50   ~ 0
GND
Wire Wire Line
	1100 5300 1400 5300
Wire Wire Line
	1100 5400 1400 5400
Wire Wire Line
	1100 5200 1400 5200
Wire Wire Line
	1400 4900 1100 4900
Wire Wire Line
	1100 4500 1400 4500
Wire Wire Line
	1400 4300 1100 4300
Text Label 2450 4400 0    50   ~ 0
MCLK2
Text Label 1400 4500 0    50   ~ 0
D2RST
Text Label 1400 4600 0    50   ~ 0
SCLK2
Text Label 1400 4700 0    50   ~ 0
SDATA2
Text Label 1400 4800 0    50   ~ 0
LRCK2
Text Label 1400 5000 0    50   ~ 0
GND
Text Label 1400 5200 0    50   ~ 0
CSDAC2
Text Label 1400 4900 0    50   ~ 0
CCLK
Text Label 1400 5100 0    50   ~ 0
CDTI
Text Label 1400 5400 0    50   ~ 0
3.3V
Text Label 1400 4300 0    50   ~ 0
GND
Wire Wire Line
	1100 2550 1400 2550
Wire Wire Line
	1100 2450 1400 2450
Text Label 1400 2550 0    50   ~ 0
CSIDENT1
Wire Wire Line
	1100 5850 1400 5850
Wire Wire Line
	1100 5750 1400 5750
Text Label 1400 5850 0    50   ~ 0
CSIDENT3
Text Label 1400 4200 0    50   ~ 0
CSIDENT2
Wire Wire Line
	1100 4100 1400 4100
Wire Wire Line
	1100 4200 1400 4200
$Comp
L Connector:Conn_01x14_Female J?
U 1 1 6051FD80
P 900 6450
AR Path="/6042B812/5E13E7DE/6051FD80" Ref="J?"  Part="1" 
AR Path="/605149D1/6051FD80" Ref="J?"  Part="1" 
F 0 "J?" H 792 5525 50  0000 C CNN
F 1 "Conn_01x14_Female" H 792 5616 50  0000 C CNN
F 2 "lib:ConnectorMicromatch-14" H 900 6450 50  0001 C CNN
F 3 "~" H 900 6450 50  0001 C CNN
F 4 "3784769" H 900 6450 50  0001 C CNN "Ref Farnell"
	1    900  6450
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x14_Female J?
U 1 1 6051FD87
P 900 4800
AR Path="/6042B812/5E13E7DE/6051FD87" Ref="J?"  Part="1" 
AR Path="/605149D1/6051FD87" Ref="J?"  Part="1" 
F 0 "J?" H 792 3875 50  0000 C CNN
F 1 "Conn_01x14_Female" H 792 3966 50  0000 C CNN
F 2 "lib:ConnectorMicromatch-14" H 900 4800 50  0001 C CNN
F 3 "~" H 900 4800 50  0001 C CNN
F 4 "3784769" H 900 4800 50  0001 C CNN "Ref Farnell"
	1    900  4800
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 6051FD8E
P 2150 6050
AR Path="/6042B812/5E13E7DE/6051FD8E" Ref="R?"  Part="1" 
AR Path="/605149D1/6051FD8E" Ref="R?"  Part="1" 
F 0 "R?" V 2357 6050 50  0000 C CNN
F 1 "200R" V 2266 6050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2080 6050 50  0001 C CNN
F 3 "~" H 2150 6050 50  0001 C CNN
F 4 "2447602" H 2150 6050 50  0001 C CNN "Ref Farnell"
	1    2150 6050
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 6051FD95
P 1950 6250
AR Path="/6042B812/5E13E7DE/6051FD95" Ref="C?"  Part="1" 
AR Path="/605149D1/6051FD95" Ref="C?"  Part="1" 
F 0 "C?" H 2065 6296 50  0000 L CNN
F 1 "20n" H 2065 6205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1988 6100 50  0001 C CNN
F 3 "~" H 1950 6250 50  0001 C CNN
F 4 "1414682" H 1950 6250 50  0001 C CNN "Ref Farnell"
	1    1950 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 6050 1950 6050
Wire Wire Line
	1950 6050 1950 6100
$Comp
L Device:R R?
U 1 1 6051FD9E
P 2150 4400
AR Path="/6042B812/5E13E7DE/6051FD9E" Ref="R?"  Part="1" 
AR Path="/605149D1/6051FD9E" Ref="R?"  Part="1" 
F 0 "R?" V 2357 4400 50  0000 C CNN
F 1 "200R" V 2266 4400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2080 4400 50  0001 C CNN
F 3 "~" H 2150 4400 50  0001 C CNN
F 4 "2447602" H 2150 4400 50  0001 C CNN "Ref Farnell"
	1    2150 4400
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 6051FDA5
P 1950 4600
AR Path="/6042B812/5E13E7DE/6051FDA5" Ref="C?"  Part="1" 
AR Path="/605149D1/6051FDA5" Ref="C?"  Part="1" 
F 0 "C?" H 2065 4646 50  0000 L CNN
F 1 "20n" H 2065 4555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1988 4450 50  0001 C CNN
F 3 "~" H 1950 4600 50  0001 C CNN
F 4 "1414682" H 1950 4600 50  0001 C CNN "Ref Farnell"
	1    1950 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 4400 1950 4400
Wire Wire Line
	1950 4400 1950 4450
$Comp
L Device:R R?
U 1 1 6051FDAE
P 2150 2750
AR Path="/6042B812/5E13E7DE/6051FDAE" Ref="R?"  Part="1" 
AR Path="/605149D1/6051FDAE" Ref="R?"  Part="1" 
F 0 "R?" V 2357 2750 50  0000 C CNN
F 1 "200R" V 2266 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2080 2750 50  0001 C CNN
F 3 "~" H 2150 2750 50  0001 C CNN
F 4 "2447602" H 2150 2750 50  0001 C CNN "Ref Farnell"
	1    2150 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 6051FDB5
P 1950 2950
AR Path="/6042B812/5E13E7DE/6051FDB5" Ref="C?"  Part="1" 
AR Path="/605149D1/6051FDB5" Ref="C?"  Part="1" 
F 0 "C?" H 2065 2996 50  0000 L CNN
F 1 "20n" H 2065 2905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1988 2800 50  0001 C CNN
F 3 "~" H 1950 2950 50  0001 C CNN
F 4 "1414682" H 1950 2950 50  0001 C CNN "Ref Farnell"
	1    1950 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2750 1950 2750
Wire Wire Line
	1950 2750 1950 2800
Connection ~ 1950 2750
Connection ~ 1950 4400
Connection ~ 1950 6050
Wire Wire Line
	2300 6050 2450 6050
Wire Wire Line
	2300 4400 2450 4400
Wire Wire Line
	2300 2750 2450 2750
Wire Wire Line
	1100 6750 1400 6750
Text Label 1400 6950 0    50   ~ 0
CDTO
Wire Wire Line
	1950 6650 1950 6400
Text Label 1400 5300 0    50   ~ 0
CDTO
Text Label 1400 3650 0    50   ~ 0
CDTO
Wire Wire Line
	1100 3450 1400 3450
Wire Wire Line
	1950 3350 1950 3100
Wire Wire Line
	1100 5100 1400 5100
Wire Wire Line
	1950 5000 1950 4750
Wire Wire Line
	3500 2000 3800 2000
Wire Wire Line
	3500 2100 3800 2100
Wire Wire Line
	3500 1900 3800 1900
Wire Wire Line
	3800 1600 3500 1600
Wire Wire Line
	3500 1200 3800 1200
Wire Wire Line
	3800 1000 3500 1000
Text Label 5000 1100 0    50   ~ 0
MCLK4
Text Label 3800 1400 0    50   ~ 0
SDATA4
Text Label 3800 1000 0    50   ~ 0
GND
Text Label 3800 1900 0    50   ~ 0
CSADC4
Text Label 3800 1600 0    50   ~ 0
CCLK
Text Label 3800 2100 0    50   ~ 0
3.3V
Text Label 3800 1700 0    50   ~ 0
GND
Wire Wire Line
	3500 900  3800 900 
Wire Wire Line
	3500 800  3800 800 
Text Label 3800 900  0    50   ~ 0
CSIDENT4
$Comp
L Connector:Conn_01x14_Female J?
U 1 1 6051FDDF
P 3300 1500
AR Path="/5E13E7DE/6051FDDF" Ref="J?"  Part="1" 
AR Path="/6051FDDF" Ref="J?"  Part="1" 
AR Path="/5E13E7DE/5EFA53E6/6051FDDF" Ref="J?"  Part="1" 
AR Path="/6042B812/5E13E7DE/6051FDDF" Ref="J?"  Part="1" 
AR Path="/605149D1/6051FDDF" Ref="J?"  Part="1" 
F 0 "J?" H 3192 575 50  0000 C CNN
F 1 "Conn_01x14_Female" H 3192 666 50  0000 C CNN
F 2 "lib:ConnectorMicromatch-14" H 3300 1500 50  0001 C CNN
F 3 "~" H 3300 1500 50  0001 C CNN
F 4 "3784769" H 3300 1500 50  0001 C CNN "Ref Farnell"
	1    3300 1500
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 6051FDE6
P 4700 1100
AR Path="/5E13E7DE/6051FDE6" Ref="R?"  Part="1" 
AR Path="/6051FDE6" Ref="R?"  Part="1" 
AR Path="/5E13E7DE/5EFA53E6/6051FDE6" Ref="R?"  Part="1" 
AR Path="/6042B812/5E13E7DE/6051FDE6" Ref="R?"  Part="1" 
AR Path="/605149D1/6051FDE6" Ref="R?"  Part="1" 
F 0 "R?" V 4907 1100 50  0000 C CNN
F 1 "200R" V 4816 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4630 1100 50  0001 C CNN
F 3 "~" H 4700 1100 50  0001 C CNN
F 4 "2447602" H 4700 1100 50  0001 C CNN "Ref Farnell"
	1    4700 1100
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 6051FDED
P 4500 1300
AR Path="/5E13E7DE/6051FDED" Ref="C?"  Part="1" 
AR Path="/6051FDED" Ref="C?"  Part="1" 
AR Path="/5E13E7DE/5EFA53E6/6051FDED" Ref="C?"  Part="1" 
AR Path="/6042B812/5E13E7DE/6051FDED" Ref="C?"  Part="1" 
AR Path="/605149D1/6051FDED" Ref="C?"  Part="1" 
F 0 "C?" H 4615 1346 50  0000 L CNN
F 1 "20n" H 4615 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4538 1150 50  0001 C CNN
F 3 "~" H 4500 1300 50  0001 C CNN
F 4 "1414682" H 4500 1300 50  0001 C CNN "Ref Farnell"
	1    4500 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 1100 4500 1100
Wire Wire Line
	4500 1100 4500 1150
Connection ~ 4500 1100
Wire Wire Line
	4850 1100 5000 1100
Text Label 3800 1500 0    50   ~ 0
LRCK4
Text Label 3800 1300 0    50   ~ 0
SCLK4
Text Label 3800 1200 0    50   ~ 0
RST4
Text Label 3800 1800 0    50   ~ 0
CDTI
Wire Wire Line
	3500 1800 3800 1800
Text Label 3800 2000 0    50   ~ 0
CDTO
Wire Wire Line
	4500 1700 4500 1450
Text Label 1400 1800 0    50   ~ 0
CDTI
Text Label 1400 2000 0    50   ~ 0
CDTO
Text Label 1400 1600 0    50   ~ 0
CCLK
Wire Wire Line
	1100 2000 1400 2000
Wire Wire Line
	1100 2100 1400 2100
Wire Wire Line
	1100 1900 1400 1900
Wire Wire Line
	1400 1600 1100 1600
Wire Wire Line
	1100 1200 1400 1200
Wire Wire Line
	1400 1000 1100 1000
Text Label 2700 1100 0    50   ~ 0
MCLK5
Text Label 1400 1700 0    50   ~ 0
GND
Text Label 1400 2100 0    50   ~ 0
3.3V
Wire Wire Line
	1100 900  1400 900 
Wire Wire Line
	1100 800  1400 800 
$Comp
L Connector:Conn_01x14_Female J?
U 1 1 6051FE0D
P 900 1500
AR Path="/5E13E7DE/6051FE0D" Ref="J?"  Part="1" 
AR Path="/6051FE0D" Ref="J?"  Part="1" 
AR Path="/5E13E7DE/5EFA53E6/6051FE0D" Ref="J?"  Part="1" 
AR Path="/6042B812/5E13E7DE/6051FE0D" Ref="J?"  Part="1" 
AR Path="/605149D1/6051FE0D" Ref="J?"  Part="1" 
F 0 "J?" H 792 575 50  0000 C CNN
F 1 "Conn_01x14_Female" H 792 666 50  0000 C CNN
F 2 "lib:ConnectorMicromatch-14" H 900 1500 50  0001 C CNN
F 3 "~" H 900 1500 50  0001 C CNN
F 4 "3784769" H 900 1500 50  0001 C CNN "Ref Farnell"
	1    900  1500
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 6051FE14
P 2400 1100
AR Path="/5E13E7DE/6051FE14" Ref="R?"  Part="1" 
AR Path="/6051FE14" Ref="R?"  Part="1" 
AR Path="/5E13E7DE/5EFA53E6/6051FE14" Ref="R?"  Part="1" 
AR Path="/6042B812/5E13E7DE/6051FE14" Ref="R?"  Part="1" 
AR Path="/605149D1/6051FE14" Ref="R?"  Part="1" 
F 0 "R?" V 2607 1100 50  0000 C CNN
F 1 "200R" V 2516 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2330 1100 50  0001 C CNN
F 3 "~" H 2400 1100 50  0001 C CNN
F 4 "2447602" H 2400 1100 50  0001 C CNN "Ref Farnell"
	1    2400 1100
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 6051FE1B
P 2200 1300
AR Path="/5E13E7DE/6051FE1B" Ref="C?"  Part="1" 
AR Path="/6051FE1B" Ref="C?"  Part="1" 
AR Path="/5E13E7DE/5EFA53E6/6051FE1B" Ref="C?"  Part="1" 
AR Path="/6042B812/5E13E7DE/6051FE1B" Ref="C?"  Part="1" 
AR Path="/605149D1/6051FE1B" Ref="C?"  Part="1" 
F 0 "C?" H 2315 1346 50  0000 L CNN
F 1 "20n" H 2315 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2238 1150 50  0001 C CNN
F 3 "~" H 2200 1300 50  0001 C CNN
F 4 "1414682" H 2200 1300 50  0001 C CNN "Ref Farnell"
	1    2200 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 1100 2200 1100
Wire Wire Line
	2200 1100 2200 1150
Connection ~ 2200 1100
Wire Wire Line
	2550 1100 2700 1100
Text Label 1400 1000 0    50   ~ 0
GND
Text Label 1400 1500 0    50   ~ 0
LRCKPSC
Text Label 1400 1900 0    50   ~ 0
CSTOSLNK
Text Label 1400 1300 0    50   ~ 0
SCLKPSC
Text Label 1400 1400 0    50   ~ 0
SDATAPSC
Text Label 1400 800  0    50   ~ 0
SPDIFOUT
Wire Wire Line
	1100 1800 1400 1800
Wire Wire Line
	2200 1700 2200 1450
Text Label 1400 900  0    50   ~ 0
CSIDENTPSC
Text Label 1400 1200 0    50   ~ 0
SPDIFRESET
Wire Wire Line
	3500 1700 4500 1700
$Comp
L Connector:Conn_01x14_Female J?
U 1 1 6051FE32
P 900 3150
AR Path="/6042B812/5E13E7DE/6051FE32" Ref="J?"  Part="1" 
AR Path="/605149D1/6051FE32" Ref="J?"  Part="1" 
F 0 "J?" H 792 2225 50  0000 C CNN
F 1 "Conn_01x14_Female" H 792 2316 50  0000 C CNN
F 2 "lib:ConnectorMicromatch-14" H 900 3150 50  0001 C CNN
F 3 "~" H 900 3150 50  0001 C CNN
F 4 "3784769" H 900 3150 50  0001 C CNN "Ref Farnell"
	1    900  3150
	-1   0    0    1   
$EndComp
Wire Wire Line
	1100 4400 1950 4400
Wire Wire Line
	1100 5000 1950 5000
Wire Wire Line
	1100 3350 1950 3350
Wire Wire Line
	1100 2750 1950 2750
Wire Wire Line
	1100 6050 1950 6050
Wire Wire Line
	1100 6650 1950 6650
Wire Wire Line
	3500 1100 4500 1100
Wire Wire Line
	1100 1100 2200 1100
Wire Wire Line
	1100 1700 2200 1700
Wire Wire Line
	1100 2950 1400 2950
Wire Wire Line
	1100 3050 1400 3050
Wire Wire Line
	1100 3150 1400 3150
Wire Wire Line
	1100 4600 1400 4600
Wire Wire Line
	1100 4700 1400 4700
Wire Wire Line
	1100 4800 1400 4800
Wire Wire Line
	1100 6250 1400 6250
Wire Wire Line
	1100 6350 1400 6350
Wire Wire Line
	1100 6450 1400 6450
Wire Wire Line
	3500 1300 3800 1300
Wire Wire Line
	3500 1400 3800 1400
Wire Wire Line
	3500 1500 3800 1500
Wire Wire Line
	1100 1300 1400 1300
Wire Wire Line
	1100 1400 1400 1400
Wire Wire Line
	1100 1500 1400 1500
Text Label 3800 3450 0    50   ~ 0
CDTI
Text Label 3800 3650 0    50   ~ 0
CDTO
Text Label 3800 3250 0    50   ~ 0
CCLK
Wire Wire Line
	3500 3650 3800 3650
Wire Wire Line
	3500 3750 3800 3750
Wire Wire Line
	3500 3550 3800 3550
Wire Wire Line
	3800 3250 3500 3250
Wire Wire Line
	3500 2850 3800 2850
Wire Wire Line
	3800 2650 3500 2650
Text Label 5100 2750 0    50   ~ 0
MCLK5
Text Label 3800 3350 0    50   ~ 0
GND
Text Label 3800 3750 0    50   ~ 0
3.3V
Wire Wire Line
	3500 2550 3800 2550
Wire Wire Line
	3500 2450 3800 2450
$Comp
L Connector:Conn_01x14_Female J?
U 1 1 606D737E
P 3300 3150
AR Path="/5E13E7DE/606D737E" Ref="J?"  Part="1" 
AR Path="/606D737E" Ref="J?"  Part="1" 
AR Path="/5E13E7DE/5EFA53E6/606D737E" Ref="J?"  Part="1" 
AR Path="/6042B812/5E13E7DE/606D737E" Ref="J?"  Part="1" 
AR Path="/605149D1/606D737E" Ref="J?"  Part="1" 
F 0 "J?" H 3192 2225 50  0000 C CNN
F 1 "Conn_01x14_Female" H 3192 2316 50  0000 C CNN
F 2 "lib:ConnectorMicromatch-14" H 3300 3150 50  0001 C CNN
F 3 "~" H 3300 3150 50  0001 C CNN
F 4 "3784769" H 3300 3150 50  0001 C CNN "Ref Farnell"
	1    3300 3150
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 606D7389
P 4800 2750
AR Path="/5E13E7DE/606D7389" Ref="R?"  Part="1" 
AR Path="/606D7389" Ref="R?"  Part="1" 
AR Path="/5E13E7DE/5EFA53E6/606D7389" Ref="R?"  Part="1" 
AR Path="/6042B812/5E13E7DE/606D7389" Ref="R?"  Part="1" 
AR Path="/605149D1/606D7389" Ref="R?"  Part="1" 
F 0 "R?" V 5007 2750 50  0000 C CNN
F 1 "200R" V 4916 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4730 2750 50  0001 C CNN
F 3 "~" H 4800 2750 50  0001 C CNN
F 4 "2447602" H 4800 2750 50  0001 C CNN "Ref Farnell"
	1    4800 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 606D7394
P 4600 2950
AR Path="/5E13E7DE/606D7394" Ref="C?"  Part="1" 
AR Path="/606D7394" Ref="C?"  Part="1" 
AR Path="/5E13E7DE/5EFA53E6/606D7394" Ref="C?"  Part="1" 
AR Path="/6042B812/5E13E7DE/606D7394" Ref="C?"  Part="1" 
AR Path="/605149D1/606D7394" Ref="C?"  Part="1" 
F 0 "C?" H 4715 2996 50  0000 L CNN
F 1 "20n" H 4715 2905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4638 2800 50  0001 C CNN
F 3 "~" H 4600 2950 50  0001 C CNN
F 4 "1414682" H 4600 2950 50  0001 C CNN "Ref Farnell"
	1    4600 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 2750 4600 2750
Wire Wire Line
	4600 2750 4600 2800
Connection ~ 4600 2750
Wire Wire Line
	4950 2750 5100 2750
Text Label 3800 2650 0    50   ~ 0
GND
Text Label 3800 3150 0    50   ~ 0
LRCK5
Text Label 3800 3550 0    50   ~ 0
CS5
Text Label 3800 2950 0    50   ~ 0
SCLK5
Text Label 3800 3050 0    50   ~ 0
SDATA5
Text Label 3800 2450 0    50   ~ 0
GPIO5
Wire Wire Line
	3500 3450 3800 3450
Wire Wire Line
	4600 3350 4600 3100
Text Label 3800 2550 0    50   ~ 0
CSIDENT5
Text Label 3800 2850 0    50   ~ 0
RESET5
Wire Wire Line
	3500 2750 4600 2750
Wire Wire Line
	3500 3350 4600 3350
Wire Wire Line
	3500 2950 3800 2950
Wire Wire Line
	3500 3050 3800 3050
Wire Wire Line
	3500 3150 3800 3150
Text Label 3800 5100 0    50   ~ 0
CDTI
Text Label 3800 5300 0    50   ~ 0
CDTO
Text Label 3800 4900 0    50   ~ 0
CCLK
Wire Wire Line
	3500 5300 3800 5300
Wire Wire Line
	3500 5400 3800 5400
Wire Wire Line
	3500 5200 3800 5200
Wire Wire Line
	3800 4900 3500 4900
Wire Wire Line
	3500 4500 3800 4500
Wire Wire Line
	3800 4300 3500 4300
Text Label 5100 4400 0    50   ~ 0
MCLK5
Text Label 3800 5000 0    50   ~ 0
GND
Text Label 3800 5400 0    50   ~ 0
3.3V
Wire Wire Line
	3500 4200 3800 4200
Wire Wire Line
	3500 4100 3800 4100
$Comp
L Connector:Conn_01x14_Female J?
U 1 1 606E766F
P 3300 4800
AR Path="/5E13E7DE/606E766F" Ref="J?"  Part="1" 
AR Path="/606E766F" Ref="J?"  Part="1" 
AR Path="/5E13E7DE/5EFA53E6/606E766F" Ref="J?"  Part="1" 
AR Path="/6042B812/5E13E7DE/606E766F" Ref="J?"  Part="1" 
AR Path="/605149D1/606E766F" Ref="J?"  Part="1" 
F 0 "J?" H 3192 3875 50  0000 C CNN
F 1 "Conn_01x14_Female" H 3192 3966 50  0000 C CNN
F 2 "lib:ConnectorMicromatch-14" H 3300 4800 50  0001 C CNN
F 3 "~" H 3300 4800 50  0001 C CNN
F 4 "3784769" H 3300 4800 50  0001 C CNN "Ref Farnell"
	1    3300 4800
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 606E767A
P 4800 4400
AR Path="/5E13E7DE/606E767A" Ref="R?"  Part="1" 
AR Path="/606E767A" Ref="R?"  Part="1" 
AR Path="/5E13E7DE/5EFA53E6/606E767A" Ref="R?"  Part="1" 
AR Path="/6042B812/5E13E7DE/606E767A" Ref="R?"  Part="1" 
AR Path="/605149D1/606E767A" Ref="R?"  Part="1" 
F 0 "R?" V 5007 4400 50  0000 C CNN
F 1 "200R" V 4916 4400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4730 4400 50  0001 C CNN
F 3 "~" H 4800 4400 50  0001 C CNN
F 4 "2447602" H 4800 4400 50  0001 C CNN "Ref Farnell"
	1    4800 4400
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 606E7685
P 4600 4600
AR Path="/5E13E7DE/606E7685" Ref="C?"  Part="1" 
AR Path="/606E7685" Ref="C?"  Part="1" 
AR Path="/5E13E7DE/5EFA53E6/606E7685" Ref="C?"  Part="1" 
AR Path="/6042B812/5E13E7DE/606E7685" Ref="C?"  Part="1" 
AR Path="/605149D1/606E7685" Ref="C?"  Part="1" 
F 0 "C?" H 4715 4646 50  0000 L CNN
F 1 "20n" H 4715 4555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4638 4450 50  0001 C CNN
F 3 "~" H 4600 4600 50  0001 C CNN
F 4 "1414682" H 4600 4600 50  0001 C CNN "Ref Farnell"
	1    4600 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 4400 4600 4400
Wire Wire Line
	4600 4400 4600 4450
Connection ~ 4600 4400
Wire Wire Line
	4950 4400 5100 4400
Text Label 3800 4300 0    50   ~ 0
GND
Text Label 3800 4800 0    50   ~ 0
LRCK6
Text Label 3800 4600 0    50   ~ 0
SCLK6
Text Label 3800 4700 0    50   ~ 0
SDATA6
Text Label 3800 4100 0    50   ~ 0
GPIO6
Wire Wire Line
	3500 5100 3800 5100
Wire Wire Line
	4600 5000 4600 4750
Text Label 3800 4200 0    50   ~ 0
CSIDENT6
Text Label 3800 4500 0    50   ~ 0
RESET6
Wire Wire Line
	3500 4400 4600 4400
Wire Wire Line
	3500 5000 4600 5000
Wire Wire Line
	3500 4600 3800 4600
Wire Wire Line
	3500 4700 3800 4700
Wire Wire Line
	3500 4800 3800 4800
Text Label 3800 800  0    50   ~ 0
GPIO4
Text Label 1400 2450 0    50   ~ 0
GPIO1
Text Label 1400 4100 0    50   ~ 0
GPIO2
Text Label 1400 5750 0    50   ~ 0
GPIO3
Text Label 3800 5200 0    50   ~ 0
CS6
$Sheet
S 7250 2100 2000 1850
U 60772067
F0 "Sheet60772066" 50
F1 "ULX3S_gpio.sch" 50
$EndSheet
$Comp
L power:+3V3 #PWR?
U 1 1 607AF73C
P 7250 1050
AR Path="/605149D1/60772067/607AF73C" Ref="#PWR?"  Part="1" 
AR Path="/605149D1/607AF73C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7250 900 50  0001 C CNN
F 1 "+3V3" H 7250 1190 50  0000 C CNN
F 2 "" H 7250 1050 50  0000 C CNN
F 3 "" H 7250 1050 50  0000 C CNN
	1    7250 1050
	-1   0    0    -1  
$EndComp
Text Label 7400 1050 0    50   ~ 0
3.3V
Wire Wire Line
	7250 1050 7400 1050
$EndSCHEMATC
