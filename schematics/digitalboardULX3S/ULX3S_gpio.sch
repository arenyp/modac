EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J?
U 1 1 607823F8
P 4150 3750
F 0 "J?" H 4200 4750 50  0000 C CNN
F 1 "CONN_02X20" H 4200 2650 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Angled_2x20" H 4150 2800 50  0001 C CNN
F 3 "" H 4150 2800 50  0000 C CNN
F 4 "Leave empty" H 4150 3750 60  0001 C CNN "Note"
	1    4150 3750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4900 4750 4350 4750
Wire Wire Line
	3850 4750 3350 4750
$Comp
L power:GND #PWR?
U 1 1 60782400
P 4750 2950
F 0 "#PWR?" H 4750 2700 50  0001 C CNN
F 1 "GND" H 4750 2800 50  0000 C CNN
F 2 "" H 4750 2950 60  0000 C CNN
F 3 "" H 4750 2950 60  0000 C CNN
	1    4750 2950
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60782406
P 4750 3850
F 0 "#PWR?" H 4750 3600 50  0001 C CNN
F 1 "GND" H 4750 3700 50  0000 C CNN
F 2 "" H 4750 3850 60  0000 C CNN
F 3 "" H 4750 3850 60  0000 C CNN
	1    4750 3850
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6078240C
P 4750 4650
F 0 "#PWR?" H 4750 4400 50  0001 C CNN
F 1 "GND" H 4750 4500 50  0000 C CNN
F 2 "" H 4750 4650 60  0000 C CNN
F 3 "" H 4750 4650 60  0000 C CNN
	1    4750 4650
	0    -1   1    0   
$EndComp
Wire Wire Line
	4750 2950 4350 2950
Wire Wire Line
	4350 2850 4900 2850
Wire Wire Line
	4750 3850 4350 3850
Wire Wire Line
	4900 3750 4350 3750
Wire Wire Line
	4750 4650 4350 4650
$Comp
L power:GND #PWR?
U 1 1 60782417
P 3400 2950
F 0 "#PWR?" H 3400 2700 50  0001 C CNN
F 1 "GND" H 3400 2800 50  0000 C CNN
F 2 "" H 3400 2950 60  0000 C CNN
F 3 "" H 3400 2950 60  0000 C CNN
	1    3400 2950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6078241D
P 3450 3850
F 0 "#PWR?" H 3450 3600 50  0001 C CNN
F 1 "GND" H 3450 3700 50  0000 C CNN
F 2 "" H 3450 3850 60  0000 C CNN
F 3 "" H 3450 3850 60  0000 C CNN
	1    3450 3850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60782423
P 3450 4650
F 0 "#PWR?" H 3450 4400 50  0001 C CNN
F 1 "GND" H 3450 4500 50  0000 C CNN
F 2 "" H 3450 4650 60  0000 C CNN
F 3 "" H 3450 4650 60  0000 C CNN
	1    3450 4650
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 2950 3850 2950
Wire Wire Line
	3850 2850 3250 2850
Wire Wire Line
	3450 3850 3850 3850
Wire Wire Line
	3850 3750 3300 3750
Wire Wire Line
	3850 4650 3450 4650
Text GLabel 4350 3050 2    60   Input ~ 0
GN0
Text GLabel 3850 3050 0    60   Input ~ 0
GP0
Text GLabel 4350 3150 2    60   Input ~ 0
GN1
Text GLabel 3850 3150 0    60   Input ~ 0
GP1
Text GLabel 4350 3250 2    60   Input ~ 0
GN2
Text GLabel 3850 3250 0    60   Input ~ 0
GP2
Text GLabel 3850 3350 0    60   Input ~ 0
GP3
Text GLabel 4350 3350 2    60   Input ~ 0
GN3
Text GLabel 4350 3450 2    60   Input ~ 0
GN4
Text GLabel 3850 3450 0    60   Input ~ 0
GP4
Text GLabel 3850 3550 0    60   Input ~ 0
GP5
Text GLabel 4350 3550 2    60   Input ~ 0
GN5
Text GLabel 4350 3650 2    60   Input ~ 0
GN6
Text GLabel 3850 3650 0    60   Input ~ 0
GP6
Text GLabel 4350 3950 2    60   Input ~ 0
GN7
Text GLabel 3850 3950 0    60   Input ~ 0
GP7
Text GLabel 4350 4050 2    60   Input ~ 0
GN8
Text GLabel 3850 4050 0    60   Input ~ 0
GP8
Text GLabel 4350 4150 2    60   Input ~ 0
GN9
Text GLabel 3850 4150 0    60   Input ~ 0
GP9
Text GLabel 4350 4250 2    60   Input ~ 0
GN10
Text GLabel 3850 4250 0    60   Input ~ 0
GP10
Text GLabel 4350 4350 2    60   Input ~ 0
GN11
Text GLabel 3850 4350 0    60   Input ~ 0
GP11
Text GLabel 4350 4450 2    60   Input ~ 0
GN12
Text GLabel 3850 4450 0    60   Input ~ 0
GP12
Text GLabel 4350 4550 2    60   Input ~ 0
GN13
Text GLabel 3850 4550 0    60   Input ~ 0
GP13
Text Notes 5900 5250 0    60   ~ 0
GP,GN 14-21 differential bidirectional connected to BANK2,3 on "ram" sheet\nGP,GN 22-27 single-ended connected to BANK1 on "gpdi" sheet
Text Notes 4150 3100 2    60   ~ 0
0
Text Notes 4150 3200 2    60   ~ 0
1
Text Notes 4150 3300 2    60   ~ 0
2
Text Notes 4150 3400 2    60   ~ 0
3
Text Notes 4150 4300 2    60   ~ 0
10
Text Notes 4150 3500 2    60   ~ 0
4
Text Notes 4150 3600 2    60   ~ 0
5
Text Notes 4150 3700 2    60   ~ 0
6
Text Notes 4150 4000 2    60   ~ 0
7
Text Notes 4150 4100 2    60   ~ 0
8
Text Notes 4150 4200 2    60   ~ 0
9
Text Notes 4150 4400 2    60   ~ 0
11
Text Notes 4150 4500 2    60   ~ 0
12
Text Notes 4150 4600 2    60   ~ 0
13
Text GLabel 4900 2850 2    60   Input ~ 0
2V5_3V3
Text GLabel 4900 3750 2    60   Input ~ 0
2V5_3V3
Text GLabel 4900 4750 2    60   Input ~ 0
2V5_3V3
Text GLabel 3350 4750 0    60   Input ~ 0
2V5_3V3
Text GLabel 3300 3750 0    60   Input ~ 0
2V5_3V3
Text GLabel 3250 2850 0    60   Input ~ 0
2V5_3V3
Text Notes 3500 4600 2    60   ~ 0
GR_PCLK7_1
Text Notes 3500 4500 2    60   ~ 0
PCLKT7_1
Text Notes 5150 4500 2    60   ~ 0
PCLKC7_1
Text Notes 3550 3100 2    60   ~ 0
PCLKT0_0
Text Notes 5100 3100 2    60   ~ 0
PCLKC0_0
Text Notes 3550 3200 2    60   ~ 0
PCLKT0_1
Text Notes 5100 3200 2    60   ~ 0
PCLKC0_1
Text Notes 3550 3300 2    60   ~ 0
GR_PCLK0_1
Text Notes 5200 3300 2    60   ~ 0
GR_PCLK0_0
Text Notes 5450 4400 2    60   ~ 0
DAC WIFI_GPIO25
Text Notes 3500 4400 2    60   ~ 0
DAC WIFI_GPIO26
Text Notes 2800 5150 0    60   ~ 0
GP,GN 0-7 single-ended connected to BANK0\nGP,GN 8-13 differential bidirectional connected to BANK7
Text Notes 2850 2650 0    60   ~ 0
J1 J2 PIN numbering 1-40 is for FEMALE 90° ANGLED header.\nFor MALE VERTICAL header, SWAP EVEN and ODD pin numbers.
Text Notes 6250 2450 0    60   ~ 0
J1 J2 PIN numbering 1-40 is for FEMALE 90° ANGLED header.\nFor MALE VERTICAL header, SWAP EVEN and ODD pin numbers.
Text Notes 8200 4300 0    60   ~ 0
GR_PCLK3_0
Text Notes 8350 2600 0    60   ~ 0
STPS2L40AF
Wire Notes Line
	8600 2700 8500 2700
Wire Notes Line
	8600 2750 8600 2650
Wire Notes Line
	8600 2700 8650 2750
Wire Notes Line
	8650 2650 8600 2700
Wire Notes Line
	8650 2650 8650 2750
Wire Notes Line
	8750 2700 8650 2700
Text Notes 6450 2600 0    60   ~ 0
STPS2L40AF
Wire Notes Line
	6650 2700 6550 2700
Wire Notes Line
	6650 2750 6650 2650
Wire Notes Line
	6650 2700 6700 2750
Wire Notes Line
	6700 2650 6650 2700
Wire Notes Line
	6700 2650 6700 2750
Wire Notes Line
	6800 2700 6700 2700
Text Notes 7550 3100 0    60   ~ 0
27
Text Notes 7550 3200 0    60   ~ 0
26
Text Notes 7550 3300 0    60   ~ 0
25
Text Notes 7550 3400 0    60   ~ 0
24
Text Notes 7550 3500 0    60   ~ 0
23
Text Notes 7550 3600 0    60   ~ 0
22
Text Notes 7550 3700 0    60   ~ 0
21
Text Notes 7550 4000 0    60   ~ 0
20
Text Notes 7550 4100 0    60   ~ 0
19
Text Notes 7550 4200 0    60   ~ 0
18
Text Notes 7550 4300 0    60   ~ 0
17
Text Notes 7550 4400 0    60   ~ 0
16
Text Notes 7550 4500 0    60   ~ 0
15
Text Notes 7550 4600 0    60   ~ 0
14
Text GLabel 7850 3050 2    60   Input ~ 0
GP27
Text GLabel 7850 3150 2    60   Input ~ 0
GP26
Text GLabel 7850 3250 2    60   Input ~ 0
GP25
Text GLabel 7850 3350 2    60   Input ~ 0
GP24
Text GLabel 7850 3450 2    60   Input ~ 0
GP23
Text GLabel 7850 3550 2    60   Input ~ 0
GP22
Text GLabel 7850 3650 2    60   Input ~ 0
GP21
Text GLabel 7350 3050 0    60   Input ~ 0
GN27
Text GLabel 7350 3150 0    60   Input ~ 0
GN26
Text GLabel 7350 3250 0    60   Input ~ 0
GN25
Text GLabel 7350 3350 0    60   Input ~ 0
GN24
Text GLabel 7350 3450 0    60   Input ~ 0
GN23
Text GLabel 7350 3550 0    60   Input ~ 0
GN22
Text GLabel 7350 3650 0    60   Input ~ 0
GN21
Text GLabel 7350 3950 0    60   Input ~ 0
GN20
Text GLabel 7350 4050 0    60   Input ~ 0
GN19
Text GLabel 7350 4150 0    60   Input ~ 0
GN18
Text GLabel 7350 4250 0    60   Input ~ 0
GN17
Text GLabel 7350 4350 0    60   Input ~ 0
GN16
Text GLabel 7350 4450 0    60   Input ~ 0
GN15
Text GLabel 7350 4550 0    60   Input ~ 0
GN14
Text GLabel 7850 3950 2    60   Input ~ 0
GP20
Text GLabel 7850 4050 2    60   Input ~ 0
GP19
Text GLabel 7850 4150 2    60   Input ~ 0
GP18
Text GLabel 7850 4250 2    60   Input ~ 0
GP17
Text GLabel 7850 4350 2    60   Input ~ 0
GP16
Text GLabel 7850 4450 2    60   Input ~ 0
GP15
Text GLabel 7850 4550 2    60   Input ~ 0
GP14
$Comp
L power:+5V #PWR?
U 1 1 607824A6
P 8700 2850
F 0 "#PWR?" H 8700 2700 50  0001 C CNN
F 1 "+5V" H 8700 2990 50  0000 C CNN
F 2 "" H 8700 2850 50  0000 C CNN
F 3 "" H 8700 2850 50  0000 C CNN
	1    8700 2850
	0    1    -1   0   
$EndComp
Wire Wire Line
	6950 2950 7350 2950
Wire Wire Line
	7850 2950 8250 2950
Wire Wire Line
	6800 3850 7350 3850
Wire Wire Line
	7850 3850 8400 3850
Wire Wire Line
	8250 3750 7850 3750
Wire Wire Line
	6950 3750 7350 3750
Wire Wire Line
	7850 4750 8350 4750
Wire Wire Line
	6750 2850 7350 2850
Wire Wire Line
	7850 2850 8500 2850
Wire Wire Line
	7350 4750 6850 4750
Wire Wire Line
	8200 4650 7850 4650
Wire Wire Line
	7000 4650 7350 4650
$Comp
L power:GND #PWR?
U 1 1 607824B8
P 8250 2950
F 0 "#PWR?" H 8250 2700 50  0001 C CNN
F 1 "GND" H 8250 2800 50  0000 C CNN
F 2 "" H 8250 2950 60  0000 C CNN
F 3 "" H 8250 2950 60  0000 C CNN
	1    8250 2950
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 607824BE
P 6950 2950
F 0 "#PWR?" H 6950 2700 50  0001 C CNN
F 1 "GND" H 6950 2800 50  0000 C CNN
F 2 "" H 6950 2950 60  0000 C CNN
F 3 "" H 6950 2950 60  0000 C CNN
	1    6950 2950
	0    1    -1   0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 607824C4
P 8400 3850
F 0 "#PWR?" H 8400 3700 50  0001 C CNN
F 1 "+3V3" H 8400 3990 50  0000 C CNN
F 2 "" H 8400 3850 50  0000 C CNN
F 3 "" H 8400 3850 50  0000 C CNN
	1    8400 3850
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 607824CA
P 8250 3750
F 0 "#PWR?" H 8250 3500 50  0001 C CNN
F 1 "GND" H 8250 3600 50  0000 C CNN
F 2 "" H 8250 3750 60  0000 C CNN
F 3 "" H 8250 3750 60  0000 C CNN
	1    8250 3750
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 607824D0
P 6800 3850
F 0 "#PWR?" H 6800 3700 50  0001 C CNN
F 1 "+3V3" H 6800 3990 50  0000 C CNN
F 2 "" H 6800 3850 50  0000 C CNN
F 3 "" H 6800 3850 50  0000 C CNN
	1    6800 3850
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 607824D6
P 6950 3750
F 0 "#PWR?" H 6950 3500 50  0001 C CNN
F 1 "GND" H 6950 3600 50  0000 C CNN
F 2 "" H 6950 3750 60  0000 C CNN
F 3 "" H 6950 3750 60  0000 C CNN
	1    6950 3750
	0    1    -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J?
U 1 1 607824DD
P 7550 3850
F 0 "J?" H 7600 4850 50  0000 C CNN
F 1 "CONN_02X20" H 7600 2750 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Angled_2x20" H 7550 2900 50  0001 C CNN
F 3 "" H 7550 2900 50  0000 C CNN
F 4 "Leave empty" H 7550 3850 60  0001 C CNN "Note"
	1    7550 3850
	1    0    0    1   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 607824E3
P 6850 4750
F 0 "#PWR?" H 6850 4600 50  0001 C CNN
F 1 "+3V3" H 6850 4890 50  0000 C CNN
F 2 "" H 6850 4750 50  0000 C CNN
F 3 "" H 6850 4750 50  0000 C CNN
	1    6850 4750
	0    -1   1    0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 607824E9
P 8350 4750
F 0 "#PWR?" H 8350 4600 50  0001 C CNN
F 1 "+3V3" H 8350 4890 50  0000 C CNN
F 2 "" H 8350 4750 50  0000 C CNN
F 3 "" H 8350 4750 50  0000 C CNN
	1    8350 4750
	0    1    -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 607824EF
P 6550 2850
F 0 "#PWR?" H 6550 2700 50  0001 C CNN
F 1 "+5V" H 6550 2990 50  0000 C CNN
F 2 "" H 6550 2850 60  0000 C CNN
F 3 "" H 6550 2850 60  0000 C CNN
	1    6550 2850
	0    -1   1    0   
$EndComp
Text Label 7900 2850 0    60   ~ 0
OUT5V
$Comp
L Device:Jumper_NC_Small D?
U 1 1 607824F7
P 8600 2850
F 0 "D?" H 8600 2950 50  0000 C CNN
F 1 "0" H 8600 2800 50  0000 C CNN
F 2 "jumper:D_SMA_Jumper_NC" V 8600 2850 60  0001 C CNN
F 3 "" V 8600 2850 60  0000 C CNN
F 4 "Leave empty" H 8600 2850 50  0001 C CNN "Note"
	1    8600 2850
	1    0    0    1   
$EndComp
Text Label 7100 2850 0    60   ~ 0
IN5V
$Comp
L Device:Jumper_NC_Small D?
U 1 1 607824FF
P 6650 2850
F 0 "D?" H 6650 2950 50  0000 C CNN
F 1 "0" H 6650 2800 50  0000 C CNN
F 2 "jumper:D_SMA_Jumper_NC" V 6650 2850 60  0001 C CNN
F 3 "" V 6650 2850 60  0000 C CNN
F 4 "Leave empty" H 6650 2850 50  0001 C CNN "Note"
	1    6650 2850
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60782505
P 8200 4650
F 0 "#PWR?" H 8200 4400 50  0001 C CNN
F 1 "GND" H 8200 4500 50  0000 C CNN
F 2 "" H 8200 4650 60  0000 C CNN
F 3 "" H 8200 4650 60  0000 C CNN
	1    8200 4650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6078250B
P 7000 4650
F 0 "#PWR?" H 7000 4400 50  0001 C CNN
F 1 "GND" H 7000 4500 50  0000 C CNN
F 2 "" H 7000 4650 60  0000 C CNN
F 3 "" H 7000 4650 60  0000 C CNN
	1    7000 4650
	0    1    -1   0   
$EndComp
Text GLabel 9550 2850 2    50   Input ~ 0
5V
$EndSCHEMATC
