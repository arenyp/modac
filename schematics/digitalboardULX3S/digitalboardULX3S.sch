EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 6045A87F
P 1800 2950
AR Path="/6042B812/6045A87F" Ref="J?"  Part="1" 
AR Path="/6045A87F" Ref="J?"  Part="1" 
F 0 "J?" H 1827 2976 50  0000 L CNN
F 1 "Conn_01x01_Female" H 1827 2885 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 1800 2950 50  0001 C CNN
F 3 "~" H 1800 2950 50  0001 C CNN
	1    1800 2950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 6045A885
P 1800 3150
AR Path="/6042B812/6045A885" Ref="J?"  Part="1" 
AR Path="/6045A885" Ref="J?"  Part="1" 
F 0 "J?" H 1827 3176 50  0000 L CNN
F 1 "Modac Digital Board" H 1827 3085 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 1800 3150 50  0001 C CNN
F 3 "~" H 1800 3150 50  0001 C CNN
	1    1800 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 6045A88B
P 1800 2550
AR Path="/6042B812/6045A88B" Ref="J?"  Part="1" 
AR Path="/6045A88B" Ref="J?"  Part="1" 
F 0 "J?" H 1827 2576 50  0000 L CNN
F 1 "Conn_01x01_Female" H 1827 2485 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 1800 2550 50  0001 C CNN
F 3 "~" H 1800 2550 50  0001 C CNN
	1    1800 2550
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 6045A891
P 1800 2750
AR Path="/6042B812/6045A891" Ref="J?"  Part="1" 
AR Path="/6045A891" Ref="J?"  Part="1" 
F 0 "J?" H 1827 2776 50  0000 L CNN
F 1 "Conn_01x01_Female" H 1827 2685 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 1800 2750 50  0001 C CNN
F 3 "~" H 1800 2750 50  0001 C CNN
	1    1800 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 2550 1450 2550
Wire Wire Line
	1450 2550 1450 2750
Wire Wire Line
	1450 3150 1600 3150
Wire Wire Line
	1450 3150 1200 3150
Connection ~ 1450 3150
Wire Wire Line
	1600 2950 1450 2950
Connection ~ 1450 2950
Wire Wire Line
	1450 2950 1450 3150
Wire Wire Line
	1600 2750 1450 2750
Connection ~ 1450 2750
Wire Wire Line
	1450 2750 1450 2950
Text Label 1200 3150 0    50   ~ 0
GND
$Comp
L Device:R R?
U 1 1 6045A8A3
P 1700 6050
AR Path="/6042B812/6045A8A3" Ref="R?"  Part="1" 
AR Path="/6045A8A3" Ref="R?"  Part="1" 
F 0 "R?" H 1770 6096 50  0000 L CNN
F 1 "4.7K" H 1770 6005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1630 6050 50  0001 C CNN
F 3 "~" H 1700 6050 50  0001 C CNN
	1    1700 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 5900 1700 5800
Wire Wire Line
	1700 6200 1700 6300
Wire Wire Line
	1700 6300 1850 6300
Connection ~ 1700 6300
Text Label 1700 5800 0    50   ~ 0
3.3V
Wire Wire Line
	1800 7200 1850 7200
Text Label 1850 7200 0    50   ~ 0
GND
$Comp
L Device:R R?
U 1 1 6045A8B0
P 1150 6050
AR Path="/6042B812/6045A8B0" Ref="R?"  Part="1" 
AR Path="/6045A8B0" Ref="R?"  Part="1" 
F 0 "R?" H 1220 6096 50  0000 L CNN
F 1 "4.7K" H 1220 6005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1080 6050 50  0001 C CNN
F 3 "~" H 1150 6050 50  0001 C CNN
	1    1150 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 5900 1150 5800
Wire Wire Line
	1150 6200 1150 6300
Wire Wire Line
	1150 6300 1300 6300
Connection ~ 1150 6300
Wire Wire Line
	1150 6300 1150 6400
Text Label 1150 5800 0    50   ~ 0
3.3V
Text Label 1300 6300 0    50   ~ 0
SEL0
Text Label 1850 6300 0    50   ~ 0
SEL1
$Comp
L Device:R R?
U 1 1 6045A8BE
P 2250 6050
AR Path="/6042B812/6045A8BE" Ref="R?"  Part="1" 
AR Path="/6045A8BE" Ref="R?"  Part="1" 
F 0 "R?" H 2320 6096 50  0000 L CNN
F 1 "4.7K" H 2320 6005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2180 6050 50  0001 C CNN
F 3 "~" H 2250 6050 50  0001 C CNN
	1    2250 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 5900 2250 5800
Wire Wire Line
	2250 6200 2250 6300
Wire Wire Line
	2250 6300 2400 6300
Connection ~ 2250 6300
Wire Wire Line
	2250 6300 2250 6400
Text Label 2250 5800 0    50   ~ 0
3.3V
Text Label 2400 6300 0    50   ~ 0
SEL2
Wire Wire Line
	2250 6400 1800 6400
Wire Wire Line
	1800 6400 1800 6500
Wire Wire Line
	1600 6500 1600 6400
Wire Wire Line
	1600 6400 1150 6400
Wire Wire Line
	1700 6300 1700 6500
Wire Wire Line
	1800 7000 1800 7100
Wire Wire Line
	1700 7000 1700 7100
Wire Wire Line
	1700 7100 1800 7100
Connection ~ 1800 7100
Wire Wire Line
	1800 7100 1800 7200
Wire Wire Line
	1700 7100 1600 7100
Wire Wire Line
	1600 7100 1600 7000
Connection ~ 1700 7100
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J?
U 1 1 6045A8D8
P 1700 6700
AR Path="/6042B812/6045A8D8" Ref="J?"  Part="1" 
AR Path="/6045A8D8" Ref="J?"  Part="1" 
F 0 "J?" V 1704 6880 50  0000 L CNN
F 1 "SEL 2 1 0" V 1795 6880 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 1700 6700 50  0001 C CNN
F 3 "~" H 1700 6700 50  0001 C CNN
	1    1700 6700
	0    1    1    0   
$EndComp
Wire Wire Line
	1850 1400 2000 1400
Text Label 2000 1250 0    50   ~ 0
5V
Text Label 2000 1400 0    50   ~ 0
GND
Text Label 2850 4900 0    50   ~ 0
GND
Text Label 3200 4450 0    50   ~ 0
VOLUME
$Comp
L Device:C C?
U 1 1 6045A8E9
P 2800 4650
AR Path="/5E13E7DE/6045A8E9" Ref="C?"  Part="1" 
AR Path="/6045A8E9" Ref="C?"  Part="1" 
AR Path="/6042B812/6045A8E9" Ref="C?"  Part="1" 
F 0 "C?" H 2915 4696 50  0000 L CNN
F 1 "0.1uF" H 2915 4605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2838 4500 50  0001 C CNN
F 3 "~" H 2800 4650 50  0001 C CNN
	1    2800 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 4450 2800 4500
Connection ~ 2800 4450
Wire Wire Line
	2800 4800 2800 4900
Wire Wire Line
	2800 4900 2850 4900
$Comp
L Device:R R?
U 1 1 6045A8F3
P 2800 4150
AR Path="/5E13E7DE/6045A8F3" Ref="R?"  Part="1" 
AR Path="/6045A8F3" Ref="R?"  Part="1" 
AR Path="/6042B812/6045A8F3" Ref="R?"  Part="1" 
F 0 "R?" H 2870 4196 50  0000 L CNN
F 1 "1K" H 2870 4105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2730 4150 50  0001 C CNN
F 3 "~" H 2800 4150 50  0001 C CNN
	1    2800 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 3950 2800 4000
Wire Wire Line
	2800 4300 2800 4450
Wire Wire Line
	2800 4450 3200 4450
Text Label 1950 4350 0    50   ~ 0
GND
Wire Wire Line
	1750 4350 1950 4350
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J?
U 1 1 6045A8FE
P 1450 4350
AR Path="/6042B812/6045A8FE" Ref="J?"  Part="1" 
AR Path="/6045A8FE" Ref="J?"  Part="1" 
F 0 "J?" H 1500 4567 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 1500 4476 50  0000 C CNN
F 2 "lib:ConnectorMicromatch-4" H 1450 4350 50  0001 C CNN
F 3 "~" H 1450 4350 50  0001 C CNN
	1    1450 4350
	1    0    0    -1  
$EndComp
Text Label 950  4450 0    50   ~ 0
GND
Wire Wire Line
	950  4450 1250 4450
Wire Wire Line
	1750 4450 2050 4450
Wire Wire Line
	2050 4450 2050 4550
Wire Wire Line
	2050 4550 800  4550
Wire Wire Line
	800  4550 800  4350
Wire Wire Line
	800  4350 1250 4350
Connection ~ 2050 4450
Wire Wire Line
	2050 4450 2800 4450
Text Notes 1000 4000 0    50   ~ 0
Re use the analog volume module
$Sheet
S 5000 1350 4650 4150
U 605149D1
F0 "Sheet605149D0" 50
F1 "IOs.sch" 50
$EndSheet
$Sheet
S 1350 1050 500  450 
U 6045A8DC
F0 "sheet6045A879" 50
F1 "./Power.sch" 50
F2 "GND" I R 1850 1400 50 
F3 "5V" I R 1850 1250 50 
$EndSheet
Text Label 2800 3950 0    50   ~ 0
3V3
$Comp
L power:+3V3 #PWR?
U 1 1 607D3253
P 800 3700
AR Path="/605149D1/60772067/607D3253" Ref="#PWR?"  Part="1" 
AR Path="/607D3253" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 800 3550 50  0001 C CNN
F 1 "+3V3" H 800 3840 50  0000 C CNN
F 2 "" H 800 3700 50  0000 C CNN
F 3 "" H 800 3700 50  0000 C CNN
	1    800  3700
	-1   0    0    -1  
$EndComp
Text Label 950  3700 0    50   ~ 0
3.3V
Wire Wire Line
	800  3700 950  3700
Text GLabel 2250 1250 2    50   Input ~ 0
5V
Wire Wire Line
	1850 1250 2250 1250
$EndSCHEMATC
