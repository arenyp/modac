PCBNEW-LibModule-V1  2020-01-05 15:04:01
# encoding utf-8
Units mm
$INDEX
TQFP50P1600X1600X160-100N
$EndINDEX
$MODULE TQFP50P1600X1600X160-100N
Po 0 0 0 15 5e11fae1 00000000 ~~
Li TQFP50P1600X1600X160-100N
Cd TQFP50P1600X1600X160-100N
Kw Integrated Circuit
Sc 0
At SMD
AR 
Op 0 0 0
T0 0.17167 -9.37467 1.27 1.27 0 0.254 N V 21 N "IC**"
T1 0.17167 -9.37467 1.27 1.27 0 0.254 N I 21 N "TQFP50P1600X1600X160-100N"
DS -7.025 -6.525 -6.525 -7.025 0.15 24
DS -6.525 -7.025 7.025 -7.025 0.15 24
DS 7.025 -7.025 7.025 7.025 0.15 24
DS 7.025 7.025 -7.025 7.025 0.15 24
DS -7.025 7.025 -7.025 -6.525 0.15 24
DS -7.025 -6.525 -7.025 -7.025 0.2 21
DS -7.025 -7.025 -6.525 -7.025 0.2 21
DS 7.025 -7.025 7.025 -6.525 0.2 21
DS 6.525 -7.025 7.025 -7.025 0.2 21
DS 7.025 7.025 6.525 7.025 0.2 21
DS 7.025 6.525 7.025 7.025 0.2 21
DS -7.025 7.025 -7.025 6.525 0.2 21
DS -6.525 7.025 -7.025 7.025 0.2 21
DS -8.8 -8.8 8.8 -8.8 0.05 24
DS 8.8 -8.8 8.8 8.8 0.05 24
DS 8.8 8.8 -8.8 8.8 0.05 24
DS -8.8 8.8 -8.8 -8.8 0.05 24
$PAD
Po -7.75 -6
Sh "1" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 -5.5
Sh "2" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 -5
Sh "3" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 -4.5
Sh "4" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 -4
Sh "5" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 -3.5
Sh "6" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 -3
Sh "7" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 -2.5
Sh "8" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 -2
Sh "9" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 -1.5
Sh "10" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 -1
Sh "11" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 -0.5
Sh "12" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 0
Sh "13" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 0.5
Sh "14" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 1
Sh "15" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 1.5
Sh "16" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 2
Sh "17" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 2.5
Sh "18" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 3
Sh "19" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 3.5
Sh "20" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 4
Sh "21" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 4.5
Sh "22" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 5
Sh "23" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 5.5
Sh "24" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.75 6
Sh "25" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -6 7.75
Sh "26" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -5.5 7.75
Sh "27" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -5 7.75
Sh "28" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -4.5 7.75
Sh "29" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -4 7.75
Sh "30" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -3.5 7.75
Sh "31" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -3 7.75
Sh "32" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -2.5 7.75
Sh "33" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -2 7.75
Sh "34" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1.5 7.75
Sh "35" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1 7.75
Sh "36" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -0.5 7.75
Sh "37" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0 7.75
Sh "38" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.5 7.75
Sh "39" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1 7.75
Sh "40" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.5 7.75
Sh "41" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 2 7.75
Sh "42" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 2.5 7.75
Sh "43" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 3 7.75
Sh "44" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 3.5 7.75
Sh "45" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 4 7.75
Sh "46" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 4.5 7.75
Sh "47" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 5 7.75
Sh "48" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 5.5 7.75
Sh "49" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 6 7.75
Sh "50" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 6
Sh "51" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 5.5
Sh "52" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 5
Sh "53" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 4.5
Sh "54" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 4
Sh "55" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 3.5
Sh "56" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 3
Sh "57" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 2.5
Sh "58" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 2
Sh "59" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 1.5
Sh "60" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 1
Sh "61" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 0.5
Sh "62" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 0
Sh "63" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 -0.5
Sh "64" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 -1
Sh "65" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 -1.5
Sh "66" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 -2
Sh "67" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 -2.5
Sh "68" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 -3
Sh "69" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 -3.5
Sh "70" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 -4
Sh "71" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 -4.5
Sh "72" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 -5
Sh "73" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 -5.5
Sh "74" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.75 -6
Sh "75" R 0.3 1.6 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 6 -7.75
Sh "76" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 5.5 -7.75
Sh "77" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 5 -7.75
Sh "78" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 4.5 -7.75
Sh "79" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 4 -7.75
Sh "80" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 3.5 -7.75
Sh "81" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 3 -7.75
Sh "82" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 2.5 -7.75
Sh "83" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 2 -7.75
Sh "84" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.5 -7.75
Sh "85" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1 -7.75
Sh "86" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.5 -7.75
Sh "87" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0 -7.75
Sh "88" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -0.5 -7.75
Sh "89" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1 -7.75
Sh "90" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1.5 -7.75
Sh "91" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -2 -7.75
Sh "92" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -2.5 -7.75
Sh "93" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -3 -7.75
Sh "94" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -3.5 -7.75
Sh "95" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -4 -7.75
Sh "96" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -4.5 -7.75
Sh "97" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -5 -7.75
Sh "98" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -5.5 -7.75
Sh "99" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -6 -7.75
Sh "100" R 0.3 1.6 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.7595 -7.7595
Sh "101" C 1 1 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.7595 7.7595
Sh "102" C 1 1 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE TQFP50P1600X1600X160-100N
$EndLIBRARY
