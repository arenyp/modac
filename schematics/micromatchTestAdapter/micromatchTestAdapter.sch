EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x20_Female J2
U 1 1 5F321A1C
P 4500 3750
F 0 "J2" H 4528 3726 50  0000 L CNN
F 1 "Conn_01x20_Female" H 4528 3635 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x10_P2.54mm_Vertical" H 4500 3750 50  0001 C CNN
F 3 "~" H 4500 3750 50  0001 C CNN
	1    4500 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x20_Female J1
U 1 1 5F3229AB
P 3650 3750
F 0 "J1" H 3542 4835 50  0000 C CNN
F 1 "Conn_01x20_Female" H 3542 4744 50  0000 C CNN
F 2 "lib:ConnectorMicromatch-20" H 3650 3750 50  0001 C CNN
F 3 "~" H 3650 3750 50  0001 C CNN
	1    3650 3750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3850 2850 4300 2850
Wire Wire Line
	4300 2950 3850 2950
Wire Wire Line
	3850 3050 4300 3050
Wire Wire Line
	4300 3150 3850 3150
Wire Wire Line
	3850 3250 4300 3250
Wire Wire Line
	3850 3350 4300 3350
Wire Wire Line
	3850 3450 4300 3450
Wire Wire Line
	4300 3550 3850 3550
Wire Wire Line
	3850 3650 4300 3650
Wire Wire Line
	4300 3750 3850 3750
Wire Wire Line
	3850 3850 4300 3850
Wire Wire Line
	3850 3950 4300 3950
Wire Wire Line
	4300 4050 3850 4050
Wire Wire Line
	3850 4150 4300 4150
Wire Wire Line
	4300 4250 3850 4250
Wire Wire Line
	3850 4350 4300 4350
Wire Wire Line
	3850 4450 4300 4450
Wire Wire Line
	3850 4550 4300 4550
Wire Wire Line
	3850 4650 4300 4650
Wire Wire Line
	3850 4750 4300 4750
Wire Wire Line
	7700 1650 7450 1650
Wire Wire Line
	7450 1650 7450 1850
Wire Wire Line
	7450 2250 7700 2250
Wire Wire Line
	7700 2050 7450 2050
Connection ~ 7450 2050
Wire Wire Line
	7450 2050 7450 2250
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5F33B493
P 7800 1650
F 0 "H1" V 7754 1800 50  0000 L CNN
F 1 "MountingHole_Pad" V 7845 1800 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 7800 1650 50  0001 C CNN
F 3 "~" H 7800 1650 50  0001 C CNN
	1    7800 1650
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5F33EA90
P 7800 2050
F 0 "H3" V 7754 2200 50  0000 L CNN
F 1 "MountingHole_Pad" V 7845 2200 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 7800 2050 50  0001 C CNN
F 3 "~" H 7800 2050 50  0001 C CNN
	1    7800 2050
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5F33EDE1
P 7800 2250
F 0 "H4" V 7754 2400 50  0000 L CNN
F 1 "MountingHole_Pad" V 7845 2400 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 7800 2250 50  0001 C CNN
F 3 "~" H 7800 2250 50  0001 C CNN
	1    7800 2250
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5F33F04C
P 7800 1850
F 0 "H2" V 7754 2000 50  0000 L CNN
F 1 "MountingHole_Pad" V 7845 2000 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 7800 1850 50  0001 C CNN
F 3 "~" H 7800 1850 50  0001 C CNN
	1    7800 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	7700 1850 7450 1850
Connection ~ 7450 1850
Wire Wire Line
	7450 1850 7450 2050
Text Label 7500 1650 0    50   ~ 0
GND
Text Label 4000 2850 0    50   ~ 0
1
Text Label 4000 2950 0    50   ~ 0
2
Text Label 4000 3050 0    50   ~ 0
3
Text Label 4000 3150 0    50   ~ 0
4
Text Label 4000 3250 0    50   ~ 0
5
Text Label 4000 3350 0    50   ~ 0
6
Text Label 4000 3450 0    50   ~ 0
7
Text Label 4000 3550 0    50   ~ 0
8
Text Label 4000 3650 0    50   ~ 0
9
Text Label 3950 3750 0    50   ~ 0
10
Text Label 3950 3850 0    50   ~ 0
11
Text Label 3950 3950 0    50   ~ 0
12
Text Label 3950 4050 0    50   ~ 0
13
Text Label 3950 4150 0    50   ~ 0
14
Text Label 3950 4250 0    50   ~ 0
15
Text Label 3950 4350 0    50   ~ 0
16
Text Label 3950 4450 0    50   ~ 0
17
Text Label 3950 4550 0    50   ~ 0
18
Text Label 3950 4650 0    50   ~ 0
19
Text Label 3950 4750 0    50   ~ 0
20
$Comp
L Connector:Conn_01x20_Female J3
U 1 1 5FB222D6
P 6200 3750
F 0 "J3" H 6228 3726 50  0000 L CNN
F 1 "Conn_01x20_Female" H 6228 3635 50  0000 L CNN
F 2 "lib:ConnectorMicromatch-20" H 6200 3750 50  0001 C CNN
F 3 "~" H 6200 3750 50  0001 C CNN
	1    6200 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 2850 6000 2850
Wire Wire Line
	6000 2950 5550 2950
Wire Wire Line
	5550 3050 6000 3050
Wire Wire Line
	6000 3150 5550 3150
Wire Wire Line
	5550 3250 6000 3250
Wire Wire Line
	5550 3350 6000 3350
Wire Wire Line
	5550 3450 6000 3450
Wire Wire Line
	6000 3550 5550 3550
Wire Wire Line
	5550 3650 6000 3650
Wire Wire Line
	6000 3750 5550 3750
Wire Wire Line
	5550 3850 6000 3850
Wire Wire Line
	5550 3950 6000 3950
Wire Wire Line
	6000 4050 5550 4050
Wire Wire Line
	5550 4150 6000 4150
Wire Wire Line
	6000 4250 5550 4250
Wire Wire Line
	5550 4350 6000 4350
Wire Wire Line
	5550 4450 6000 4450
Wire Wire Line
	5550 4550 6000 4550
Wire Wire Line
	5550 4650 6000 4650
Wire Wire Line
	5550 4750 6000 4750
Text Label 5550 2850 0    50   ~ 0
1
Text Label 5550 2950 0    50   ~ 0
2
Text Label 5550 3050 0    50   ~ 0
3
Text Label 5550 3150 0    50   ~ 0
4
Text Label 5550 3250 0    50   ~ 0
5
Text Label 5550 3350 0    50   ~ 0
6
Text Label 5550 3450 0    50   ~ 0
7
Text Label 5550 3550 0    50   ~ 0
8
Text Label 5550 3650 0    50   ~ 0
9
Text Label 5550 3750 0    50   ~ 0
10
Text Label 5550 3850 0    50   ~ 0
11
Text Label 5550 3950 0    50   ~ 0
12
Text Label 5550 4050 0    50   ~ 0
13
Text Label 5550 4150 0    50   ~ 0
14
Text Label 5550 4250 0    50   ~ 0
15
Text Label 5550 4350 0    50   ~ 0
16
Text Label 5550 4450 0    50   ~ 0
17
Text Label 5550 4550 0    50   ~ 0
18
Text Label 5550 4650 0    50   ~ 0
19
Text Label 5550 4750 0    50   ~ 0
20
$EndSCHEMATC
