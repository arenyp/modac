EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_Coaxial_x2 J1
U 1 1 5DAC5926
P 5100 3100
F 0 "J1" H 5199 3076 50  0000 L CNN
F 1 "Conn_Coaxial_x2" H 5199 2985 50  0000 L CNN
F 2 "lib:CliffDoubleRCA" H 5100 3000 50  0001 C CNN
F 3 " ~" H 5100 3000 50  0001 C CNN
F 4 "2817593" H 5100 3100 50  0001 C CNN "Ref Farnell"
	1    5100 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial_x2 J3
U 1 1 5DAC59C5
P 6500 3100
F 0 "J3" H 6599 3076 50  0000 L CNN
F 1 "Conn_Coaxial_x2" H 6599 2985 50  0000 L CNN
F 2 "lib:CliffDoubleRCA" H 6500 3000 50  0001 C CNN
F 3 " ~" H 6500 3000 50  0001 C CNN
F 4 "2817593" H 6500 3100 50  0001 C CNN "Ref Farnell"
	1    6500 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 3400 6500 3500
Wire Wire Line
	6500 3500 5100 3500
Wire Wire Line
	5100 3400 5100 3500
Connection ~ 5100 3500
Wire Wire Line
	5100 3500 4750 3500
Text Label 4750 3500 0    50   ~ 0
GND
Wire Wire Line
	6300 3000 6000 3000
Wire Wire Line
	6000 3200 6300 3200
Wire Wire Line
	4600 3000 4900 3000
Wire Wire Line
	4600 3200 4900 3200
Text Label 4600 3200 0    50   ~ 0
L1
Text Label 6000 3200 0    50   ~ 0
L2
Text Label 4600 3000 0    50   ~ 0
R1
Text Label 6000 3000 0    50   ~ 0
R2
Text Label 6000 3950 0    50   ~ 0
L1
Text Label 6000 4250 0    50   ~ 0
R1
Text Label 6000 4500 0    50   ~ 0
L2
Text Label 6000 4800 0    50   ~ 0
R2
Text Label 6000 4100 0    50   ~ 0
GND
Text Label 6000 4650 0    50   ~ 0
GND
Wire Wire Line
	6350 4650 6000 4650
Wire Wire Line
	6350 4100 6000 4100
$Comp
L Connector:Conn_01x04_Female J2
U 1 1 5E361712
P 6700 4050
F 0 "J2" H 6727 4026 50  0000 L CNN
F 1 "Conn_01x04_Female" H 6727 3935 50  0000 L CNN
F 2 "lib:ConnectorMicromatch-4" H 6700 4050 50  0001 C CNN
F 3 "~" H 6700 4050 50  0001 C CNN
F 4 "1291785 " H 6700 4050 50  0001 C CNN "Ref Farnell"
	1    6700 4050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J4
U 1 1 5E3619E6
P 6700 4600
F 0 "J4" H 6727 4576 50  0000 L CNN
F 1 "Conn_01x04_Female" H 6727 4485 50  0000 L CNN
F 2 "lib:ConnectorMicromatch-4" H 6700 4600 50  0001 C CNN
F 3 "~" H 6700 4600 50  0001 C CNN
F 4 "1291785" H 6700 4600 50  0001 C CNN "Ref Farnell"
	1    6700 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3950 6500 3950
Wire Wire Line
	6350 4100 6350 4050
Wire Wire Line
	6350 4050 6500 4050
Wire Wire Line
	6500 4150 6350 4150
Wire Wire Line
	6350 4150 6350 4100
Connection ~ 6350 4100
Wire Wire Line
	6000 4250 6500 4250
Wire Wire Line
	6000 4500 6500 4500
Wire Wire Line
	6000 4800 6500 4800
Wire Wire Line
	6350 4700 6350 4650
Wire Wire Line
	6350 4700 6500 4700
Wire Wire Line
	6500 4600 6350 4600
Wire Wire Line
	6350 4600 6350 4650
Connection ~ 6350 4650
$EndSCHEMATC
