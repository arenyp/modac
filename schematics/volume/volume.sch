EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 4WayVolumeKnob:4WKnob U1
U 1 1 5E57BACC
P 5400 3900
F 0 "U1" H 5400 4975 50  0000 C CNN
F 1 "4WKnob" H 5400 4884 50  0000 C CNN
F 2 "lib:POT4WayBourns" H 5300 3900 50  0001 C CNN
F 3 "" H 5300 3900 50  0001 C CNN
	1    5400 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3050 5950 3050
Wire Wire Line
	5800 3150 5950 3150
Wire Wire Line
	5950 3150 5950 3050
Connection ~ 5950 3050
Wire Wire Line
	5950 3050 6200 3050
Wire Wire Line
	5800 3250 5950 3250
Wire Wire Line
	5950 3250 5950 3150
Connection ~ 5950 3150
Wire Wire Line
	5800 3350 5950 3350
Wire Wire Line
	5950 3350 5950 3250
Connection ~ 5950 3250
Text Label 6200 3050 0    50   ~ 0
GND
$Comp
L Connector:Conn_01x04_Female J1
U 1 1 5E57CAC1
P 3800 3150
F 0 "J1" H 3692 2725 50  0000 C CNN
F 1 "Conn_01x04_Female" H 3692 2816 50  0000 C CNN
F 2 "lib:ConnectorMicromatch-4" H 3800 3150 50  0001 C CNN
F 3 "~" H 3800 3150 50  0001 C CNN
F 4 "1291785" H 3800 3150 50  0001 C CNN "Ref Farnell"
	1    3800 3150
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 3250 4200 3250
Wire Wire Line
	4000 2950 4200 2950
Wire Wire Line
	4000 3050 4050 3050
Wire Wire Line
	4050 3050 4050 3100
Wire Wire Line
	4050 3150 4000 3150
Wire Wire Line
	4050 3100 4200 3100
Connection ~ 4050 3100
Wire Wire Line
	4050 3100 4050 3150
Text Label 4200 3100 0    50   ~ 0
GND
Text Label 4200 3250 0    50   ~ 0
R1
Text Label 4200 2950 0    50   ~ 0
L1
$Comp
L Connector:Conn_01x04_Female J2
U 1 1 5E574065
P 3800 3800
F 0 "J2" H 3692 3375 50  0000 C CNN
F 1 "Conn_01x04_Female" H 3692 3466 50  0000 C CNN
F 2 "lib:ConnectorMicromatch-4" H 3800 3800 50  0001 C CNN
F 3 "~" H 3800 3800 50  0001 C CNN
F 4 "1291785" H 3800 3800 50  0001 C CNN "Ref Farnell"
	1    3800 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 3900 4200 3900
Wire Wire Line
	4000 3600 4200 3600
Wire Wire Line
	4000 3700 4050 3700
Wire Wire Line
	4050 3700 4050 3750
Wire Wire Line
	4050 3800 4000 3800
Wire Wire Line
	4050 3750 4200 3750
Connection ~ 4050 3750
Wire Wire Line
	4050 3750 4050 3800
Text Label 4200 3750 0    50   ~ 0
GND
Text Label 4200 3900 0    50   ~ 0
R2
Text Label 4200 3600 0    50   ~ 0
L2
$Comp
L Connector:Conn_01x04_Female J4
U 1 1 5E575B87
P 7300 3850
F 0 "J4" H 7192 3425 50  0000 C CNN
F 1 "Conn_01x04_Female" H 7192 3516 50  0000 C CNN
F 2 "lib:ConnectorMicromatch-4" H 7300 3850 50  0001 C CNN
F 3 "~" H 7300 3850 50  0001 C CNN
F 4 "1291785" H 7300 3850 50  0001 C CNN "Ref Farnell"
	1    7300 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3750 6900 3750
Wire Wire Line
	7100 4050 6900 4050
Wire Wire Line
	7100 3950 7050 3950
Wire Wire Line
	7050 3950 7050 3900
Wire Wire Line
	7050 3850 7100 3850
Wire Wire Line
	7050 3900 6900 3900
Connection ~ 7050 3900
Wire Wire Line
	7050 3900 7050 3850
Text Label 6900 3900 2    50   ~ 0
GND
Text Label 6900 3750 2    50   ~ 0
RO2
Text Label 6900 4050 2    50   ~ 0
LO2
$Comp
L Connector:Conn_01x04_Female J3
U 1 1 5E575B9C
P 7300 3200
F 0 "J3" H 7192 2775 50  0000 C CNN
F 1 "Conn_01x04_Female" H 7192 2866 50  0000 C CNN
F 2 "lib:ConnectorMicromatch-4" H 7300 3200 50  0001 C CNN
F 3 "~" H 7300 3200 50  0001 C CNN
F 4 "1291785" H 7300 3200 50  0001 C CNN "Ref Farnell"
	1    7300 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3100 6900 3100
Wire Wire Line
	7100 3400 6900 3400
Wire Wire Line
	7100 3300 7050 3300
Wire Wire Line
	7050 3300 7050 3250
Wire Wire Line
	7050 3200 7100 3200
Wire Wire Line
	7050 3250 6900 3250
Connection ~ 7050 3250
Wire Wire Line
	7050 3250 7050 3200
Text Label 6900 3250 2    50   ~ 0
GND
Text Label 6900 3100 2    50   ~ 0
RO1
Text Label 6900 3400 2    50   ~ 0
LO1
Wire Wire Line
	5000 3500 4750 3500
Wire Wire Line
	5000 3600 4750 3600
Wire Wire Line
	5000 3700 4750 3700
Wire Wire Line
	5000 3800 4750 3800
Wire Wire Line
	6050 3500 5800 3500
Wire Wire Line
	6050 3600 5800 3600
Wire Wire Line
	6050 3700 5800 3700
Wire Wire Line
	6050 3800 5800 3800
Text Label 4750 3500 0    50   ~ 0
L1
Text Label 4750 3600 0    50   ~ 0
R1
Text Label 4750 3700 0    50   ~ 0
L2
Text Label 4750 3800 0    50   ~ 0
R2
Text Label 6050 3600 2    50   ~ 0
RO1
Text Label 6050 3500 2    50   ~ 0
LO1
Text Label 6050 3800 2    50   ~ 0
RO2
Text Label 6050 3700 2    50   ~ 0
LO2
$EndSCHEMATC
