# test for FIR filters
# from https://www.youtube.com/watch?v=q_VG3TFMb7M

clear all;
close all;
clf;
pkg load signal;

# loading a sinusoid 440hz
# s signal, fp frequency (44100)
# [s, fp] = audioread("sin440.wav");
# [s, fp] = audioread("DreamStateLogic.wav");
[s, fp] = audioread("mcKenna.wav");

t_length = length(s);
t = linspace(0, t_length / fp, length(s));


amp = 1.;
# multiply the signal
s(1:t_length) = amp * s(1:t_length);

BITS = 5;

for n = 1:BITS;

  a = 2 ^ n - 1;
  signal_quantized = round(s(1:t_length) * a) / a;
  
  noise = s(1:t_length) - signal_quantized;
  
##  plot(t, s(1:t_length), '-b; Original signal;',
##       t, signal_quantized, '-g; quantized signal;',
##       t, noise, '-r; Error of sampling;');
##  grid on;
##  xlabel('Time [s]');
##  ylabel('amplitude [-]');
##  title("singular sterp quantisation");
  
endfor
  N = fp / 2;

  nf_Help = 1;
  help = 2;
  while( help < N)
    help = help * 2;
    nf_Help++;
  endwhile
  
  Nf = 2 ^ nf_Help;
  Nf2 = N / 2 + 1;
 
  
  freqCut = [3000];
  wc = freqCut / (fp / 2);
  firCoeff = fir1(16, wc, 'low');

  stem(firCoeff);
  title("Filter coefficients");
  hold on;
  sfiltered = filter(firCoeff, 1, s);

  s_final = sfiltered; #audioread("mcKenna.wav");
  t_length = length(s_final);


  f = linspace(0, fp / 2, Nf2);
  
  figure;
  subplot(3,3,4);
  plot(t, s_final(1:t_length));
  title('ft');
  xlabel('Time [s]');
  ylabel('amplitude [-]');
  hold on;
  grid on;

  s_fft = fft(s_final, Nf);
  s_fftabs = abs(s_fft);
  
  subplot(3,3,5);
  plot(f, s_fftabs(1:Nf2));
  title('ft');
  xlabel('frequency [f]');
  ylabel('abs [-]');
  hold on;
  grid on;


  
player = audioplayer (sfiltered, 44100, 16);

play (player); 

  figure;
  freqz(firCoeff, 1, 2 ^ nf_Help, fp);
